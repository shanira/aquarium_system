<!-- Navigation Bar-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\web\Session;
AppAsset::register($this);
$session = Yii::$app->session;
?>
<!-- Navigation Bar-->
<header id="topnav">



    <div class="navbar-custom">
    <div class="container">
		 <div class="col-md-12" style="padding-top:10px;">
			<a  class="col-md-1" href="<?= Url::home()?>"> <img src="<?php echo Yii::$app->request->baseUrl;?>/web/uploads/logo_grey_high.png" width="50" height="60"></a>
			<div class="col-md-9">

			<div class="col-md-9">
            <div id="navigation">
                <!-- Navigation Menu-->

                <ul class="navigation-menu">
						<?php
				   if(Yii::$app->user->identity->status=='Admin'){ ?>
                    <li class="has-submenu">
                      <a class="fa fa-cog" href="<?= Url::to(['site/admin' ])?>"> <span> <?php echo Yii::t('app', 'הגדרות'); ?> </span> </a>
                        <ul class="submenu">

				<li><a href="<?= Url::to(['foods/index' ])?>"><?php echo Yii::t('app', 'ניהול מזון' ); ?></a></li>
              <li><a href="<?= Url::to(['foodtype/index' ])?>"><?php echo Yii::t('app', 'הוספת סוגי מזון' ); ?></a></li>
			        <li><a href="<?= Url::to(['diagnostic/index'])?>"><?php echo Yii::t('app', 'הוספת סוג אבחון'); ?></a></li>
              <li><a href="<?= Url::to(['dead/index' ])?>"><?php echo Yii::t('app', 'הוספת סוגי מוות' ); ?></a></li>
              <li><a href="<?= Url::to(['lsssystem/index' ])?>"><?php echo Yii::t('app', 'מכונות LSS' ); ?></a></li>
              <li><a href="<?= Url::to(['lss/index' ])?>"><?php echo Yii::t('app', 'קישור LSS למיכל' ); ?></a></li>
              <li><a href="<?= Url::to(['user/index'])?>"><?php echo Yii::t('app', 'הרשאות משתמשים'); ?></a></li>
              <li><a href="<?= Url::to(['employee/index'])?>"><?php echo Yii::t('app', 'הרשאות עובדים'); ?></a></li>
                        </ul>
                    </li>
                    <?php } ?>

                    <li class="has-submenu">
                        <a class="fa fa-line-chart" href="<?= Url::to(['/chartbuilder'])?>"><span> <?php echo Yii::t('app', ' גרפים'); ?> </span> </a>
                    </li>

                    <li class="has-submenu">
                        <a class="fa fa-table" href="<?= Url::to(['report/index'])?>"><span> <?php echo Yii::t('app', 'דוחות'); ?> </span> </a>
                        <ul class="submenu">
						  <li><a href="<?= Url::to(['report/fishcount' ])?>"><?php echo Yii::t('app', 'דוח כמות דגים' ); ?></a></li>
							<li><a href="<?= Url::to(['report/fish' ])?>"><?php echo Yii::t('app', 'דוח היסטוריית דגים' ); ?></a></li>
              <li><a href="<?= Url::to(['report/weightreport' ])?>"><?php echo Yii::t('app', 'דוח משקל דגים' ); ?></a></li>
							<li><a href="<?= Url::to(['report/sample' ])?>"><?php echo Yii::t('app', 'דוח דגימות מים' ); ?></a></li>
							<li><a href="<?= Url::to(['report/food' ])?>"><?php echo Yii::t('app', 'דוח מזון' ); ?></a></li>
							<li><a href="<?= Url::to(['report/lss' ])?>"><?php echo Yii::t('app', 'דוח LSS' ); ?></a></li>
              <li><a href="<?= Url::to(['report/vetdiagno' ])?>"><?php echo Yii::t('app', 'דוח אבחון' ); ?></a></li>
              <li><a href="<?= Url::to(['report/vet' ])?>"><?php echo Yii::t('app', 'דוח טיפול' ); ?></a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a class="fa fa-tint" href="<?= Url::to(['sample/index'])?>"><span> <?php echo Yii::t('app', 'מים'); ?> </span> </a>
                    </li>
					

                    <li class="has-submenu">
                        <a class="fa fa-home" href="<?= Url::to(['site/index' ])?>"> <span> <?php echo Yii::t('app', 'מיכלים'); ?> </span> </a>
						<?php
						$selQry4 = Yii::$app->db->createCommand("SELECT * from containers");
                        $result4 = $selQry4->queryAll();

			           ?>
                        <ul class="submenu" style="width:300px;">
						<?php foreach($result4 as $cnt){

						?>
						<li>
						<a href="<?= Url::to(['containers/show', 'id' => $cnt['cid'] ])?>"><?php echo Yii::t('app', $cnt['container_name'] ); ?></a>
						</li>
						<?php } ?>

                        </ul>
                    </li>

                </ul>

                <!-- End navigation menu  -->
            </div>
			</div>
			<div class="col-md-2"></div>
			<!--div class="col-md-3"><!?= Html::submitButton('מצב <br> Last Check', ['class' => 'btn topbtn', 'id' => 'lastcheck', 'name' => 'login-button']) ?></div-->
			<!--div class="col-md-3"><!?= Html::submitButton('PreShow <br>State', ['class' => 'btn topbtn', 'id' => 'preshow', 'name' => 'login-button']) ?></div-->


			</div>
      <div class = "col-md-1"></div>
			<div class="col-md-2 bluecolor">
			<?php if(!empty(Yii::$app->user->identity->username)){ ?>
			<?php echo Yii::t('app', 'מחובר כ-'); ?> <?php if(!empty(Yii::$app->user->identity->username)){
				echo $session->get('employee_name'); //Yii::$app->user->identity->username;
				} ?>
			<a class="logout fa fa-sign-out" href="<?= Url::to(['site/second'])?>" class="dropdown-item notify-item" data-method="get"><span><?php echo Yii::t('app', ' התנתק '); ?></span></a>
			<?php } ?>
			</div>
			</div>
        </div>
    </div>
</header>


<!-- End Navigation Bar-->


<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container">
<style>
#topnav .navbar-custom{
  background-color: #003366 !important;
}
#topnav .navigation-menu > li > a{
color: #FFFFFF !important;

}
.logout{
  color: #FFFFFF !important;

}
</style>
