    
   
    <!-- Bootstrap CSS -->
    <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- App CSS -->
    <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/css/style.css" rel="stylesheet" type="text/css" />
    
    <!-- Modernizr js -->
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/modernizr.min.js"></script>
  <?php //$this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<?php if(!empty(Yii::$app->user->identity->username) && empty($session->get('sleep'))){ ?>
    <?php require 'topbar.php'; ?>
<?php } ?>

 