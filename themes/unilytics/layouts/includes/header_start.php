<?php
use yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <meta name="robots" content="noindex">
    <meta http-equiv="refresh" content="86400;url=<?= Url::to(['site/logout'])?>" />
    <?php if(!empty(Yii::$app->user->identity->username)){ ?>
    <meta http-equiv="refresh" content="1800;url=<?= Url::to(['site/second'])?>" />
    <?php } ?>
    <!-- App Favicon -->
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/images/favicon.ico">

    <!-- App title -->
    <title>אקווריום ישראל</title>

    <?php $this->head() ?>

   <!-- DataTables -->
        <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Switchery css -->

    <link href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/jquery.min.js"></script>
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/popper.min.js"></script><!-- popper for Bootstrap -->
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/bootstrap.min.js"></script>
    <!--<script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/waves.js"></script>-->
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/js/jquery.nicescroll.js"></script>
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/switchery/switchery.min.js"></script>

     <!-- Counter Up  -->
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/counterup/jquery.counterup.min.js"></script>
    <!-- Required datatable js -->
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/jszip.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/datatables/responsive.bootstrap4.min.js"></script>
