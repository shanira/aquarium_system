<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>

<?php $this->beginPage(); ?>
<?php require 'includes/header_start.php'; ?>
<!--Morris Chart CSS -->
<link rel="stylesheet" href="<?php echo Yii::$app->request->baseUrl;?>/themes/unilytics/plugins/morris/morris.css">

<?php require 'includes/header_end.php'; ?>
<!-- Page-Title -->
<?php /*?><div class="row">
    <div class="col-sm-12">
        <h4 class="page-title"><?php echo $this->title; ?></h4>
    </div>
</div><?php */?>

<!-- Content from pages START -->

	<?= $content ?>

<!-- Content from pages END -->

<?php require 'includes/footer_start.php' ?>



<!-- Page specific js -->
<?php /*?><script src="<?php @app?>../themes/unilytics/pages/jquery.dashboard.js"></script><?php */?>

<?php require 'includes/footer_end.php' ?>