
/**
* Theme: Uplon Admin Template
* Author: Coderthemes
* Dashboard
*/

!function($) {
    "use strict";

    var Dashboard = function() {};

    //creates Stacked chart
    Dashboard.prototype.createStackedChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            stacked: true,
            labels: labels,
            hideHover: 'auto',
            barSizeRatio: 0.7,
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barColors: lineColors
        });
    },


    //creates Donut chart
    Dashboard.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors
        });
    },

    Dashboard.prototype.init = function() {

        //creating Stacked chart
        var $stckedData  = [
            { y: 'SUND', a: 5, b: 80, c: 15 },
            { y: 'TEO', a: 75,  b: 5, c: 20 },
            { y: 'JUR', a: 0, b: 10, c: 90 },
            { y: 'HUM', a: 25,  b: 65, c: 10 },
            { y: 'SCIENCE', a: 10, b: 80, c: 10 },
           
        ];
        this.createStackedChart('morris-bar-stacked', $stckedData, 'y', ['a', 'b' ,'c'], ['Dead', 'Not-visited', 'Visited'], ['#ff5d48','#f1b53d', '#1bb99a']);

        //creating donut chart
        var $donutData = [
                {label: "Dead", value: 36},
                {label: "Not visited", value: 30},
                {label: "Visited", value: 34}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#ff5d48','#f1b53d', '#1bb99a']);
    },
    //init
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.Dashboard.init();
}(window.jQuery);
