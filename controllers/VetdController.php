<?php

namespace app\controllers;

use Yii;
use app\models\Vetd;
use app\models\VetdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use  yii\web\Session;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * VetController implements the CRUD actions for Vet model.
 */
class VetdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));

    if($action->id == "index")
        $this->enableCsrfValidation = false;
	if($action->id == "addcomment")
        $this->enableCsrfValidation = false;
	if($action->id == "savecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "updatecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "deletecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "tabsupdate")
        $this->enableCsrfValidation = false;
    if($action->id == "getsecond")
        $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }
    /**
     * Lists all Vet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VetdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		return Json::encode($html);
    }

    /**
     * Displays a single Vet model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
   public function actionAddcomment() {
		  $model = new Vetd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vedt_id]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new Vet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vetd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vedt_id]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vedt_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
public function actionUpload()
{
    $fileName = 'image';
    $uploadPath = 'vetd_files';

    if (isset($_FILES[$fileName])) {
        $file = \yii\web\UploadedFile::getInstanceByName($fileName);

        //Print file data
        //print_r($file);

        if ($file->saveAs($uploadPath . '/' . $file->name)) {
		   
            //Now save file data to database

            echo \yii\helpers\Json::encode($file);
        }
    }

    return false;
}
    /**
     * Finds the Vet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vetd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vetd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	public function actionSavecomment()
    {
	$post = Yii::$app->request->post();

	Yii::$app->db->createCommand('INSERT INTO vet_diagnostic (container_id, diagnostic_for, diagnostic_name, diagnostic_type, image, comments, date_created, updated_date, created_by) VALUES (:container_id,:diagnostic_for, :diagnostic_name, :diagnostic_type, :image, :comments,  :date_created, :updated_date, :created_by)', array(
  ':container_id' => $post['container_id'],
	':diagnostic_for' => $post['diagnostic_for'],
	':diagnostic_name' => $post['diagnostic_name'],
	':diagnostic_type' => $post['diagnostic_type'],
	':image' => $post['image'],
	':comments' => $post['comments'],
	':date_created' => $post['created_date'],
	':updated_date' => $post['updated_date'],
	':created_by' => $post['created_by'],
	))->execute();

	Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Added Vet Diagnostic For ",
	':who_did' => $post['created_by'],
	':container_id' => $post['container_id'],
	':action_date' => $post['created_date'],
	))->execute();



    }

   public function actionUpdatecomment()
    {
        $model = new Vetd();
        $post = Yii::$app->request->post();
		Yii::$app->db->createCommand()->update('vet_diagnostic', ['diagnostic_for' => $post['diagnostic_for'], 'diagnostic_name' => $post['diagnostic_name'], 'diagnostic_type' => $post['diagnostic_type'], 'comments' => $post['comments'], 'updated_date' => $post['updated_date']], 'vedt_id = "'.$post['vet_id'].'"')->execute();
		Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updared Vet Diagonostic Report For ",
	':who_did' => $post['created_by'],
	':container_id' => $post['container_id'],
	':action_date' => $post['created_date'],
	))->execute();
    }
  public function actionDeletecomment()
    {
		$post = Yii::$app->request->post();
		$id  = $post['fid'];
        Yii::$app->db->createCommand('DELETE FROM vet_diagnostic WHERE vedt_id='.$id.'')->execute();
		$this->actionIndex();
    }
	public function actionTabsupdate() {
		  $model = new Vetd();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'vedt_id' => $model->vedt_id]);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }

	public function actionGetsecond(){

		 $qry = "";
		 $post = Yii::$app->request->post();
		 $id = $post['id'];
		 $dropdown  = '';
	     if($id){
		   $qry = " where parent ='".$id."'";
		   }
            $selQry2       = Yii::$app->db->createCommand("SELECT * from diagnostic $qry");
            $result = $selQry2->queryAll();
			foreach ($result as $model) {
				$dropdown .= '<option value="'.$model['d_id'].'">'.$model['d_name'].'</option>';
			}
			return $dropdown;
		}

}
