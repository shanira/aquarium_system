<?php

namespace app\controllers;

use Yii;
use yii\base\Theme; 
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\Session;
use app\models\User;
use app\models\Employee;
use app\models\Containers;
use app\models\ContainersSearch;
use yii\helpers\Url;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','logout','sample','second'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'sample'],
                        'allow' => true,
                    ],
                    [
                         // allow authenticated users
						'actions' => ['logout', 'index', 'about','sample','second'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
					 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all Containers models.
     * @return mixed
     */
    public function actionIndex()
    {  
	  $session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
		if (!$session->get('employee_name'))
            $this->redirect(array('site/second'));
        $searchModel = new ContainersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Containers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Containers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Containers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Containers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Containers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Containers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Containers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Containers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
			 return $this->redirect(['second']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
	
	public function actionSecond()
    {
        
        $session = Yii::$app->session;
        $session->set('sleep', 'deep');
        $model = new Employee();
		if(Yii::$app->request->post()){
			$post = Yii::$app->request->post();
			
			if (isset($post['Employee']['code'])) {
				 $id = $post['Employee']['code']; 
				$super = Employee::findOne(["code" => $id]);
				if($super){ 
				$session = Yii::$app->session;
				$session->set('employee_name', $super->name);
				$session->remove('sleep');
				unset($session['sleep']);
                return $this->goHome();
				}else{
					ob_start();
				Yii::$app->session->setFlash('success', "does not match username.");
				$session = Yii::$app->session;
				//$session->remove('sleep');
				//echo '<meta http-equiv="refresh" content="1;url='.Url::to(["site/logout"]).'" />';
				}
			}
		}
        return $this->render('second', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	public function actionAdmin(){
		
	return $this->render('admin');
	}
	
	public function actionGii ()
	{
		return $this->render('gii');
	}
	

	
 }