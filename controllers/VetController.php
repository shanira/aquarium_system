<?php

namespace app\controllers;

use Yii;
use app\models\Vet;
use app\models\VetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use  yii\web\Session;
use yii\helpers\Url;

/**
 * VetController implements the CRUD actions for Vet model.
 */
class VetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));

    if($action->id == "index")
        $this->enableCsrfValidation = false;
	if($action->id == "addcomment")
        $this->enableCsrfValidation = false;
	if($action->id == "savecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "updatecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "deletecomment")
        $this->enableCsrfValidation = false;
	if($action->id == "tabsupdate")
        $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }
    /**
     * Lists all Vet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		return Json::encode($html);
    }

    /**
     * Displays a single Vet model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
   public function actionAddcomment() {
		  $model = new Vet();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vet_id]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new Vet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vet();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vet_id]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vet_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vet::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	public function actionSavecomment()
    {
        $post = Yii::$app->request->post();

	Yii::$app->db->createCommand('INSERT INTO vet (container_id, treatment_for, treatment, specific_treatment, frequency,dilution,how_given,high, comments, date_created, updated_date, created_by) VALUES (:container_id,:treatment_for, :treatment, :specific_treatment,:frequency,:dilution, :how_given, :high, :comments, :date_created, :updated_date, :created_by)', array(
    ':container_id' => $post['container_id'],
	':treatment_for' => $post['treatment_for'],
	':treatment'    => $post['treatment'],
	':specific_treatment' => $post['specific_treatment'],
	':frequency' => $post['frequency'],
	':dilution' => $post['dilution'],
	':how_given' => $post['how_given'],
	':high' => $post['high'],
	':comments' => $post['comments'],
	':date_created' => $post['created_date'],
	':updated_date' => $post['updated_date'],
	':created_by' => $post['created_by'],
	))->execute();

	Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Added Vet Report For ",
	':who_did' => $post['created_by'],
	':container_id' => $post['container_id'],
	':action_date' => $post['created_date'],
	))->execute();



    }

   public function actionUpdatecomment()
    {
        $model = new Vet();
        $post = Yii::$app->request->post();
		Yii::$app->db->createCommand()->update('vet', ['treatment_for' => $post['treatment_for'],'treatment' => $post['treatment'],'specific_treatment' => $post['specific_treatment'],'frequency' => $post['frequency'], 'dilution' => $post['dilution'],'how_given' => $post['how_given'],'high' => $post['high'],'comments' => $post['comments'], 'updated_date' => $post['updated_date']], 'vet_id = "'.$post['vet_id'].'"')->execute();
		Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updared Vet Report For ",
	':who_did' => $post['created_by'],
	':container_id' => $post['container_id'],
	':action_date' => $post['created_date'],
	))->execute();
    }
  public function actionDeletecomment()
    {
		$post = Yii::$app->request->post();
		$id  = $post['fid'];
        Yii::$app->db->createCommand('DELETE FROM vet WHERE vet_id='.$id.'')->execute();
		$this->actionIndex();
    }
	public function actionTabsupdate() {
		  $model = new Vet();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'vet_id' => $model->vet_id]);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }

}
