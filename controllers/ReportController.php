<?php

namespace app\controllers;
use Yii;
use app\models\Fish;
use app\models\FishSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;

use  yii\web\Session;
use yii\helpers\Url;


class ReportController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action)
	{
	$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));

		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionFish()
    {
		$model = new Fish();
        $reportData = $model->getFishReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getFishReport();
           return $this->render('fish', [
            'reportData' => $reportData,
        ]);
        }
        return $this->render('fish', [
            'reportData' => $reportData,
        ]);
    }

	public function actionFishcount()
    {
		$model = new Fish();
        $reportData = $model->getFishAmountReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getFishAmountReport();
           return $this->render('fishcount', [
            'reportData' => $reportData,
        ]);
        }
        return $this->render('fishcount', [
            'reportData' => $reportData,
        ]);
    }

	public function actionWeightreport()
    {
		$model = new Fish();
        $reportData = $model->getFishWeightReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getFishWeightReport();
           return $this->render('weight', [
            'reportData' => $reportData,
        ]);
        }
        return $this->render('weight', [
            'reportData' => $reportData,
        ]);
    }


	public function actionWater()
    {
        $model = new Fish();
		$reportData = $model->getWaterReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getWaterReport();
           return $this->render('water', [
            'reportData' => $reportData,
          ]);
        }
        return $this->render('water', [
            'reportData' => $reportData,
        ]);
    }
	
	public function actionSample()
    {
        $model = new Fish();
		$reportData = $model->getSampleReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getSampleReport();
           return $this->render('sample', [
            'reportData' => $reportData,
          ]);
        }
        return $this->render('sample', [
            'reportData' => $reportData,
        ]);
    }

	public function actionFood()
    {
        $model = new Fish();
		$reportData = $model->getFoodReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getFoodReport();
           return $this->render('food', [
            'reportData' => $reportData,
          ]);
        }
         return $this->render('food', [
            'reportData' => $reportData,
        ]);

    }
	
	public function actionFoods()
    {
        $model = new Fish();
		$reportData = $model->getFoodsReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getFoodsReport();
           return $this->render('foods', [
            'reportData' => $reportData,
          ]);
        }
         return $this->render('foods', [
            'reportData' => $reportData,
        ]);

    }
	public function actionLss()
    {
         $model = new Fish();
		 $reportData = $model->getLssReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getLssReport();
           return $this->render('lss', [
            'reportData' => $reportData,
          ]);
        }
         return $this->render('lss', [
            'reportData' => $reportData,
        ]);
    }
	public function actionVet()
    {
        $model = new Fish();
		$reportData = $model->getVetReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getVetReport();
           return $this->render('vat', [
            'reportData' => $reportData,
          ]);
        }
         return $this->render('vat', [
            'reportData' => $reportData,
        ]);
    }


	public function actionVetdiagno()
    {
        $model = new Fish();
		$reportData = $model->getVetdiagnoReport();
        if (Yii::$app->request->post()) {
			$reportData = $model->getVetdiagnoReport();
           return $this->render('vetdiagno', [
            'reportData' => $reportData,
          ]);
        }
         return $this->render('vetdiagno', [
            'reportData' => $reportData,
        ]);
    }


	public function actionGetcontainerfish()
    {
        $model = new Fish();
		return $reportData = $model->getFishData();
		exit;
    }
	public function actionDiagonstictype()
    {
        $model = new Fish();
		return $reportData = $model->getdiagonstictype();
		exit;
    }
	public function actionLssmachinedrop()
    {
        $model = new Fish();
		return $reportData = $model->lssmachinedrop();
		exit;
    }

}
