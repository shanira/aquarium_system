<?php

namespace app\controllers;

use Yii;
use app\models\Containers;
use app\models\ContainersSearch;
use app\models\Lss;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Session;
/**
 * ContainersController implements the CRUD actions for Containers model.
 */
class ContainersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','update'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'sample'],
                        'allow' => true,
                    ],
                    [
                         // allow authenticated users
						'actions' => ['view', 'index', 'update','create','lss'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
					 'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
		if (!$session->get('employee_name'))
            $this->redirect(array('site/second'));

    if($action->id == "lss")
        $this->enableCsrfValidation = false;
	if($action->id == "savelss")
        $this->enableCsrfValidation = false;
	if($action->id == "lsshistory")
        $this->enableCsrfValidation = false;
		if($action->id == "savestatus")
        $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
    }

    /**
     * Lists all Containers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContainersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Containers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Containers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Containers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Containers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Containers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Containers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Containers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Containers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

	public function actionShow($id)
    {
        return $this->render('show', [
            'model' => $this->findModel($id),
        ]);
    }
	public function actionLss()
    {
		$model = new Lss();
        $html = $this->renderPartial('lss', ['model' => $model,]);
		 return Json::encode($html);
    }

	public function actionSavelss()
    {
       $post = Yii::$app->request->post();
		   $exp_data = explode("&", $post['datalist']);
		   $cnt = sizeof($exp_data);
		   $session = Yii::$app->session;

		   $selQry2 = Yii::$app->db->createCommand("SELECT lss_id from lss where date = '".date('Y-m-d')."' AND container_id='".$post['container_id']."'");
           $result2 = $selQry2->queryOne();


			for($i=0; $i<$cnt; $i++){
				$mData = explode("=", $exp_data[$i]);

		   $selLssAssociated = Yii::$app->db->createCommand("SELECT container_id from lss_machine_associate where lss_attached_id='".$mData[0]."'");
           $LssAssociated = $selLssAssociated->queryAll();

			foreach($LssAssociated as $associatedData){
				if(!empty($mData[1])){
			Yii::$app->db->createCommand('INSERT INTO lss (container_id, lss_machine, machine_value, date, date_created, date_modified,status) VALUES (:container_id,:lss_machine, :machine_value, :date, :date_created, :date_modified, :status)', array(
    ':container_id' => $associatedData['container_id'],
	':lss_machine' => $mData[0],
	':machine_value' => $mData[1],
	':date'  => date ("Y-m-d"),
	':date_created'  => date ("Y-m-d H:i:s"),
	':date_modified' => date ("Y-m-d H:i:s"),
  ':status' => 'pre show'
	))->execute();
				}
			 }

	  Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Reported Pre-Show Value For ",
	  ':who_did' => $session->get('employee_name'),
	  ':container_id' => $post['container_id'],
	   ':action_date' => date ("Y-m-d H:i:s"),
	))->execute();

	 }
}

	public function actionLsshistory() {
		  $model = new Lss();
		  $post = Yii::$app->request->post();
          $fid = $post['field_id'];
		  $cid = $post['container_id'];
		  $selQry = Yii::$app->db->createCommand("SELECT lss_id, machine_value, status, date_created from lss where container_id='".$cid."' AND lss_machine='".$fid."'   order by lss_id DESC LIMIT 0,30");
          $result = $selQry->queryAll();

        return $this->renderPartial('lsshistory', [
            'result' => $result,
        ]);
    }

	public function actionSavestatus()
    {
           $post = Yii::$app->request->post();

		   $selQry2 = Yii::$app->db->createCommand("SELECT lss_id from lss where date = '".date('Y-m-d')."' AND container_id='".$post['container_id']."'");
           $result2 = $selQry2->queryOne();

			if($post['flag']=='ok'){
			Yii::$app->db->createCommand()->update('lss', ['status' =>'ok'], 'lss_machine = "'.$post['lss_machine'].'" AND date = "'.date('Y-m-d').'"')->execute();
			}
			if($post['flag']=='notok'){
			Yii::$app->db->createCommand()->update('lss', ['status' =>''], 'lss_machine = "'.$post['lss_machine'].'" AND date = "'.date('Y-m-d').'"')->execute();
			}

    }

    public function actionSavelsslast()
      {
         $post = Yii::$app->request->post();
  		   $exp_data = explode("&", $post['datalist']);
  		   $cnt = sizeof($exp_data);
  		   $session = Yii::$app->session;

  		   $selQry2 = Yii::$app->db->createCommand("SELECT lss_id from lss where date = '".date('Y-m-d')."' AND container_id='".$post['container_id']."'");
             $result2 = $selQry2->queryOne();


  			for($i=0; $i<$cnt; $i++){
  				$mData = explode("=", $exp_data[$i]);

  		   $selLssAssociated = Yii::$app->db->createCommand("SELECT container_id from lss_machine_associate where lss_attached_id='".$mData[0]."'");
             $LssAssociated = $selLssAssociated->queryAll();

  			foreach($LssAssociated as $associatedData){
  				if(!empty($mData[1])){
  			Yii::$app->db->createCommand('INSERT INTO lss (container_id, lss_machine, machine_value, date, date_created, date_modified, status) VALUES (:container_id,:lss_machine, :machine_value, :date, :date_created, :date_modified, :status)', array(
      ':container_id' => $associatedData['container_id'],
  	':lss_machine' => $mData[0],
  	':machine_value' => $mData[1],
  	':date'  => date ("Y-m-d"),
  	':date_created'  => date ("Y-m-d H:i:s"),
  	':date_modified' => date ("Y-m-d H:i:s"),
    ':status' => 'last check'
  	))->execute();
  				}
  			 }

  	  Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
      ':what_did' => "Reported Last-Check Value For ",
  	':who_did' => $session->get('employee_name'),
  	':container_id' => $post['container_id'],
  	':action_date' => date ("Y-m-d H:i:s"),
  	))->execute();

  	 }
  }

}
