<?php
namespace app\controllers;

use Yii;
use app\models\User;
use app\models\SearchUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\PasswordForm;


class UserController extends \yii\web\Controller
{


    /**
     * @inheritdoc
     */

	public function beforeAction($action)
    {
		//echo "<pre>";
		//print_r($action);
		//die("Debugging...");
        if (in_array($action->id, ['ajax'])) {
            $this->enableCsrfValidation = false;
        }
		if (in_array($action->id, ['delete'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
	public function behaviors()
    {
        return [
            'access'=>[
                'class'=>AccessControl::className(),
				'only' => ['create','update','delete','view','index','profile','requestPasswordReset','resetPassword','Setting','ajax'],
                'rules'=>[
                        [
                        'actions'=>['create','update','delete','view','index','profile','requestPasswordReset','resetPassword','Setting','ajax'],
                        'allow'=>true,
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->status=='Admin'
                            );
                        }
                        ],
						[
                         // allow authenticated users
						'actions' => ['index','profile','requestPasswordReset','resetPassword','Setting','ajax'],
                        'allow' => true,
                         'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->status=='Moderator'
                            );
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['get'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

/**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionProfile()
    {
        return $this->render('profile');
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
				//Email Cde
				$message = Yii::$app->mailer->compose();
				if (Yii::$app->user->isGuest) {
					$message->setFrom('info@brainzteck.com');
				} else {
					$message->setFrom(Yii::$app->user->identity->email);
				}

				$body = "<b>Hello ".Yii::$app->request->post()['SignupForm']['username'].", <br> Your login details are below:<br><b>Username</b>:".Yii::$app->request->post()['SignupForm']['username']."<br><b>Password:</b>".Yii::$app->request->post()['SignupForm']['password'];
				$message->setTo(Yii::$app->request->post()['SignupForm']['email'])
					->setSubject('New User Created.')
					->setHtmlBody($body)
					->send();

				Yii::$app->session->setFlash('success', "משתמש נוצר בהצלחה!");
                 return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
			$model->updateStatus($id);
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->redirect(['request-password-reset']);
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
			   throw new BadRequestHttpException($e->getMessage());
        }
     if(!$model->validate()){
		 if(isset($_SESSION['success'])){
		 $msg = $_SESSION['success'];
		 Yii::$app->session->setFlash('success', $msg);
		 return $this->render('resetPassword', [
            'model' => $model,
        ]);
		 }
	 }
        if ($model->load(Yii::$app->request->post())  && $model->resetPassword()) {

            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }


        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

	public function actionSetting()
    {

        $model = new PasswordForm;
        $modeluser = User::findByUsername(Yii::$app->user->identity->email);

        if($model->load(Yii::$app->request->post())){
           /// if($model->validate()){
                try{

                    $modeluser->password = $_POST['PasswordForm']['newpass'];
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed'
                        );
                        return $this->redirect(['setting']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                        return $this->redirect(['setting']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('setting',[
                        'model'=>$model
                    ]);
                }
           //}
        }else{
            return $this->render('setting',[
                'model'=>$model
            ]);
        }

	}

	public function actionAjax()
    {
		$UsersModel = new user();
		$email = $UsersModel->updateEmail(Yii::$app->request->post());
    }

}
