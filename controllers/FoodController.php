<?php

namespace app\controllers;

use Yii;
use app\models\Food;
use app\models\FoodSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use  yii\web\Session;
use yii\helpers\Url;


/**
 * FoodController implements the CRUD actions for Food model.
 */
class FoodController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
			if (!$session->get('employee_name'))
            $this->redirect(array('site/second'));

    if($action->id == "index")
        $this->enableCsrfValidation = false;
	if($action->id == "tabscreate")
        $this->enableCsrfValidation = false;
	if($action->id == "savefood")
        $this->enableCsrfValidation = false;
	if($action->id == "saveactualamount")
        $this->enableCsrfValidation = false;
	if($action->id == "deletefood")
        $this->enableCsrfValidation = false;
	if($action->id == "foodupdate")
        $this->enableCsrfValidation = false;
	if($action->id == "updatefood")
        $this->enableCsrfValidation = false;
	if($action->id == "updatedfood")
        $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }

    /**
     * Lists all Food models.
     * @return mixed
     */
    public function actionIndex()
    {
		$session = Yii::$app->session;
        $searchModel = new FoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$container_id = $session->get('cid');
		$data = $searchModel->getContainerData($container_id);
		$current_update = $searchModel->getCurrentUpdate($container_id);
		$last_update = $searchModel->getLastUpdate($container_id);



        $html = $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'data' => $data,
			'current_update' => $current_update,
			'last_update' => $last_update,
        ]);
		return Json::encode($html);
    }

	public function actionTabscreate() {
		  $model = new Food();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->food_id]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Food model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Food model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Food();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->food_id]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Food model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->food_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Food model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Food model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Food the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Food::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	public function actionSavefood()
    {
        $post = Yii::$app->request->post();
		 $selQry     = Yii::$app->db->createCommand("SELECT cid from containers");
         $result = $selQry->queryAll();
		 foreach($result as $rows){
	Yii::$app->db->createCommand('INSERT INTO food (container_id, food_type, unit, amount, actual_unit, actual_amount,commemt, created_date, updated_date, created_by) VALUES (:container_id,:food_type,:unit,:amount,:actual_unit, :actual_amount,:comment, :created_date, :updated_date, :created_by)', array(
    ':container_id' => $rows['cid'],
	':food_type' => $post['food_type'],
	':unit' => $post['unit'],
	':amount' => $post['amount'],
	':actual_unit' => $post['actual_unit'],
	':actual_amount'  => $post['actual_amount'],
  	':comment'  => $post['comment'],
	':created_date' => $post['created_date'],
	':updated_date' => $post['updated_date'],
	':created_by' => $post['created_by'],
	))->execute();
	Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updated food table For ",
	':who_did' => $post['created_by'],
	':container_id' => $rows['cid'],
	':action_date' => $post['created_date'],
	))->execute();
		 }
    }

	public function actionUpdatedfood()
    {
        $post = Yii::$app->request->post();
		$session = Yii::$app->session;
		Yii::$app->db->createCommand()->update('food', ['food_type' => $post['food_type'],'unit' => $post['unit'],'amount' => $post['amount'],'actual_amount' => $post['actual_amount'],'updated_date' => $post['updated_date'], 'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'"')->execute();

	Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updated food table For ",
	':who_did' => $post['created_by'],
	':container_id' => $post['container_id'],
	':action_date' => $post['created_date'],
	))->execute();
    }


	public function actionSaveactualamount()
    {
        $post = Yii::$app->request->post();
		$session = Yii::$app->session;
		if(!empty($post['actual_amount'])){
		Yii::$app->db->createCommand()->update('food', ['actual_amount' => $post['actual_amount'], 'updated_date'=>date('Y-m-d H:i:s'), 'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'"')->execute();
		}
		if(!empty($post['actual_unit'])){
		Yii::$app->db->createCommand()->update('food', ['actual_unit' => $post['actual_unit'], 'updated_date'=>date('Y-m-d H:i:s'), 'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'"')->execute();
		}
		if(!empty($post['comment'])){
		Yii::$app->db->createCommand()->update('food', ['comment' => $post['comment'], 'updated_date'=>date('Y-m-d H:i:s'), 'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'"')->execute();
		}
    if(!empty($post['comment_w'])){
		Yii::$app->db->createCommand()->update('food', ['comment_w' => $post['comment_w'], 'updated_date'=>date('Y-m-d H:i:s'), 'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'"')->execute();
		}

		 $selQry     = Yii::$app->db->createCommand("SELECT * from food where food_id='".$post['food_id']."'");
         $rows = $selQry->queryOne();

		 $selQry2     = Yii::$app->db->createCommand("SELECT f_history from food_history where food_id='".$post['food_id']."' AND created_date='".date('Y-m-d')."' AND container_id ='".$rows['container_id']."'");
         $row = $selQry2->queryOne();
		 $created_by =  $session->get('employee_name');
		if(!empty($row['f_history'])){
		Yii::$app->db->createCommand()->update('food_history', ['comment' => $post['comment'],'comment_w' => $post['comment_w'], 'actual_amount' => $post['actual_amount'], 'updated_date'=>date('Y-m-d H:i:s'),  'created_by' => $session->get('employee_name')], 'food_id = "'.$post['food_id'].'" AND created_date="'.date('Y-m-d').'"')->execute();
		}else{
		Yii::$app->db->createCommand('INSERT INTO food_history (food_id, container_id, fish_to, day_of_week, food_type, unit, amount, actual_unit, actual_amount,comment,comment_w, created_date, updated_date, created_by) VALUES (:food_id, :container_id, :fish_to, :day_of_week, :food_type,:unit,:amount,:actual_unit, :actual_amount, :comment, :comment_w, :created_date, :updated_date, :created_by)', array(
	':food_id' => $rows['food_id'],
    ':container_id' => $rows['container_id'],
	':fish_to' => $rows['fish_to'],
	':day_of_week' => $rows['day_of_week'],
	':food_type' => $rows['food_type'],
	':unit' => $rows['unit'],
	':amount' => $rows['amount'],
	':actual_unit' => '',
	':actual_amount'  => $post['actual_amount'],
	':comment'  => $post['comment'],
  ':comment_w'  => $post['comment_w'],
	':created_date' => date('Y-m-d'),
	':updated_date' => date('Y-m-d H:i:s'),
	':created_by' => $session->get('employee_name'),
	))->execute();
	Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updated food table For ",
	':who_did' => $created_by,
	':container_id' => $rows['container_id'],
	':action_date' => date('Y-m-d H:i:s'),
	))->execute();
		}


    }

	public function actionDeletefood()
    {
		$post = Yii::$app->request->post();
		$id  = $post['fid'];
        Yii::$app->db->createCommand('DELETE FROM food WHERE food_id='.$id.'')->execute();
		$this->actionIndex();
    }

	public function actionUpdatefood() {
		  $model = new Food();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'vet_id' => $model->food_id]);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }
}
