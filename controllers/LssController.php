<?php

namespace app\controllers;

use Yii;
use app\models\LssMachineAssociate;
use app\models\LssMachineAssociateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LssController implements the CRUD actions for LssMachineAssociate model.
 */
class LssController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
					'delete' => ['GET','POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action)
	{
	$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
	if($action->id == "index")
        $this->enableCsrfValidation = false;
	if($action->id == "tabscreate")
        $this->enableCsrfValidation = false;
	if($action->id == "savefood")
        $this->enableCsrfValidation = false;
	if($action->id == "waterhistory")
        $this->enableCsrfValidation = false;
	if($action->id == "savewater")
        $this->enableCsrfValidation = false;
	if($action->id == "updatewater")
        $this->enableCsrfValidation = false;
	if($action->id == "deletewater")
        $this->enableCsrfValidation = false;
	if($action->id == "tabsupdate")
        $this->enableCsrfValidation = false;

		return parent::beforeAction($action);
	}
	/*public function beforeAction($action) {
    if($action->id == "delete")
        $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }*/


    /**
     * Lists all LssMachineAssociate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LssMachineAssociateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LssMachineAssociate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LssMachineAssociate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LssMachineAssociate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->aid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LssMachineAssociate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->aid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LssMachineAssociate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LssMachineAssociate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LssMachineAssociate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LssMachineAssociate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

	public function actionUpdatelss()
    {
        $model = new Lss();
		$session = Yii::$app->session;
        $post = Yii::$app->request->post();
		if(!empty($post['lss_id'])){
        $id = $post['lss_id'];
		$watertype = $post['watertype'];
		$watervalue = $post['watervalue'];
		Yii::$app->db->createCommand()->update('lss', [$watertype => $watervalue], 'lss_id = "'.$post['lss_id'].'"')->execute();
     }
	}

	public function actionDeletelss()
    {
		 $post = Yii::$app->request->post();
		 $id  = $post['fid'];
		Yii::$app->db->createCommand('DELETE FROM lss WHERE lss_id='.$id.'')->execute();
		$this->actionIndex();
    }


	public function actionTabsupdate() {
		  $model = new Lss();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
		 $watertype = $post['watertype'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->renderPartial('updatelss', [
            'model' => $model, 'lss_id'  => $lss_id,
        ]);
    }

}
