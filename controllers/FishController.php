<?php

namespace app\controllers;

use Yii;
use app\models\Fish;
use app\models\FishSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;

use  yii\web\Session;
use yii\helpers\Url;



/**
 * FishController implements the CRUD actions for Fish model.
 */
class FishController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'index' => ['POST'],
					'create' => ['POST'],
                ],
            ],
        ];
    }


	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
        if (!$session->get('employee_name'))
            $this->redirect(array('site/second'));
    if($action->id == "tabsdata")
        $this->enableCsrfValidation = false;
	if($action->id == "tabscreate")
        $this->enableCsrfValidation = false;
	if($action->id == "savefish")
        $this->enableCsrfValidation = false;
	if($action->id == "viewfishdetails")
        $this->enableCsrfValidation = false;
	if($action->id == "updatefish")
        $this->enableCsrfValidation = false;
	if($action->id == "deletefish")
        $this->enableCsrfValidation = false;
	if($action->id == "tabsupdate")
        $this->enableCsrfValidation = false;
	if($action->id == "deadfish")
        $this->enableCsrfValidation = false;
	if($action->id == "saveaddfish")
        $this->enableCsrfValidation = false;
	if($action->id == "movefish")
        $this->enableCsrfValidation = false;
  if($action->id == "getsecond")
    $this->enableCsrfValidation = false;
	if($action->id == "chkname")
    $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }

    /**
     * Lists all Fish models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FishSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	 public function actionTabsdata() {
		  $searchModel = new FishSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $html = $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        return Json::encode($html);
    }
	public function actionTabscreate() {
		  $model = new Fish();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }
	public function actionTabsupdate() {
		  $model = new Fish();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }

	public function actionViewfishdetails() {
		$post = Yii::$app->request->post();
		 $id = $post['fid'];
       return $html = $this->renderPartial('viewfish', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * Displays a single Fish model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fish();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


	public function actionSavefish()
    {
        $model = new Fish();
        $post = Yii::$app->request->post();
		$session = Yii::$app->session;
	Yii::$app->db->createCommand('INSERT INTO fish (container_id, name, s_name, avg_weight, amount, created_date, updated_date) VALUES (:container_id,:name,:s_name,:avg_weight,:amount, :created_date, :updated_date)', array(
    ':container_id' => $post['cid'],
	':name' => $post['fish_name'],
	':s_name' => $post['s_name'],
	':avg_weight' => $post['avg_weight'],
	':amount' => $post['amount'],
	':created_date'  => $post['fish_created_date'],
	':updated_date' => $post['fish_updated_date']
	))->execute();
     $id = Yii::$app->db->getLastInsertID();
	 $created_by =  $session->get('employee_name');
	 $cid = $post['cid'];
	 $addition = $post['amount'];
	 $created_date = date ("Y-m-d H:i:s");
	 Yii::$app->db->createCommand('INSERT INTO fish_history (f_id, cid, current_num, addition, created_date, created_by) VALUES (:f_id, :cid, :current_num, :addition, :created_date, :created_by)', array(
  ':f_id' => $id,
	':cid' => $cid,
	':current_num' => $post['amount'],
	':addition' => $addition,
	':created_date' => $created_date,
	':created_by' => $created_by
	))->execute();

    }

	public function actionUpdatefish()
    {
        $model = new Fish();
        $post = Yii::$app->request->post();


		Yii::$app->db->createCommand()->update('fish', ['name' => $post['fish_name'], 's_name' => $post['s_name'], 'avg_weight' => $post['avg_weight']], 'fid = "'.$post['fid'].'"')->execute();

    }
  public function actionDeletefish()
    {
		 $post = Yii::$app->request->post();
		 $id  = $post['fid'];
		Yii::$app->db->createCommand('DELETE FROM fish WHERE fid='.$id.'')->execute();
		$this->actionTabsdata();
    }
    /**
     * Updates an existing Fish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Fish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fish::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

	public function actionDeadfish()
    {
        $model = new Fish();
		$session = Yii::$app->session;
        $post = Yii::$app->request->post();
		if(!empty($post['f_id'])){
        $id = $post['f_id'];

        $selQry2 = Yii::$app->db->createCommand("SELECT amount, name FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();

		$existAmount = $result['amount'];
		$name = $result['name'];
		$remainingAmount = $existAmount - $post['d_amount'];
		Yii::$app->db->createCommand()->update('fish', ['amount' => $remainingAmount,  'updated_date' => $post['created_date']], 'fid = "'.$post['f_id'].'"')->execute();

		Yii::$app->db->createCommand('INSERT INTO fish_history (f_id, current_num, death, d_note, death_main_name, death_second_name, cid, created_date, created_by) VALUES (:f_id,  :current_num, :death, :d_note, :death_main_name,:death_second_name, :cid, :created_date, :created_by)', array(
    ':f_id' => $post['f_id'],
	':current_num' => $existAmount,
	':death' => $post['d_amount'],
	':d_note' => $post['d_note'],
  ':death_main_name' => $post['death_main_name'],
  ':death_second_name' => $post['death_second_name'],
	':cid' => $post['cid'],
	':created_date' => $post['created_date'],
	':created_by' => $session->get('employee_name'),
	))->execute();


      Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Reported Death Of ". $post['d_amount']. " ".$name." In ",
	':who_did' => $session->get('employee_name'),
	':container_id' => $post['cid'],
	':action_date' => $post['created_date'],
	))->execute();

		}

}

	public function actionSaveaddfish()
    {
        $model = new Fish();
		$session = Yii::$app->session;
        $post = Yii::$app->request->post();
		if(!empty($post['f_id'])){
        $id = $post['f_id'];

        $selQry2 = Yii::$app->db->createCommand("SELECT amount, name FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();

		$existAmount = $result['amount'];
		$name = $result['name'];
		$remainingAmount = $existAmount + $post['d_amount'];
		Yii::$app->db->createCommand()->update('fish', ['amount' => $remainingAmount,  'updated_date' => $post['created_date']], 'fid = "'.$post['f_id'].'"')->execute();

		Yii::$app->db->createCommand('INSERT INTO fish_history (f_id, current_num, addition, a_note, cid, created_date,created_by) VALUES (:f_id, :current_num,  :addition,:a_note,:cid, :created_date, :created_by)', array(
    ':f_id' => $post['f_id'],
	':current_num' => $existAmount,
	':addition' => $post['d_amount'],
	':a_note' => $post['d_note'],
	':cid' => $post['cid'],
	':created_date' => $post['created_date'],
	':created_by' => $session->get('employee_name'),
	))->execute();


      Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Fish added Of ". $post['d_amount']. " ".$name." In ",
	':who_did' => $session->get('employee_name'),
	':container_id' => $post['cid'],
	':action_date' => $post['created_date'],
	))->execute();

		}

    }

	public function actionMovefish()
    {
        $model = new Fish();
		$session = Yii::$app->session;
        $post = Yii::$app->request->post();
		if(!empty($post['f_id'])){
        $id = $post['f_id'];

        $selQry2 = Yii::$app->db->createCommand("SELECT * FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();

		$existAmount = $result['amount'];
		$name = $result['name'];
		$s_name = $result['s_name'];
		$avg_weight = $result['avg_weight'];

		$remainingAmount = $existAmount - $post['d_amount'];
		Yii::$app->db->createCommand()->update('fish', ['amount' => $remainingAmount,  'updated_date' => $post['created_date']], 'fid = "'.$post['f_id'].'"')->execute();

		Yii::$app->db->createCommand('INSERT INTO fish_history (f_id, current_num, move, m_note, cid, move_cid,move_reason, created_date,created_by) VALUES (:f_id, :current_num, :addition,:a_note, :cid, :move_cid,:move_reason, :created_date, :created_by)', array(
    ':f_id' => $post['f_id'],
	':current_num' => $existAmount,
	':addition' => $post['d_amount'],
	':a_note' => $post['d_note'],
	':cid' => $post['cid'],
	':move_cid' => $post['move_cid'],
	':move_reason' => $post['move_reason'],
	':created_date' => $post['created_date'],
	':created_by' => $session->get('employee_name'),
	))->execute();

	Yii::$app->db->createCommand('INSERT INTO fish (container_id, name, s_name, avg_weight, amount, created_date, updated_date) VALUES (:container_id,:name,:s_name,:avg_weight,:amount, :created_date, :updated_date)', array(
    ':container_id' => $post['move_cid'],
	':name' => $name,
	':s_name' => $s_name,
	':avg_weight' => $avg_weight,
	':amount' => $post['d_amount'],
	':created_date'  => $post['created_date'],
	':updated_date' => $post['created_date']
	))->execute();

      Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Reported Fish Moved Of ". $post['d_amount']. " ".$name." In ",
	':who_did' => $session->get('employee_name'),
	':container_id' => $post['cid'],
	':action_date' => $post['created_date'],
	))->execute();

		}

    }
    public function actionGetsecond(){

       $qry = "";
       $post = Yii::$app->request->post();
       $id = $post['id'];
       $dropdown  = '';
         if($id){
         $qry = " where parent ='".$id."'";
         }
              $selQry2       = Yii::$app->db->createCommand("SELECT * from dead $qry");
              $result = $selQry2->queryAll();
        foreach ($result as $model) {
          $dropdown .= '<option value="'.$model['dead_id'].'">'.$model['dead_name'].'</option>';
        }
        return $dropdown;
      }

	  public function actionChkname(){
              $post = Yii::$app->request->post();
              $names = $post['names'];

              $selQry2 = Yii::$app->db->createCommand("SELECT * FROM fish where name like '%".$names."'");
              $result = $selQry2->queryOne();
			  if($result['name']){
				 return "1" ;
				 }else{
			     return "0";
				 }

      }

}
