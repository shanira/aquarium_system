<?php

namespace app\controllers;

use Yii;
use app\models\Water;
use app\models\WaterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use yii\web\Session;
use yii\helpers\Url;

/**
 * WaterController implements the CRUD actions for Water model.
 */
class WaterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function beforeAction($action) {
		$session = Yii::$app->session;
		if ($session->get('sleep'))
            $this->redirect(array('site/second'));
         if (!$session->get('employee_name'))
            $this->redirect(array('site/second'));
    if($action->id == "index")
        $this->enableCsrfValidation = false;
	if($action->id == "tabscreate")
        $this->enableCsrfValidation = false;
	if($action->id == "savefood")
        $this->enableCsrfValidation = false;
	if($action->id == "waterhistory")
        $this->enableCsrfValidation = false;
	if($action->id == "savewater")
        $this->enableCsrfValidation = false;
	if($action->id == "updatewater")
        $this->enableCsrfValidation = false;
	if($action->id == "deletewater")
        $this->enableCsrfValidation = false;
	if($action->id == "tabsupdate")
        $this->enableCsrfValidation = false;

    return parent::beforeAction($action);
    }
    /**
     * Lists all Water models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WaterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		return Json::encode($html);
    }

    /**
     * Displays a single Water model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Water model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Water();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->wid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Water model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->wid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
   public function actionTabsupdate() {
		  $model = new Water();
         $post = Yii::$app->request->post();
		 $id = $post['fid'];
		 $watertype = $post['watertype'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fid]);
        }

        return $this->renderPartial('update', [
            'model' => $model, 'watertype'  => $watertype,
        ]);
    }
    /**
     * Deletes an existing Water model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Water model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Water the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Water::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

   public function actionSavewater()
    {
           $post = Yii::$app->request->post();
			$session = Yii::$app->session;
			Yii::$app->db->createCommand('INSERT INTO water (container_id, water_level, temperature, co2, no2, no3, po4, hardness, bromines, copper, ca, salinity, ph, alkalinity, tan, uia, do_mg, do, mg, strontium, updated_date) VALUES (:container_id,:water_level, :temperature,:co2,:no2,:no3,:po4, :hardness, :bromines, :copper, :ca, :salinity, :ph, :alkalinity, :tan, :uia, :do_mg, :do, :mg,:strontium, :updated_date)', array(
    ':container_id' => $post['container_id'],
    ':water_level' => $post['water_level'],
    ':temperature' => $post['temperature'],
	':co2' => $post['co2'],
	':no2' => $post['no2'],
	':no3' => $post['no3'],
	':po4' => $post['po4'],
	':hardness' => $post['hardness'],
	':bromines' => $post['bromines'],
	':copper' => $post['copper'],
	':ca'  => $post['ca'],
	':salinity'  => $post['salinity'],
	':ph' => $post['ph'],
	':alkalinity' => $post['alkalinity'],
	':tan' => $post['tan'],
	':uia' => $post['uia'],
	':do_mg' => $post['do_mg'],
	':do' => $post['dos'],
	':mg' => $post['mg'],
  ':strontium' => $post['strontium'],
	':updated_date' => date ("Y-m-d H:i:s"),
	))->execute();


Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Took Water Samples For ",
	':who_did' => $session->get('employee_name'),
	':container_id' => $post['container_id'],
	':action_date' => date ("Y-m-d H:i:s"),
	))->execute();

Yii::$app->db->createCommand('INSERT INTO logs (what_did, who_did, container_id, action_date) VALUES (:what_did,:who_did,:container_id, :action_date)', array(
    ':what_did' => "Updated Water Data For ",
	':who_did' => $session->get('employee_name'),
	':container_id' => $post['container_id'],
	':action_date' => date ("Y-m-d H:i:s"),
	))->execute();

    }

	public function actionWaterhistory() {
		  $model = new Water();
		  $post = Yii::$app->request->post();
      $fid = $post['field_id'];
		  $cid = $post['container_id'];
		  $selQry = Yii::$app->db->createCommand("SELECT $fid, wid, updated_date from water where $fid IS NOT NULL and trim($fid) <> '' and container_id='".$cid."' order by wid DESC LIMIT 0,30");
          $result = $selQry->queryAll();

        return $this->renderPartial('waterhistory', [
            'model' => $result,
        ]);
    }

	public function actionUpdatewater()
    {
        $model = new Water();
		$session = Yii::$app->session;
        $post = Yii::$app->request->post();
		if(!empty($post['wid'])){
        $id = $post['wid'];
		$watertype = $post['watertype'];
		$watervalue = $post['watervalue'];
		Yii::$app->db->createCommand()->update('water', [$watertype => $watervalue], 'wid = "'.$post['wid'].'"')->execute();
     }
	}

	public function actionDeletewater()
    {
		 $post = Yii::$app->request->post();
		 $id  = $post['fid'];
		Yii::$app->db->createCommand('DELETE FROM water WHERE wid='.$id.'')->execute();
		$this->actionIndex();
    }

}
