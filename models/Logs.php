<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property int $logs_id
 * @property string $what_did
 * @property int $who_did
 * @property int $container_id
 * @property string $action_date
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['what_did', 'who_did', 'container_id', 'action_date'], 'required'],
            [['who_did', 'container_id'], 'integer'],
            [['action_date'], 'safe'],
            [['what_did'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'logs_id' => Yii::t('app', 'Logs ID'),
            'what_did' => Yii::t('app', 'What Did'),
            'who_did' => Yii::t('app', 'מי ביצע'),
            'container_id' => Yii::t('app', 'Container ID'),
            'action_date' => Yii::t('app', 'Action Date'),
        ];
    }
	public static function getUser($id){

        $selQry2 = Yii::$app->db->createCommand("SELECT username FROM user where id= '".$id."'");
		$result = $selQry2->queryOne();
			if(!empty($result)){
				return $result['username'];
			}
        return null;
    }
	public static function getContainerName($id){

        $selQry2 = Yii::$app->db->createCommand("SELECT container_name FROM containers where cid= '".$id."'");
		$result = $selQry2->queryOne();
			if(!empty($result)){
				return $result['container_name'];
			}
        return null;
    }
}
