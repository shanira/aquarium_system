<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "food".
 *
 * @property int $food_id
 * @property int $container_id
 * @property string $fish_to
 * @property string $day_of_week
 * @property string $food_type
 * @property string $unit
 * @property string $amount
 * @property string $actual_unit
 * @property string $actual_amount
 * @property string $comment
 * @property string $created_date
 * @property string $updated_date
 * @property int $created_by
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'food_type', 'created_date', 'updated_date', 'unit', 'fish_to', 'food_type', 'amount'], 'required'],
            [['container_id'], 'integer'],
            [['unit', 'actual_unit', 'comment', 'comment_w','created_by'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['fish_to', 'food_type', 'amount', 'actual_amount'], 'string', 'max' => 55],
            [['day_of_week'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'food_id' => Yii::t('app', 'Food ID'),
            'container_id' => Yii::t('app', 'מיכל'),
            'fish_to' => Yii::t('app', 'עבור'),
            'day_of_week' => Yii::t('app', 'יום בשבוע'),
            'food_type' => Yii::t('app', 'סוג מזון'),
            'unit' => Yii::t('app', 'יחידות'),
            'amount' => Yii::t('app', 'כמות'),
            'actual_unit' => Yii::t('app', 'Actual Unit'),
            'actual_amount' => Yii::t('app', 'סטטוס האכלה'),
            'comment' => Yii::t('app', 'הערה'),
            'comment_w' => Yii::t('app', 'הערת מאכיל'),
            'created_date' => Yii::t('app', 'תאריך יצירה'),
            'updated_date' => Yii::t('app', 'Updated Date'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FoodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FoodQuery(get_called_class());
    }

	public function GetFishName($fid){
	          if($fid=='All'){
			  return "All";
			  }else{
		       $selQry = Yii::$app->db->createCommand("SELECT name from fish where fid='".$fid."'");
                $result = $selQry->queryOne();
				 return  $result['name'];
			  }
	}

	public static function containerdropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT cid, container_name FROM containers");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdown[$model['cid']] = $model['container_name'];
    }
    return $dropdown;
   }

   public static function daydropdown() {
    $dropdown = array('Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday');
    return $dropdown;
   }

   public static function unitdropdown() {
    $dropdown = array('גרם'=>'גרם','קילוגרם'=>'קילוגרם','יחידות'=>'יחידות');
    return $dropdown;
   }

	public static function foodtypedropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT foodt_id, food_type FROM food_type");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdownt[$model['foodt_id']] = $model['food_type'];
    }
    return $dropdownt;
   }
}
