<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Food]].
 *
 * @see Food
 */
class DeathCauseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DeathCause[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DeathCause|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
