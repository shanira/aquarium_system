<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fish;

/**
 * FishSearch represents the model behind the search form of `app\models\Fish`.
 */
class FishSearch extends Fish
{
    /**
     * {@inheritdoc}
     */
	 public $amount;
    public function rules()
    {
        return [
            [['fid', 'container_id', 'amount'], 'integer'],
            [['name', 's_name', 'avg_weight', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fish::find()->select('fid, container_id, sum(amount) as amount, name, s_name, avg_weight, created_date,updated_date ');
 
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => false,
			 'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		$session = Yii::$app->session;
        $cid = $session->get('cid');
        // grid filtering conditions
        $query->andFilterWhere([
            'fid' => $this->fid,
            'container_id' => $cid,
			'avg_weight' => $this->avg_weight,
            'amount' => $this->amount,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 's_name', $this->s_name]);
		$query->groupBy('name');

        return $dataProvider;
    }
}
