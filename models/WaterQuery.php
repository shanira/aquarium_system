<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Water]].
 *
 * @see Water
 */
class WaterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Water[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Water|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
