<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Diagnostic;

/**
 * DiagnosticSearch represents the model behind the search form of `app\models\Diagnostic`.
 */
class DiagnosticSearch extends Diagnostic
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['d_id' ], 'integer'],
            [['d_name','parent'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diagnostic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'d_id' => $this->d_id,
			//'d_name' => $this->d_name,
            //'parent' => $this->parent,
        ]);

        $query->andFilterWhere(['like', 'd_name', $this->d_name])
				->andFilterWhere(['like', 'parent', $this->parent]);
				
        return $dataProvider;
    }
}
