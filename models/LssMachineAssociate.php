<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lss_machine_associate".
 *
 * @property int $aid
 * @property int $container_id
 * @property int $lss_attached_id
 * @property string $input_type
 */
class LssMachineAssociate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lss_machine_associate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'lss_attached_id'], 'required'],
            [['container_id', 'lss_attached_id'], 'integer'],
            [['input_type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aid' => Yii::t('app', 'Aid'),
            'container_id' => Yii::t('app', 'מיכל'),
            'lss_attached_id' => Yii::t('app', 'מכונת LSS'),
            'input_type' => Yii::t('app', 'סוג שדה'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LssMachineAssociateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LssMachineAssociateQuery(get_called_class());
    }
}
