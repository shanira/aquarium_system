<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lss_system".
 *
 * @property int $lss_machine_id
 * @property string $lss_machine_name
 */
class LssSystem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lss_system';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lss_machine_name'], 'required'],
            [['lss_machine_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lss_machine_id' => Yii::t('app', 'מזהה LSS'),
            'lss_machine_name' => Yii::t('app', 'שם LSS'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LssSystemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LssSystemQuery(get_called_class());
    }
}
