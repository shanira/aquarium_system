<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vet".
 *
 * @property int $vet_id
 * @property int $container_id
 * @property string $treatment_for
 * @property string $comments
 * @property string $date_created
 */
class Vet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'treatment_for', 'comments', 'date_created'], 'required'],
            [['container_id'], 'integer'],
            [['comments'], 'string'],
            [['date_created'], 'safe'],
            [['treatment_for','treatment','specific_treatment', 'frequency', 'dilution','how_given'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vet_id' => Yii::t('app', 'Vet ID'),
            'container_id' => Yii::t('app', 'Container ID'),
            'treatment_for' => Yii::t('app', 'Treatment For'),
			'treatment' => Yii::t('app', 'Treatment '),
			'specific_treatment' => Yii::t('app', 'Specific Treatment '),
			'frequency' => Yii::t('app', 'Frequency'),
			'dilution' => Yii::t('app', 'Dilution'),
			'how_given' => Yii::t('app', 'How Given'),
            'comments' => Yii::t('app', 'Comments'),
            'date_created' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return VetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VetQuery(get_called_class());
    }

	public function getFishData($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT * from fish where container_id='".$container_id."'");
        return $result = $selQry->queryAll();
	}

  public function getFishDataDrop($cnt){
           $selQry    = Yii::$app->db->createCommand("SELECT DISTINCT name,fid from fish where container_id =$cnt group by 1");
               return $result = $selQry->queryAll();
  }
  
	public static function getFish($id){
		if($id==0){
			return "All Container";
		}else{
        $selQry2 = Yii::$app->db->createCommand("SELECT name FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();
			if(!empty($result)){
				return $result['name'];
			}
		}

        return null;
    }
}
