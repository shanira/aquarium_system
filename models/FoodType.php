<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "lss_system".
 *
 * @property int $lss_machine_id
 * @property string $lss_machine_name
 */
class FoodType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['food_type'], 'required'],
            [['food_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'foodt_id' => Yii::t('app', 'מזהה סוג מזון'),
            'food_type' => Yii::t('app', 'סוג מזון'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FoodTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FoodTypeQuery(get_called_class());
    }
    public static function getFoodType()
	   {
		$allFoodType = self::find()->all();
		$allFoodTypeArray = ArrayHelper::
					map($allFoodType, 'foodt_id', 'food_type');
		return $allFoodTypeArray;
	   }
	   
	   public function getAllFoodType(){
           $selQry = Yii::$app->db->createCommand("SELECT food_type from food_type");
        return $result = $selQry->queryAll();
  }

}
