<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diagnostic".
 *
 * @property int $dead_id
 * @property string $dead_name
 * @property int $parent
 */
class Dead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dead';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dead_name', 'parent'], 'required'],
            [['parent'], 'integer'],
            [['dead_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dead_id' => Yii::t('app', 'מספר'),
            'dead_name' => Yii::t('app', 'סיבת מוות'),
            'parent' => Yii::t('app', 'סיבת מוות ראשית'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DeadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeadQuery(get_called_class());
    }

	public function dropParent(){
		$selQry = Yii::$app->db->createCommand("SELECT * from dead where parent=0");
        $result = $selQry->queryAll();
		$data =  array();
		 $data[0] = "No Parent";
		   foreach($result as $rows){
			$data[$rows['dead_id']] = $rows['dead_name'];
			}
	     return $data;
	}

	public function getParentData(){
		$selQry = Yii::$app->db->createCommand("SELECT * from dead where parent=0");
        $result = $selQry->queryAll();

	     return $result;
	}

	 public static function relationdropdown() {
	   $qry = "";
	   if(Yii::$app->request->get()){
		   $data = Yii::$app->request->get();
		    if(!empty($data['DeadSearch']['parent'])){
		   $web = $data['DeadSearch']['parent'];
		   $qry = " where parent ='".$web."'";
			}
		   }
       $selQry2       = Yii::$app->db->createCommand("SELECT * from dead $qry");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdown[$model['dead_id']] = $model['dead_name'];
    }
    return $dropdown;
   }

   	public function getSecondName($id){
		if($id==0){
		return "";
		}
		$selQry = Yii::$app->db->createCommand("SELECT * from dead where dead_id=$id");
        $result = $selQry->queryOne();

	     return $result['dead_name'];
	}
	public function getSecondDropdown($id){
		    $selQry2       = Yii::$app->db->createCommand("SELECT * from dead where parent = $id");
            return $result = $selQry2->queryAll();

		}
	
	public static function deadmaindropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT dead_id, dead_name, parent FROM dead where parent = 0");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdownt[$model['dead_id']] = $model['dead_name'];
    }
    return $dropdownt;
   }
		public static function diagnoseconddropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT dead_id, dead_name, parent FROM dead");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdowns[$model['dead_name']] = $model['dead_name'];
    }
    return $dropdowns;
   }
}
