<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Food;

/**
 * FoodSearch represents the model behind the search form of `app\models\Food`.
 */
class FoodSearch extends Food
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['food_id', 'container_id', 'comment', 'created_by'], 'integer'],
            [['food_type', 'unit', 'day_of_week', 'amount', 'actual_unit', 'actual_amount', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Food::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'food_id' => $this->food_id,
            'container_id' => $this->container_id,
            'comment' => $this->comment,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'food_type', $this->food_type])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'actual_unit', $this->actual_unit])
			 ->andFilterWhere(['like', 'day_of_week', $this->day_of_week])
            ->andFilterWhere(['like', 'actual_amount', $this->actual_amount]);

        return $dataProvider;
    }
	public function getContainerData($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT * from food where container_id='".$container_id."'");
        return $result = $selQry->queryAll();
	}

	public function getCurrentUpdate($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT updated_date from food where container_id='".$container_id."' order by food_id DESC LIMIT 0,1 ");
        $result = $selQry->queryOne();
		return $result['updated_date'];
	}
	public function getLastUpdate($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT updated_date from food where container_id='".$container_id."' order by food_id DESC LIMIT 1,1 ");
        $result = $selQry->queryOne();
		return $result['updated_date'];
	}

}
