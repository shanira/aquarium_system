<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vetd;

/**
 * VetSearch represents the model behind the search form of `app\models\Vet`.
 */
class VetdSearch extends Vetd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vedt_id', 'container_id'], 'integer'],
            [['diagnostic_for','diagnostic_name','diagnostic_type', 'comments', 'date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vetd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $session = Yii::$app->session;
        $cid = $session->get('cid');
        // grid filtering conditions
        $query->andFilterWhere([
            'vedt_id' => $this->vedt_id,
            'container_id' => $cid,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'diagnostic_for', $this->diagnostic_for])
            ->andFilterWhere(['like', 'diagnostic_name', $this->diagnostic_name])
            ->andFilterWhere(['like', 'diagnostic_type', $this->diagnostic_type])
            ->andFilterWhere(['like', 'comments', $this->comments]);
        $query->orderBy(['(vedt_id)' => SORT_DESC]);
        return $dataProvider;
    }
}
