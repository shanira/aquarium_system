<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LssMachineAssociate]].
 *
 * @see LssMachineAssociate
 */
class LssMachineAssociateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LssMachineAssociate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LssMachineAssociate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
