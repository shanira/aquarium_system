<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Water;

/**
 * WaterSearch represents the model behind the search form of `app\models\Water`.
 */
class WaterSearch extends Water
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wid', 'container_id'], 'integer'],
            [['co2', 'no2', 'no3', 'po4', 'hardness', 'bromines', 'copper', 'ca', 'salinity', 'ph', 'alkalinity', 'tan', 'uia', 'do_mg', 'do', 'mg', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Water::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wid' => $this->wid,
            'container_id' => $this->container_id,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'co2', $this->co2])
            ->andFilterWhere(['like', 'no2', $this->no2])
            ->andFilterWhere(['like', 'no3', $this->no3])
            ->andFilterWhere(['like', 'po4', $this->po4])
            ->andFilterWhere(['like', 'hardness', $this->hardness])
            ->andFilterWhere(['like', 'bromines', $this->bromines])
            ->andFilterWhere(['like', 'copper', $this->copper])
            ->andFilterWhere(['like', 'ca', $this->ca])
            ->andFilterWhere(['like', 'salinity', $this->salinity])
            ->andFilterWhere(['like', 'ph', $this->ph])
            ->andFilterWhere(['like', 'alkalinity', $this->alkalinity])
            ->andFilterWhere(['like', 'tan', $this->tan])
            ->andFilterWhere(['like', 'uia', $this->uia])
            ->andFilterWhere(['like', 'do_mg', $this->do_mg])
            ->andFilterWhere(['like', 'do', $this->do])
            ->andFilterWhere(['like', 'mg', $this->mg]);

        return $dataProvider;
    }
}
