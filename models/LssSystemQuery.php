<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LssSystem]].
 *
 * @see LssSystem
 */
class LssSystemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LssSystem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LssSystem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
