<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "containers".
 *
 * @property int $cid
 * @property string $container_name
 * @property string $created_date
 */
class Containers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'containers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_name', 'created_date'], 'required'],
            [['created_date'], 'safe'],
            [['container_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cid' => Yii::t('app', 'מזהה מיכל'),
            'container_name' => Yii::t('app', 'שם המיכל'),
            'created_date' => Yii::t('app', 'תאריך הוספה'),
        ];
    }
}
