<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Vetd]].
 *
 * @see Vetd
 */
class VetdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vetd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vetd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
