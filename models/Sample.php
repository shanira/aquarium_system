<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sample".
 *
 * @property int $sample_id
 * @property int $container_id
 * @property string $sample_name
 * @property int $sample_value
 * @property string $comment
 * @property string $updated_date
 */
class Sample extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sample';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'sample_name', 'sample_value', 'updated_date'], 'required'],
            [['container_id'], 'integer'],
            [['updated_date'], 'safe'],
            [['sample_name', 'sample_value', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sample_id' => Yii::t('app', 'Sample ID'),
            'container_id' => Yii::t('app', 'מיכל'),
            'sample_name' => Yii::t('app', 'סוג דגימה'),
            'sample_value' => Yii::t('app', 'ערך דגימה'),
            'comment' => Yii::t('app', 'הערה'),
            'updated_date' => Yii::t('app', 'תאריך'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return SampleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SampleQuery(get_called_class());
    }

	public static function sampledropdown() {
    $dropdown = array('Alkalinity'=>'Alkalinity',
      'Bromines'=>'Bromines',
      'Ca'=>'Ca',
      'Copper'=>'Copper',
      'CO2'=>'CO2',
      'DO %'=>'DO %',
      'DO mg/l'=>'DO mg/l',
      'Hardness'=>'Hardness',
      'Mg'=>'Mg',
      'NO2'=>'NO2',
      'NO3'=>'NO3',
      'PO4'=>'PO4',
      'pH'=>'pH',
      'Salinity'=>'Salinity',
      'Strontium'=>'Strontium',
      'TAN'=>'TAN',
	'UIA'=>'UIA');
    return $dropdown;
   }

}
