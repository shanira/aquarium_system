<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vet_diag".
 *
 * @property int $vedt_id
 * @property int $container_id
 * @property string $diagnostic_for
 * @property string $comments
 * @property string $date_created
 */
class Vetd extends \yii\db\ActiveRecord
{
    public $upload_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vet_diagnostic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'diagnostic_for', 'diagnostic_name','diagnostic_type' , 'comments', 'date_created'], 'required'],
            [['container_id'], 'integer'],
            [['comments'], 'string'],
            [['date_created'], 'safe'],
            [['diagnostic_for','diagnostic_name','diagnostic_type', 'image'], 'string', 'max' => 255],
            [['upload_file'],'file', 'skipOnEmpty' => true ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vedt_id' => Yii::t('app', 'Vetd ID'),
            'container_id' => Yii::t('app', 'Container ID'),
            'diagnostic_for' => Yii::t('app', 'Diagnostic For'),
            'diagnostic_name' => Yii::t('app', 'Diagnostic Name'),
            'diagnostic_type' => Yii::t('app', 'Diagnostic Type'),
            'comments' => Yii::t('app', 'Comments'),
            'date_created' => Yii::t('app', 'Date'),
            'image' => Yii::t('app', 'Image'),
            'upload_file' =>  Yii::t('app','Upload File')
        ];
    }

    /**
     * {@inheritdoc}
     * @return VetdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VetdQuery(get_called_class());
    }
    

	public function getFishData($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT * from fish where container_id='".$container_id."'");
        return $result = $selQry->queryAll();
	}

  public function getFishDataDrop($cnt){
           $selQry    = Yii::$app->db->createCommand("SELECT DISTINCT name,fid from fish where container_id =$cnt group by 1");
               return $result = $selQry->queryAll();
  }

	public static function getFish($id){
		if($id==0){
			return "All Container";
		}else{
        $selQry2 = Yii::$app->db->createCommand("SELECT name FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();
			if(!empty($result)){
				return $result['name'];
			}
		}

        return null;
    }

	public static function getDiagonoName($id){
		$selQry = Yii::$app->db->createCommand("SELECT d_name from diagnostic where d_id='".$id."'");
        $result = $selQry->queryOne();
		if(!empty($result['d_name'])){
		return $result['d_name'];
		}else{
			return "";
		}
	}
}
