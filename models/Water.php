<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "water".
 *
 * @property int $wid
 * @property int $container_id
 * @property string $co2
 * @property string $no2
 * @property string $no3
 * @property string $po4
 * @property string $hardness
 * @property string $bromines
 * @property string $copper
 * @property string $ca
 * @property string $salinity
 * @property string $ph
 * @property string $alkalinity
 * @property string $tan
 * @property string $uia
 * @property string $do_mg
 * @property string $do
 * @property string $mg
 * @property string $updated_date
 */
class Water extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'water';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'updated_date'], 'required'],
            [['container_id'], 'integer'],
            [['updated_date'], 'safe'],
            [['water_level','temperature','co2', 'no2', 'no3', 'po4', 'hardness', 'bromines', 'copper', 'ca', 'salinity', 'ph', 'alkalinity', 'tan', 'uia', 'do_mg', 'do', 'mg','strontium'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'wid' => Yii::t('app', 'Wid'),
            'container_id' => Yii::t('app', 'Container ID'),
            'water_level' => Yii::t('app', 'Water Level'),
            'temperature' => Yii::t('app', 'Temperature'),
            'co2' => Yii::t('app', 'Co2'),
            'no2' => Yii::t('app', 'No2'),
            'no3' => Yii::t('app', 'No3'),
            'po4' => Yii::t('app', 'Po4'),
            'hardness' => Yii::t('app', 'Hardness'),
            'bromines' => Yii::t('app', 'Bromines'),
            'copper' => Yii::t('app', 'Copper'),
            'ca' => Yii::t('app', 'Ca'),
            'salinity' => Yii::t('app', 'Salinity'),
            'ph' => Yii::t('app', 'Ph'),
            'alkalinity' => Yii::t('app', 'Alkalinity'),
            'tan' => Yii::t('app', 'Tan'),
            'uia' => Yii::t('app', 'Uia'),
            'do_mg' => Yii::t('app', 'Do Mg'),
            'do' => Yii::t('app', 'Do'),
            'mg' => Yii::t('app', 'Mg'),
            'strontium' => Yii::t('app', 'Strontium'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }

	public function getWaterLastData($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT * from water where container_id='".$container_id."' AND DATE(updated_date)= '".date('Y-m-d')."' order by wid desc limit 0,1");
        return $result = $selQry->queryOne();
	}
}
