<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dead;

/**
 * DiagnosticSearch represents the model behind the search form of `app\models\Diagnostic`.
 */
class DeadSearch extends Dead
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dead_id', 'parent'], 'integer'],
            [['dead_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dead::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'dead_id' => $this->dead_id,
			//'dead_name' => $this->dead_name,
            //'parent' => $this->parent,
        ]);

        $query->andFilterWhere(['like', 'dead_name', $this->dead_name])
		->andFilterWhere(['like', 'parent', $this->parent]);
				

        return $dataProvider;
    }
}
