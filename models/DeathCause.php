<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "food".
 *
 * @property int $death_id
 * @property string $death_main_name

 */
class DeathCause extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'death_cause';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['death_id', 'death_main_name'], 'required'],
            [['death_id'], 'integer'],
            [['death_main_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'death_id' => Yii::t('app', 'Death ID'),
            'death_main_name' => Yii::t('app', 'Death main'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DeathCauseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeathCauseQuery(get_called_class());
    }


/*no need this function*/

	public function getDeathCause(){
		       $selQry = Yii::$app->db->createCommand("SELECT death_main_name from death_cause");
                $result = $selQry->queryOne();
				 return  $result['death_main_name'];
			  }

}
