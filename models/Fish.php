<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fish".
 *
 * @property int $fid
 * @property int $container_id
 * @property string $name
 * @property string $s_name
 * @property int $amount
 * @property string $created_date
 * @property string $updated_date
 */
class Fish extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'name', 's_name', 'amount', 'created_date', 'updated_date'], 'required'],
            [['container_id', 'amount'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['name'], 'string', 'max' => 55],
            [['s_name'], 'string', 'max' => 255],
			[['avg_weight'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fid'          => Yii::t('app', 'Fid'),
            'container_id' => Yii::t('app', 'Container ID'),
            'name'         => Yii::t('app', 'Fish Name'),
            's_name'       => Yii::t('app', 'Scientific Name'),
			       'avg_weight'   => Yii::t('app', 'Avg Weight'),
            'amount'       => Yii::t('app', 'כמות'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }
	public function getContainerData(){
		       $selQry = Yii::$app->db->createCommand("SELECT * from containers");
        return $result = $selQry->queryAll();
	}

  public function getDeathData(){
           $selQry = Yii::$app->db->createCommand("SELECT * from death_cause");
        return $result = $selQry->queryAll();
  }
  public function getDeathSecondData(){
           $selQry = Yii::$app->db->createCommand("SELECT * from death_explain");
        return  $result = $selQry->queryAll();
  }

	public function getFishReport(){
		 $post = Yii::$app->request->post();
		 if($post){
		 if(!empty($post['container']) && empty($post['reason']) && empty($post['start'])){
			 $container  = $post['container'];
			 $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name, f.amount, h.* from fish_history h LEFT JOIN fish f ON f.fid = h.f_id LEFT JOIN containers c ON c.cid = f.container_id where h.cid=$container order by h.created_date DESC");
          return $result = $selQry->queryAll();

		  }
		     $sql       = "";
		  if(!empty($post['reason']) && empty($post['container'])){
			 $move      = $post['reason'];
		  }
		   if(!empty($post['reason']) && !empty($post['container']) && !empty($post['start'])){
			   if($post['reason']=='all_reason'){
				 $sql      .="";
			  }else{
				$move      = $post['reason'];
				 $sql      .= "  AND ".$move." IS NOT NULL ";
			  }
			 if($post['container']=='all_container'){
			 $sql      .= "";
			 }else{
			 $container = $post['container'];
			 $sql      .= " AND  h.cid=$container ";
			 }

			 $start     = $post['start'];
			 $end       = $post['end'];
			 $sql      .= "  AND h.created_date BETWEEN '$start' AND '$end'";
		  }
		  if(!empty($post['reason']) && !empty($post['container']) && empty($post['start'])){
			 if($post['reason']=='all_reason'){
				 $sql      .="";
			  }else{
				$move      = $post['reason'];
				 $sql      .= "  AND ".$move." IS NOT NULL ";
			  }
			 if($post['container']=='all_container'){
			 $sql      .= "";
			 }else{
			 $container = $post['container'];
			 $sql      .= " AND  h.cid=$container ";
			 }

             if(!empty($post['fish_to'])){
				 $fish = $post['fish_to'];
				 $sql      .= " AND  f.name= '".$fish."' ";
			 }


			// $sql      .= " AND  h.cid=$container AND ".$move." IS NOT NULL ";

		  }
		   if(!empty($post['comments'])){
				 $comments = $post['comments'];
				 $sql      .= " AND  (h.d_note like '%".$comments."%' OR h.m_note like '%".$comments."%' OR h.a_note like '%".$comments."%') ";

			 }

		     $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name, f.amount, h.* from fish_history h LEFT JOIN fish f ON f.fid = h.f_id LEFT JOIN containers c ON c.cid = f.container_id where 1 $sql");
          return $result = $selQry->queryAll();
		 }else{
			 $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name, f.amount, h.* from fish_history h LEFT JOIN fish f ON f.fid = h.f_id LEFT JOIN containers c ON c.cid = f.container_id ");
          return $result = $selQry->queryAll();
			 }

	}


		public function getFishAmountReport(){
		 $post = Yii::$app->request->post();
		 if($post){
		     $sql       = "";
			 if(!empty($post['container'])){
				 if($post['container']=='all_container'){
			        $sql      .= "";
				 }else{
				 $container = $post['container'];
				 $sql      .= " AND  f.container_id=$container ";
				 }
			 }

             if(!empty($post['fish_to'])){
				 $fish = $post['fish_to'];
				 $sql      .= " AND  f.name= '".$fish."' ";
			 }


		     $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name, f.amount, f.updated_date from fish f LEFT JOIN containers c ON c.cid = f.container_id where 1 $sql group by f.name, f.container_id");
          return $result = $selQry->queryAll();
		 }else{
			 $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name, f.amount, f.updated_date from fish f LEFT JOIN containers c ON c.cid = f.container_id group by f.name, f.container_id");
          return $result = $selQry->queryAll();
			 }

	}


	public function getFishWeightReport(){
		 $post = Yii::$app->request->post();
		 if($post){
		     $sql       = "";
			 if(!empty($post['container'])){
				 if($post['container']=='all_container'){
			        $sql      .= "";
				 }else{
				 $container = $post['container'];
				 $sql      .= " AND  f.container_id=$container ";
				 }
			 }

             if(!empty($post['fish_to'])){
				 $fish = $post['fish_to'];
				 $sql      .= " AND  f.name= '".$fish."' ";
			 }

			 if(!empty($post['amount'])){
				 $amt = $post['amount'];
				 $sql      .= " ORDER BY f.`amount` ".$amt." ";
			 }

			if(!empty($post['awg_weight'])){
				 $awg = $post['awg_weight'];
				 $sql      .= " ORDER BY f.`avg_weight` ".$awg." ";
			 }
		     $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.* from fish f LEFT JOIN containers c ON c.cid = f.container_id where 1 $sql ");
          return $result = $selQry->queryAll();
		 }else{
			 $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.* from fish f LEFT JOIN containers c ON c.cid = f.container_id");
          return $result = $selQry->queryAll();
			 }

	}


	public function getVetReport(){
		    $post        = Yii::$app->request->post();
			if($post){

		     $sql       = "";

		   if(!empty($post['container'])){
			 if($post['container']=='all_container'){
			 $container = " ";
			 }else{
			 $container = " AND f.container_id=".$post['container'];
			 }

		  }
		  if(!empty($post['start'])){
			$start     = $post['start'];
			 $end       = $post['end'];
			 $sql      .= "  $container AND v.date_created BETWEEN '$start' AND '$end'";
		  }

		  if(!empty($post['fish_to'])){
			 $fish_to     = $post['fish_to'];
			 $sql      .= " AND  f.name = '$fish_to'";
		  }

		  		  if(!empty($post['treatment'])){
			 $treatment     = $post['treatment'];
			 $sql      .= " AND  v.treatment = '$treatment'";
		  }

		  if(!empty($post['specific_treatment'])){
			 $specific_treatment     = $post['specific_treatment'];
			 $sql      .= " AND  v.specific_treatment = '$specific_treatment'";
		  }


		  if(!empty($post['emp'])){
			 $emp     = $post['emp'];
			 $sql      .= " AND  v.created_by = '$emp'";
		  }

		   if(!empty($post['comments'])){
			 $emp     = $post['comments'];
			 $sql      .= " AND  v.comments = '$comments'";
		  }

		     $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, f.name, v.* from vet v LEFT JOIN fish f ON f.fid = v.treatment_for LEFT JOIN containers c ON c.cid = f.container_id where 1 $sql order by vet_id");
          return $result = $selQry->queryAll();
			}else{
			$selQry    = Yii::$app->db->createCommand("SELECT c.container_name, f.name, v.* from vet v LEFT JOIN fish f ON f.fid = v.treatment_for LEFT JOIN containers c ON c.cid = f.container_id  order by vet_id");
          return $result = $selQry->queryAll();
			}
	}

	public function getWaterReport(){
		    $post        = Yii::$app->request->post();
			if($post){

		     $sql       = "";

		   if(!empty($post['container'])){
			 if($post['container']=='all_container'){
			 $sql .= " ";
			 }else{
			 $sql .= " AND w.container_id=".$post['container'];
			 }
		  }
		  if(!empty($post['start'])){
			 $start     = $post['start'];
			 $end       = $post['end'];
			 $sql      .= " AND  w.updated_date BETWEEN '$start' AND '$end'";
		  }

		   if(!empty($post['sys'])){
			 $sys     = $post['sys'];
			 $sql      .= " AND  $sys != ''";
		  }
		     $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, w.* from water w LEFT JOIN containers c ON c.cid = w.container_id where 1 $sql order by wid");
              return $result = $selQry->queryAll();
	   }else{
		   $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, w.* from water w LEFT JOIN containers c ON c.cid = w.container_id group by updated_date");
          return $result = $selQry->queryAll();
		 }

	}

	public function getFoodReport(){
		    $post        = Yii::$app->request->post();
			if($post){
		     $sql       = "";
		   if(!empty($post['container'])){
			 if($post['container']=='all_container'){
			 $sql      .= " ";
			 }else{
			 $sql      .= " AND w.container_id=".$post['container'];
			 }
		  }
		   if(!empty($post['start'])){
			 $start     = $post['start']." 00:00:46";
			 $end       = $post['end']." 23:59:46";
			 $sql      .= "  AND w.updated_date BETWEEN '$start' AND '$end'";
		  }

		  if(!empty($post['fish_to'])){
			 $fish_to     = $post['fish_to'];
			 $sql      .= " AND f.name = '$fish_to'";
		  }

		  if(!empty($post['week_day'])){
			 $week_day     = $post['week_day'];
			 $sql      .= " AND w.day_of_week = '$week_day'";
		  }

		  if(!empty($post['emp'])){
			 $emp     = $post['emp'];
			 $sql      .= " AND w.food_type = '$emp'";
		  }

		   if(!empty($post['unit'])){
			 $unit     = $post['unit'];
			 $sql      .= " AND w.unit = '".$unit."'";
		  }

		   if(!empty($post['actual_amount'])){
			 $actual_amount     = $post['actual_amount'];
			 $sql      .= " AND w.actual_amount = '$actual_amount'";
		  }

		 $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, ft.food_type as fttype, f.name, w.* from food_history w LEFT JOIN containers c ON c.cid = w.container_id  LEFT JOIN fish f ON f.fid = w.fish_to LEFT JOIN food_type ft ON ft.foodt_id = w.food_type where 1 $sql order by created_date desc");
          return $result = $selQry->queryAll();

		  }else{
		    $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, ft.food_type as fttype, f.name, w.* from food_history w LEFT JOIN containers c ON c.cid = w.container_id  LEFT JOIN fish f ON f.fid = w.fish_to LEFT JOIN food_type ft ON ft.foodt_id = w.food_type order by created_date desc");
          return $result = $selQry->queryAll();
		 }
	}

		public function getLssReport(){
		    $post        = Yii::$app->request->post();
			if($post){
				$sql       = "";


		   if(!empty($post['container'])){
			 if($post['container']=='all_container'){
			 $sql .= " ";
			 }else{
			 $sql .= " AND l.container_id=".$post['container'];
			 }
		  }
		  if(!empty($post['start'])){
			 $start     = $post['start'];
			 $end       = $post['end'];
			 $sql      .= " AND l.date_modified BETWEEN '$start' AND '$end'";
		  }

		   if(!empty($post['lssmachine'])){
			 $lssmachine     = $post['lssmachine'];
			 $sql      .= " AND l.lss_machine = '$lssmachine' ";

		  }

      if(!empty($post['comments'])){
        $comments = $post['comments'];
        $sql      .= " AND  (l.machine_value like '%".$comments."%') ";
      }

		     $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, l.*, lm.lss_machine_name from lss l LEFT JOIN lss_system lm ON lm.lss_machine_id = l.lss_machine LEFT JOIN containers c ON c.cid = l.container_id where 1 $sql order by lss_id");
          return $result = $selQry->queryAll();
			}else{
		    $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, l.*, lm.lss_machine_name from lss l LEFT JOIN lss_system lm ON lm.lss_machine_id = l.lss_machine LEFT JOIN containers c ON c.cid = l.container_id  order by lss_id");
          return $result = $selQry->queryAll();
		 }
	}

	public function getFishData(){
		    $post        = Yii::$app->request->post();
			if($post){
				$cnt     = $post['fid'];
		        $selQry    = Yii::$app->db->createCommand("SELECT fid, name from fish where container_id =$cnt");
                $result = $selQry->queryAll();
				$html = "";
				$html = '<option value="">Select Fish</option>>';
				foreach($result as $rows){
					$html .='<option value="'.$rows['fid'].'">'.$rows['name'].'</option>';
				}
				return $html;
			}
	 }

	 public function getFishDatas($cnt){
		        $selQry    = Yii::$app->db->createCommand("SELECT DISTINCT name from fish where container_id =$cnt");
                return $result = $selQry->queryAll();
	 }

	 public function getFishDataz(){
		        $selQry    = Yii::$app->db->createCommand("SELECT DISTINCT name from fish group by name");
                return $result = $selQry->queryAll();
	 }

	 public function getVetdiagnoReport(){
		 $post = Yii::$app->request->post();
		 if($post){
		     $sql       = "";
			 if(!empty($post['container'])){
				 if($post['container']=='all_container'){
			        $sql      .= "";
				 }else{
				 $container = $post['container'];
				 $sql      .= " AND  v.container_id=$container ";
				 }
			 }

             if(!empty($post['fish_to'])){
				 $fish = $post['fish_to'];
				 $sql      .= " AND  f.name= '".$fish."' ";
			 }
			 if(!empty($post['diagonsticname'])){
				 $diagonsticname = $post['diagonsticname'];
				 $sql      .= " AND  v.diagnostic_name= ".$diagonsticname." ";
			 }

			 if(!empty($post['dtype'])){
				 $dtype = $post['dtype'];
				 $sql      .= " AND  v.diagnostic_type= ".$dtype." ";
			 }
			 if(!empty($post['emp'])){
				 $emp = $post['emp'];
				 $sql      .= " AND  v.created_by= '".$emp."' ";
			 }
			 if(!empty($post['emp'])){
				 $emp = $post['emp'];
				 $sql      .= " AND  v.created_by= '".$emp."' ";
			 }
			 if(!empty($post['comments'])){
				 $comments = $post['comments'];
				 $sql      .= " AND  (v.comments like '%".$comments."%') ";
			 }
			 if(!empty($post['start'])){
				 $start = $post['start'];
				 $sql      .= " AND  v.date_created = '".$start."' ";
			 }


		     $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name,d.d_name, v.* from vet_diagnostic v LEFT JOIN containers c ON c.cid = v.container_id LEFT JOIN diagnostic d ON d.d_id = v.diagnostic_name LEFT JOIN fish f ON f.fid = v.diagnostic_for where 1 $sql order by v.vedt_id");
          return $result = $selQry->queryAll();
		 }else{
			 $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, f.name,d.d_name, v.* from vet_diagnostic v LEFT JOIN containers c ON c.cid = v.container_id LEFT JOIN diagnostic d ON d.d_id = v.diagnostic_name LEFT JOIN fish f ON f.fid = v.diagnostic_for order by v.vedt_id");
          return $result = $selQry->queryAll();
			 }

	}

	 public function getDiagnosticType($cnt){
		        $selQry    = Yii::$app->db->createCommand("SELECT d_name from diagnostic where d_id =$cnt");
                $result = $selQry->queryOne();
				return $result['d_name'];
	 }

	 public function getDiagostic(){
		     $selQry    = Yii::$app->db->createCommand("SELECT d_id, d_name from diagnostic where parent=0");
             return $result = $selQry->queryAll();
		 }

	public function getdiagonstictype(){
		    $post        = Yii::$app->request->post();
			if($post){
				$cnt     = $post['fid'];
		        $selQry    = Yii::$app->db->createCommand("SELECT d_id, d_name from diagnostic where parent =$cnt");
                $result = $selQry->queryAll();
				$html = "";
				$html = '<option value="">Select Diagonostic Type</option>>';
				foreach($result as $rows){
					$html .='<option value="'.$rows['d_id'].'">'.$rows['d_name'].'</option>';
				}
				return $html;
			}
	 }

	public function getdiagonstictypes($cnt){
		        $selQry    = Yii::$app->db->createCommand("SELECT d_id, d_name from diagnostic where parent=$cnt");
                return $result = $selQry->queryAll();
	 }

	 public function getdiagonstictypez(){
		        $selQry    = Yii::$app->db->createCommand("SELECT d_id, d_name from diagnostic where parent !=0");
                return $result = $selQry->queryAll();
	 }
	 public function getEmployee(){
		     $selQry    = Yii::$app->db->createCommand("SELECT name from employee");
             return $result = $selQry->queryAll();
	 }
	 public function getFoodType(){
		     $selQry    = Yii::$app->db->createCommand("SELECT foodt_id, food_type from food_type");
             return $result = $selQry->queryAll();
	 }
	 public function getLssMeachineData(){
		       $selQry = Yii::$app->db->createCommand("SELECT * from lss_system");
        return $result = $selQry->queryAll();
	}

	public function getDeadNamebyId($id){
		       $selQry = Yii::$app->db->createCommand("SELECT * from dead where dead_id='".$id."'");
              $result = $selQry->queryOne();
			  if(!empty($result['dead_name'])){
			  return $result['dead_name'];
			  }
	}

	public function lssmachinedrop(){
		    $post        = Yii::$app->request->post();
			if($post){
				$cnt     = $post['cid'];
		        $selQry    = Yii::$app->db->createCommand("SELECT s.lss_machine_id, s.lss_machine_name from lss_system s LEFT JOIN lss_machine_associate a ON a.lss_attached_id = s.lss_machine_id  where a.container_id  =$cnt");
                $result = $selQry->queryAll();
				$html = "";
				$html = '<option value="">Select Machine</option>>';
				foreach($result as $rows){
					$html .='<option value="'.$rows['lss_machine_id'].'">'.$rows['lss_machine_name'].'</option>';
				}
				return $html;
			}
	 }




	 public function getSampleReport(){
		    $post        = Yii::$app->request->post();
			if($post){

		     $sql       = "";

		   if(!empty($post['container'])){
			 if($post['container']=='all_container'){
			 $sql .= " ";
			 }else{
			 $sql .= " AND w.container_id=".$post['container'];
			 }
		  }
		  if(!empty($post['start'])){
			 $start     = $post['start'];
			 $end       = $post['end'];
			 $sql      .= " AND  w.updated_date BETWEEN '$start' AND '$end'";
		  }

		   if(!empty($post['sys'])){
			 $sys     = $post['sys'];
			 $sql      .= " AND sample_name  = '$sys'";
		  }
		     $selQry    = Yii::$app->db->createCommand("SELECT c.container_name, w.* from sample w LEFT JOIN containers c ON c.cid = w.container_id where 1 $sql order by sample_id");
              return $result = $selQry->queryAll();
	   }else{
		   $selQry     = Yii::$app->db->createCommand("SELECT c.container_name, w.* from sample w LEFT JOIN containers c ON c.cid = w.container_id group by updated_date");
          return $result = $selQry->queryAll();
		 }

	}


}
