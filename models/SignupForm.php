<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends User
{
    public $username;
    public $email;
    public $password;
	 public $fullname;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
			
			
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email address has already been taken.'],
            ['security_question', 'required'],
            ['fullname', 'required'],
			['status', 'required'],
            ['password', 'string', 'min' => 6],
			['status', 'required'],

        ];
    }

 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
		$user->fullname = $this->fullname;
        $user->email = $this->email;
        $user->password = $this->password;
		$user->security_question = $this->security_question;
		$user->status = $this->status;
		
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
	 
}
