<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Containers;

/**
 * ContainersSearch represents the model behind the search form of `app\models\Containers`.
 */
class ContainersSearch extends Containers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cid'], 'integer'],
            [['container_name', 'created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Containers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cid' => $this->cid,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'container_name', $this->container_name]);

        return $dataProvider;
    }
}
