<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeathCause;

/**
 * FoodSearch represents the model behind the search form of `app\models\Food`.
 */
class DeathCauseSearch extends DeathCause
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['death_id'], 'integer'],
            [['death_main_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeathCause::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'death_id' => $this->death_id,

        ]);

        $query->andFilterWhere(['like','death_main_name' => $this->death_main_name]);

        return $dataProvider;
    }
	public function getDeathCauseData($death_id){
		$selQry = Yii::$app->db->createCommand("SELECT * from death_cause where death_id='".$death_id."'");
        return $result = $selQry->queryAll();
	}

}
