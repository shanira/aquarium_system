<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $passwd
 * @property string $name
 * @property string $email
 * @property string $notes
 * @property integer $admin
 * @property integer $deactivated
 * @property integer $can_manage_users
 * @property integer $can_manage_groups
 * @property string $surveillance
 * @property string $faculty
 * @property integer $faculty_id
 * @property integer $webteam_id
 * @property string $created_by
 * @property string $created
 * @property string $phoneno
 * @property integer $is_admin
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'required'],
            [['notes', 'surveillance'], 'string'],
            [['admin', 'deactivated', 'can_manage_users', 'can_manage_groups', 'faculty_id', 'webteam_id', 'is_admin'], 'integer'],
            [['created'], 'safe'],
            [['login', 'created_by'], 'string', 'max' => 31],
            [['passwd'], 'string', 'max' => 63],
            [['name', 'email'], 'string', 'max' => 127],
            [['faculty'], 'string', 'max' => 255],
            [['phoneno'], 'string', 'max' => 20],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'passwd' => Yii::t('app', 'Passwd'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'notes' => Yii::t('app', 'Notes'),
            'admin' => Yii::t('app', 'Admin'),
            'deactivated' => Yii::t('app', 'Deactivated'),
            'can_manage_users' => Yii::t('app', 'Can Manage Users'),
            'can_manage_groups' => Yii::t('app', 'Can Manage Groups'),
            'surveillance' => Yii::t('app', 'Surveillance'),
            'faculty' => Yii::t('app', 'Faculty'),
            'faculty_id' => Yii::t('app', 'Faculty ID'),
            'webteam_id' => Yii::t('app', 'Webteam ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created' => Yii::t('app', 'Created'),
            'phoneno' => Yii::t('app', 'Phoneno'),
            'is_admin' => Yii::t('app', 'Is Admin')
			
        ];
    }
	public function countEditorperFaculty(){
		 $date = date('Y-m-d');
		 $selQry = Yii::$app->db->createCommand("SELECT count(wid) as num from ext_users_history where date_created='".$date."'");
		 $result = $selQry->queryOne();

		 	if($result['num']>0){
			//die("debugging.....");//
			}else{
				 Yii::$app->db->createCommand('INSERT INTO ext_users_history (date_created) VALUES (:date)', array(
	':date' => $date,
))->execute();
        $selQry2 = Yii::$app->db->createCommand("SELECT count(faculty_id) as total, faculty_id FROM users group by faculty_id");
		 foreach($result  = $selQry2->queryAll() as $data){
			if($data['faculty_id']==1){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_1' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==3){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_3' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==5){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_5' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==7){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_7' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==9){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_9' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==11){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_11' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==13){
				Yii::$app->db->createCommand()->update('ext_users_history', ['fid_13' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
		 }
		}

    }

	public function countImagesFaculty(){
		 $date = date('Y-m-d');
		 $selQry = Yii::$app->db->createCommand("SELECT count(wid) as num from ext_images_faculty_history where date_created='".$date."'");
		 $result = $selQry->queryOne();

		 	if($result['num']>0){
			//die("debugging.....");//
			}else{
				 Yii::$app->db->createCommand('INSERT INTO ext_images_faculty_history (date_created) VALUES (:date)', array(
	':date' => $date,
))->execute();

        $selQry2 = Yii::$app->db->createCommand("SELECT count(u.faculty_id) as total, u.faculty_id FROM documents d LEFT JOIN users u ON d.owner = u.id where type=15 group by u.faculty_id");
		 foreach($result  = $selQry2->queryAll() as $data){
			if($data['faculty_id']==1){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_1' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==3){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_3' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==5){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_5' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==7){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_7' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==9){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_9' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==11){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_11' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==13){
				Yii::$app->db->createCommand()->update('ext_images_faculty_history', ['fid_13' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
		 }
		}

    }

	public function countImagesWebteam(){
		 $date = date('Y-m-d');
		 $selQry = Yii::$app->db->createCommand("SELECT count(wid) as num from ext_images_webteam_history where date_created='".$date."'");
		 $result = $selQry->queryOne();

		 	if($result['num']>0){
			//die("debugging.....");//
			}else{
				 Yii::$app->db->createCommand('INSERT INTO ext_images_webteam_history (date_created) VALUES (:date)', array(
	':date' => $date,
))->execute();

        $selQry2 = Yii::$app->db->createCommand("SELECT count(u.faculty_id) as total, u.faculty_id FROM documents d LEFT JOIN users u ON d.owner = u.id where type=15 group by u.faculty_id");
		 foreach($result  = $selQry2->queryAll() as $data){
			if($data['faculty_id']==1){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_1' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==3){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_3' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==5){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_5' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==7){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_7' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==9){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_9' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==11){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_11' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
			if($data['faculty_id']==13){
				Yii::$app->db->createCommand()->update('ext_images_webteam_history', ['fid_13' =>$data['total']], 'date_created = "'.$date.'"')->execute();
			}
		 }
		}

    }


}
