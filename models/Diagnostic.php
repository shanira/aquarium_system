<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diagnostic".
 *
 * @property int $d_id
 * @property string $d_name
 * @property int $parent
 */
class Diagnostic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diagnostic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['d_name', 'parent'], 'required'],
            [['parent'], 'integer'],
            [['d_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'd_id' => Yii::t('app', 'מספר'),
            'd_name' => Yii::t('app', 'אבחנה משנית'),
            'parent' => Yii::t('app', 'אבחנה ראשית'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DiagnosticQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiagnosticQuery(get_called_class());
    }

	public function dropParent(){
		$selQry = Yii::$app->db->createCommand("SELECT * from diagnostic where parent=0");
        $result = $selQry->queryAll();
		$data =  array();
		 $data[0] = "No Parent";
		   foreach($result as $rows){
			$data[$rows['d_id']] = $rows['d_name'];
			}
	     return $data;
	}

	public function getParentData(){
		$selQry = Yii::$app->db->createCommand("SELECT * from diagnostic where parent=0");
        $result = $selQry->queryAll();

	     return $result;
	}

	 public static function relationdropdown() {
	   $qry = "";
	   if(Yii::$app->request->get()){
		   $data = Yii::$app->request->get();
		    if(!empty($data['DiagnosticSearch']['parent'])){
		   $web = $data['DiagnosticSearch']['parent'];
		   $qry = " where parent ='".$web."'";
			}
		   }
       $selQry2       = Yii::$app->db->createCommand("SELECT * from diagnostic $qry");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdown[$model['d_id']] = $model['d_name'];
    }
    return $dropdown;
   }

   	public function getSecondName($id){
		if($id==0){
		return "";
		}
		$selQry = Yii::$app->db->createCommand("SELECT * from diagnostic where d_id=$id");
        $result = $selQry->queryOne();

	     return $result['d_name'];
	}
	public function getSecondDropdown($id){
		    $selQry2       = Yii::$app->db->createCommand("SELECT * from diagnostic where parent = $id");
            return $result = $selQry2->queryAll();

		}
		
	public static function diagnomaindropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT d_id, d_name, parent FROM diagnostic where parent = 0");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdownt[$model['d_id']] = $model['d_name'];
    }
    return $dropdownt;
   }
	public static function diagnoseconddropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT d_id, d_name, parent FROM diagnostic");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdowns[$model['d_name']] = $model['d_name'];
    }
    return $dropdowns;
   }
   
}
