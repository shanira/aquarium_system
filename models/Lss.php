<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lss".
 *
 * @property int $lss_id
 * @property int $container_id
 * @property string $water_level
 * @property string $tempeture
 * @property string $sf1
 * @property string $sf2
 * @property string $sf3
 * @property string $sf4
 * @property string $sf5
 * @property string $date_created
 * @property string $date_modified
 */
class Lss extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lss';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['container_id', 'water_level', 'tempeture', 'sf1', 'sf2', 'sf3', 'sf4', 'sf5', 'date_created', 'date_modified'], 'required'],
            [['container_id'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['water_level'], 'string', 'max' => 255],
            [['tempeture', 'sf1', 'sf2', 'sf3', 'sf4', 'sf5'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lss_id' => Yii::t('app', 'Lss ID'),
            'container_id' => Yii::t('app', 'Container ID'),
            'water_level' => Yii::t('app', 'Water Level'),
            'tempeture' => Yii::t('app', 'Tempeture'),
            'sf1' => Yii::t('app', 'Sf1'),
            'sf2' => Yii::t('app', 'Sf2'),
            'sf3' => Yii::t('app', 'Sf3'),
            'sf4' => Yii::t('app', 'Sf4'),
            'sf5' => Yii::t('app', 'Sf5'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_modified' => Yii::t('app', 'Date Modified'),
        ];
    }
	public function getLesstodayData($container_id, $fid){
		$selQry = Yii::$app->db->createCommand("SELECT * from lss where container_id='".$container_id."' AND lss_machine='".$fid."' AND date='".date('Y-m-d')."'  order by lss_id DESC");
        $result = $selQry->queryOne();
		if($result['machine_value']){
		return $result['machine_value'];
		}else{
			return '';
			}
	}

  public function getLSSData(){
           $selQry = Yii::$app->db->createCommand("SELECT * from lss_system");
        return $result = $selQry->queryAll();
  }

	public function getLastUpdatedData($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT date_modified from lss where container_id='".$container_id."' order by lss_id DESC LIMIT 1,1 ");
        $result = $selQry->queryOne();
		return $result['date_modified'];
	}
	public function getCurrentDate($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT date_created from lss where container_id='".$container_id."' order by lss_id DESC LIMIT 1,1 ");
        return $result = $selQry->queryOne();
		//return $result['date_created'];
	}

	public function getContainerLssMachine($container_id){
		$selQry = Yii::$app->db->createCommand("SELECT a.*, s.lss_machine_name from lss_machine_associate a LEFT JOIN lss_system s ON s.lss_machine_id = a.lss_attached_id where container_id='".$container_id."'");
        return $result = $selQry->queryAll();

	}
	public static function getstatus($lid){
		$selQry = Yii::$app->db->createCommand("SELECT status from lss where lss_machine='".$lid."' AND date='".date('Y-m-d')."'");
        $result = $selQry->queryOne();
		if(!empty($result)){
            return $result['status'];
        }

        return null;

	}
	public static function getLssName($lid){
		$selQry = Yii::$app->db->createCommand("SELECT lss_machine_name from lss_system where lss_machine_id='".$lid."'");
        $result = $selQry->queryOne();
		if(!empty($result)){
            return $result['lss_machine_name'];
        }

        return null;

	}
	public function dropContainer(){
		$selQry = Yii::$app->db->createCommand("SELECT * from containers");
        $result = $selQry->queryAll();
		$data =  array();
		   foreach($result as $rows){
			$data[$rows['cid']] = $rows['container_name'];
			}
	     return $data;
	}
    public function dropLssMachine(){
		$selQry = Yii::$app->db->createCommand("SELECT * from lss_system");
        $result = $selQry->queryAll();
		$data =  array();
		   foreach($result as $rows){
			$data[$rows['lss_machine_id']] = $rows['lss_machine_name'];
			}
	     return $data;
	}

	public static function getContainerName($id){
        $selQry2 = Yii::$app->db->createCommand("SELECT container_name FROM containers where cid= '".$id."'");
		$result = $selQry2->queryOne();
        if(!empty($result)){
            return $result['container_name'];
        }

        return null;
    }

	public static function getFishName($id){
		if($id == 'ALL'){
		  return 'ALL';
		}else{
        $selQry2 = Yii::$app->db->createCommand("SELECT name FROM fish where fid= '".$id."'");
		$result = $selQry2->queryOne();
        if(!empty($result)){
            return $result['name'];
        }
        }
        return null;
    }

    public static function getFoodType($id){
          $selQry2 = Yii::$app->db->createCommand("SELECT food_type FROM food_type where foodt_id= '".$id."'");
          $result = $selQry2->queryOne();
          if(!empty($result)){
              return $result['food_type'];
          }
          return null;
      }

	public static function containerdropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT cid, container_name FROM containers");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdown[$model['cid']] = $model['container_name'];
    }
    return $dropdown;
   }
   public static function lssmachingdropdown() {
       $selQry2       = Yii::$app->db->createCommand("SELECT lss_machine_id,lss_machine_name FROM lss_system");
       $result = $selQry2->queryAll();
    foreach ($result as $model) {
        $dropdownt[$model['lss_machine_id']] = $model['lss_machine_name'];
    }
    return $dropdownt;
   }
}
