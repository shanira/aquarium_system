<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LssSystem;

/**
 * LssSystemSearch represents the model behind the search form of `app\models\LssSystem`.
 */
class FoodTypeSearch extends FoodType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['foodt_id'], 'integer'],
            [['food_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FoodType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'foodt_id' => $this->foodt_id,
			 //'food_type' => $this->food_type,
        ]);

        $query->andFilterWhere(['like', 'food_type', $this->food_type]);

        return $dataProvider;
    }
}
