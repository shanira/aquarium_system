<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LssMachineAssociate;

/**
 * LssMachineAssociateSearch represents the model behind the search form of `app\models\LssMachineAssociate`.
 */
class LssMachineAssociateSearch extends LssMachineAssociate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aid', 'container_id'], 'integer'],
			[['lss_attached_id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LssMachineAssociate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'aid' => $this->aid,
            'container_id' => $this->container_id,
            'lss_attached_id' => $this->lss_attached_id,
        ]);
		
		$query->andFilterWhere(['like', 'container_id', $this->container_id])
				->andFilterWhere(['like', 'lss_attached_id', $this->lss_attached_id]);
				

        return $dataProvider;
    }
}
