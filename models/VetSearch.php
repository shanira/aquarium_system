<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vet;

/**
 * VetSearch represents the model behind the search form of `app\models\Vet`.
 */
class VetSearch extends Vet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vet_id', 'container_id'], 'integer'],
            [['treatment_for','frequency', 'comments', 'date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $session = Yii::$app->session;
        $cid = $session->get('cid');
        // grid filtering conditions
        $query->andFilterWhere([
            'vet_id' => $this->vet_id,
            'container_id' => $cid,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'treatment_for', $this->treatment_for])
            ->andFilterWhere(['like', 'comments', $this->comments]);
        $query->orderBy(['(vet_id)' => SORT_DESC]);
        return $dataProvider;
    }
}
