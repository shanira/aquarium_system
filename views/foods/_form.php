<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Lss;
use  yii\web\Session;
use app\models\FoodType;

$session = Yii::$app->session;
$Lssmodel = new Lss();
$data              = $Lssmodel->dropContainer();
/* @var $this yii\web\View */
/* @var $model app\models\Food */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="food-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_id')->dropdownList($data,
	         ['prompt'=>'בחר מיכל',
			  'onchange'=>'
				$.post( "'.Yii::$app->urlManager->createUrl('foods/lists?id=').'"+$(this).val(), function( data ) {
				  $( "select#fish_to" ).html( data );
				});
			']);  ?>

     <?php
     $dataPost=ArrayHelper::map(\app\models\Fish::find()->asArray()->groupBy('name')->all(), 'fid', 'name');
	echo $form->field($model, 'fish_to')
        ->dropDownList(
            $dataPost,
            ['id'=>'fish_to']
        );

	?>
   <?= $form->field($model, 'day_of_week')->dropdownList(['Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday'],
					['prompt'=>'בחר יום בשבוע']
				); ?>

    <!--?= $form->field($model, 'food_type')->textInput(['maxlength' => true]) ?-->

	<?= $form->field($model, 'food_type')->
				dropDownList([FoodType::getFoodType()],	['prompt'=>'בחר סוג מזון']) ?>


    <?= $form->field($model, 'amount')->textInput([
                                 'type' => 'number'
                            ]) ?>

    <?= $form->field($model, 'unit')->dropDownList([ 'גרם' => 'גרם', 'קילוגרם' => 'קילוגרם', 'יחידות' => 'יחידות', ], ['prompt' => 'בחר יחידות']) ?>


    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'created_date')->hiddenInput(['value' => date('Y-m-d')])->label(false); ?>

    <?= $form->field($model, 'updated_date')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false); ?>

    <?= $form->field($model, 'created_by')->hiddenInput(['value' => $session->get('employee_name')])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'שמור'), ['class' => 'btn topbtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
