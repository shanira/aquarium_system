<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Food */

$this->title = $model->food_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Foods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <a href="<?= Url::to(['foods/index' ])?>"><?php echo Yii::t('app', 'חזור' ); ?></a>
        <?= Html::a(Yii::t('app', 'עדכן'), ['update', 'id' => $model->food_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'מחק'), ['delete', 'id' => $model->food_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'food_id',
            'container_id',
            'fish_to',
            'day_of_week',
            'food_type',
            'unit',
            'amount',
            //'actual_unit',
            'actual_amount',
            'comment:ntext',
            'created_date',
            'updated_date',
            'created_by',
        ],
    ]) ?>

</div>
