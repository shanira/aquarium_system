<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Lss;
use app\models\Food;

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  yii\web\Session;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'מזון');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוספת תפריט האכלה'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'tableOptions' => ['id' => 'myTable','class' => 'table table-striped table-bordered'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'food_id',
            [
			'label' => 'מיכל',
			'attribute' => 'container_id',
			'format' => 'raw',
			'filter' => Food::containerdropdown(),
			'value' => function($model, $index, $dataColumn) {
                // more optimized than $model->role->name;
                $containerDropdowns = Food::containerdropdown();
				 if($model->container_id){  
                return $containerDropdowns[$model->container_id];
				 }else{
						return '';
					}
            },
		   
           ],
		   [
			'label' => 'עבור',
			'attribute' => 'fish_to',
			'format' => 'raw',
		   'value' => function($model){
                return Lss::getFishName($model->fish_to);
            },
           ],
           [
			'attribute' => 'day_of_week',
			'label' => 'יום בשבוע',
			'format' => 'raw',
			'filter' => Food::daydropdown(),
			'value' => function($model, $index, $dataColumn) {
                $dayDropdowns = Food::daydropdown();
				 if($model->day_of_week){  
                return $dayDropdowns[$model->day_of_week];
				 }else{
						return '';
					}
            },
           ],
           
            [
     			'label' => 'סוג מזון',
     			'attribute' => 'food_type',
     			'format' => 'raw',
				'filter' => Food::foodtypedropdown(),
     		   'value' => function($model){
                     return LSS::getFoodType($model->food_type);
                 },
                ],
            'amount',
			[
     			'label' => 'יחידות',
     			'attribute' => 'unit',
     			'format' => 'raw',
				'filter' => Food::unitdropdown(),
                
                ],
			  //    'unit',
            //'actual_unit',
            //'actual_amount',
            'comment:ntext',
            'created_date',
            //'updated_date',
            //'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<style>
#w0-filters {
    display: table-row;
}
</style>
