<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;

$this->title = 'הרשמה';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>יש למלא את השדות הבאים:</p>

    <div class="row">
    <div class="col-md-4"></div>
        <div class="col-md-4">
           <?php if (Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <h4><i class="icon fa fa-check"></i>נשמר!</h4>
  <?= Yii::$app->session->getFlash('success') ?>
  </div>
           <?php endif; ?>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                 <?= $form->field($model, 'fullname')->textInput(['autofocus' => true]) ?>
                  <?= $form->field($model, 'security_question')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                 <?= $form->field($model, 'status')->dropdownList([
						'Moderator' => 'עובד',
						'Admin' => 'מנהל'
					],
					['prompt'=>'בחר הרשאה']
				); ?>



                <div class="form-group">
                    <?= Html::submitButton('שמור', ['class' => 'btn topbtn', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<style>
.help-block-error{ color:red; }
</style>
