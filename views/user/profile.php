<?php
/* @var $this yii\web\View */
use yii\web\Request;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use  yii\web\Session;
$session = Yii::$app->session;
//$this->title = 'Domains and subsites';
// Modify the XPath query to match the content
?>

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Profile</h4>
    </div>
</div>

<div class="card-box">



<div class="row">
	<div class="col-lg-9">
		<div class="card-box">

    <form action="#" class="form-horizontal">
    <div class="form-group row">
        <label class="col-sm-5 form-control-label">שם משתמש:  </label>
        <div class="col-sm-7"> <?php echo ucfirst(Yii::$app->user->identity->username); ?></div>
    </div>

    <div class="form-group row">
        <label class="col-sm-5 form-control-label">אימייל:  </label>
        <div class="col-sm-7"><a href="#" id="email" data-type="text" data-url="<?php echo Yii::$app->getUrlManager()->createUrl('user/ajax'); ?>"  data-pk="<?php echo $session->get('employee_name'); ?>" data-value="<?php echo Yii::$app->user->identity->email; ?>" data-placement="right" data-placeholder="Required" data-title="<?php echo Yii::$app->user->identity->email; ?>"><?php echo Yii::$app->user->identity->email; ?></a>
        </div>
     </div>

     <div class="form-group row">
        <label class="col-sm-5 form-control-label">הרשאה: </label>
		<div class="col-sm-7"><?php echo Yii::$app->user->identity->status; ?></div>
      </div>

    </form>

</div>
</div></div>



</div>

   <link type="text/css" href="<?php @web?>/themes/unilytics/plugins/x-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?php @web?>/themes/unilytics/plugins/moment/moment.js"></script>
<script src="<?php @web?>/themes/unilytics/plugins/x-editable/js/bootstrap-editable.min.js"></script>
<script>
$(function(){

    //modify buttons style
    $.fn.editableform.buttons =
    '<button type="submit" class="btn btn-primary editable-submit waves-effect waves-light"><i class="zmdi zmdi-check"></i></button>' +
    '<button type="button" class="btn editable-cancel btn-secondary waves-effect"><i class="zmdi zmdi-close"></i></button>';

    //inline

/*
  $('#inline-username').editable({
     type: 'text',
     pk: 1,
     name: 'username',
     title: 'Enter username',
     mode: 'inline'
   });*/

    $('#email').editable({
      validate: function(value) {
       if($.trim(value) == '') return 'This field is required';
     },
     mode: 'inline'
   });



  });
</script>
