<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>
  <?php if (Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <?= Yii::$app->session->getFlash('success') ?>
  </div>
  <?php endif; ?>
    <p>Please choose your new password:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                
                <div class="form-group field-resetpasswordform-password required has-error">
                <label class="control-label" for="resetpasswordform-password">Confirm Password</label>
                <input id="confirm_password" class="form-control" name="confirmPassword" value="" autofocus="" aria-required="true" aria-invalid="true" type="password" required="required">
                
                <span id="indicator" style="display:none">Password not matching.</span>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'id' => 'regbtn']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>

$('#confirm_password').keyup(function(){
    var pass    =   $('#resetpasswordform-password').val();
    var cpass   =   $('#confirm_password').val();
    if(pass!=cpass){
		$('#indicator').css({display:'block'});
        $('#indicator').css({color:'red'});
        $('#regbtn').attr({disabled:true});
    }
    else{
        $('#indicator').attr({color:'green'});
		$('#indicator').css({display:'none'});
        $('#regbtn').attr({disabled:false});
    }
});

</script>
