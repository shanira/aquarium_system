<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-sm-12 col-md-10">
        <div class="btn-group pull-right m-t-15">
         
        </div>
        <h4 class="page-title"><?= Html::encode($this->title) ?></h4>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
 	   <div class="row">
        	<div class="col-lg-4"></div>
				<div class="col-lg-4">
<div class="user-update">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly'=> true]) ?>
    
    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'security_question')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'status')->dropDownList(['Admin' => 'Admin', 'Moderator' => 'Moderator']) ?>
    



    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
<div class="col-lg-4"></div>
</div></div></div></div>
