<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CmsRecordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'User');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-12 col-md-10">
        <div class="btn-group pull-right m-t-15">
           <?= Html::a(Yii::t('app', 'הוספת משתמש חדש'), ['user/create'], ['class' => 'btn topbtn']) ?>
        </div>
        <h4 class="page-title">משתמשים</h4>
    </div>
</div>
<!-- end row -->

<div class="row">
	<div class="col-lg-12">
 	   <div class="row">
        	<div class="col-lg-12">

<?php Pjax::begin(); ?>
 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            'username',
			'fullname',
			'security_question',
			[
				'attribute' => 'status',
				'label' => 'סוג הרשאה',
				'filter' => User::statusdropdown(),
				'value' => function($model, $index, $dataColumn) {
					$roleDropdown = User::statusdropdown();
					return $roleDropdown[$model->status];
				},
			],
            ['class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a ( '<span class="zmdi zmdi-eye" aria-hidden="true"></span> ', ['user/view', 'id' => $model->id] );
                },
				'update' => function ($url, $model, $key) {
                    return Html::a ( '<span class="zmdi zmdi-edit" aria-hidden="true"></span> ', ['user/update', 'id' => $model->id] );
                },
				'delete' => function ($url, $model, $key) {
                    return Html::a ( '<span class="zmdi zmdi-delete" aria-hidden="true"></span> ', ['user/delete', 'id' => $model->id]);
                },
            ],
            'template' => '{update} {view} {delete}'


        ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div></div></div></div>
