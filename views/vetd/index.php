<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Vetd;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Vetd');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12">
		<div class="btn-group pull-right m-t-15">
		<a class="btn topbtn" href="#tab=w1-tab0" onclick="return addcommentd();">הוספת אבחון</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
			'label' => 'תאריך',
			'format' => 'raw',
		   'value' => function($model){
			   $newdate = date('d-m-Y', strtotime($model->date_created));
                return $newdate;
            },
           ],
		   [
			'label' => 'אבחנה ראשית',
			'format' => 'raw',
		   'value' => function($model){
			   return $webteamsdropdowns = Vetd::getDiagonoName($model->diagnostic_name);
            },
           ],
		   [
			'label' => 'אבחנה משנית',
			'format' => 'raw',
		   'value' => function($model){
			   return $webteamsdropdowns = Vetd::getDiagonoName($model->diagnostic_type);
            },
           ],
		   [
			'label' => 'הערה',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->comments;
            },
           ],

			[
			'label' => 'עבור',
			'format' => 'raw',
		   'value' => function($model){
                return Vetd::getFish($model->diagnostic_for);
            },
           ],
           // ['class' => 'yii\grid\ActionColumn'],
		   ['class' => 'yii\grid\ActionColumn',
			 	'buttons' => [

			'update' => function ($url, $model, $key) {
				   return '<a href="JavaScript:void(0);" onclick="vetdupdate('.$model->vedt_id.');" ><span class="glyphicon glyphicon-pencil"></span></a>';
                },

			'delete' => function ($url, $model, $key)
				{
					return '<a href="JavaScript:void(0);" onclick="vetddelete('.$model->vedt_id.');" ><span class="glyphicon glyphicon-trash"></span></a>';

                },

            ],
            'template' => '{update} {delete}'


        ],
        ],

    ]); ?>
    </div>
</div>
