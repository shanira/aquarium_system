<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
use app\models\Diagnostic;

$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Vet */

$this->title = Yii::t('app', 'Update Vet: ' . $model->vedt_id, [
    'nameAttribute' => '' . $model->vedt_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vedt_id, 'url' => ['view', 'id' => $model->vedt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$post = Yii::$app->request->post();
$data = $model->getFishDataDrop($post['cid']);
$diagnoModel  = new Diagnostic();
$parentDrop = $diagnoModel->getParentData();
$secondDrop = $diagnoModel->getSecondDropdown($model->diagnostic_name);
?>
<div class="row">
	<div class="col-sm-12">
	     <h1>עדכון אבחון</h1>
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn" href="#tab=w0-tab2" onclick="return backfood();">חזור</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">


     <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">אבחנה ראשית</label>
		<select class="form-control" id="diagnostic_main" name="diagnostic_main" style="height: 36px;" onchange="setseconddropdown(this.value);">
        <option value="0">Select Main Diagnostic</option>
		  <?php foreach($parentDrop as $parentDrops){ ?>
			<option value="<?php echo $parentDrops['d_id']; ?>"  <?=$model->diagnostic_name == $parentDrops['d_id'] ? ' selected="selected"' : '';?>><?php echo $parentDrops['d_name']; ?></option>
		  <?php } ?>

		  </select>

    </div>

    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">אבחנה משנית</label>
		<select class="form-control" id="diagnostic_second" name="diagnostic_second" style="height: 36px;">
			 <option value="0">Select Main Diagnostic</option>
		  <?php foreach($secondDrop as $secondDrops){ ?>
			<option value="<?php echo $secondDrops['d_id']; ?>"  <?=$model->diagnostic_type == $secondDrops['d_id'] ? ' selected="selected"' : '';?>><?php echo $secondDrops['d_name']; ?></option>
		  <?php } ?>
		  </select>

    </div>

    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">עבור</label>
		<select class="form-control" id="diagnostic_for" name="diagnostic_for" style="height: 36px;">
		  <?php foreach($data as $fishDrop){ ?>
			<option value="<?php echo $fishDrop['fid']; ?>"  <?=$model->diagnostic_for == $fishDrop['fid'] ? ' selected="selected"' : '';?>><?php echo $fishDrop['name']; ?></option>
		  <?php } ?>
			<option value="0" <?=$model->diagnostic_for == 0 ? ' selected="selected"' : '';?>>All Container</option>
		  </select>
    </div>


    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea cols="5" rows="3" name="comments" class="form-control" id="comments"><?php echo $model->comments; ?></textarea>
		<div class="help-block"></div>
	</div>



    <div class="form-group field-fish-created_date required">
	<input id="vet_id" class="form-control" name="vet_id" value="<?php echo $model->vedt_id; ?>" type="hidden">
		<input id="created_date" class="form-control" name="created_date" value="<?php echo $model->date_created; ?>" type="hidden">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
	</div>

    <div class="form-group "><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return updatevetd();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>
	function backfood(){
        $("#vetd").trigger('click');
	}

</script>
