<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
use app\models\Diagnostic;
use yii\widgets\ActiveForm;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Food */

$this->title = Yii::t('app', 'Create Vetd');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vetd'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();
$data = $model->getFishDataDrop($post['cid']);
$diagnoModel  = new Diagnostic();
$parentDrop = $diagnoModel->getParentData();
?>
<div class="row">
	<div class="col-sm-12">
	     <h1>אבחנה חדשה</h1>
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn fa fa-chevron-right" href="#tab=w0-tab2" onclick="return backfood();"> חזור </a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">
     <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">אבחנה ראשית</label>
		<select class="form-control" id="diagnostic_main" name="diagnostic_main" style="height: 36px;" onchange="setseconddropdown(this.value);">
        <option value="0">בחר אבחנה ראשית</option>
		  <?php foreach($parentDrop as $parentDrops){ ?>
			<option value="<?php echo $parentDrops['d_id']; ?>"><?php echo $parentDrops['d_name']; ?></option>
		  <?php } ?>

		  </select>

    </div>

    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">אבחנה משנית</label>
		<select class="form-control" id="diagnostic_second" name="diagnostic_second" style="height: 36px;">
			<option value="0">בחר אבחנה משנית</option>
		  </select>

    </div>

    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">עבור</label>
		<select class="form-control" id="diagnostic_for" name="diagnostic_for" style="height: 36px;">
		  <?php foreach($data as $fishDrop){ ?>
			<option value="<?php echo $fishDrop['fid']; ?>"><?php echo $fishDrop['name']; ?></option>
		  <?php } ?>
			<option value="0">All Container</option>
		  </select>

    </div>

	<input type="file" name = "image" id="image" />



    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea cols="5" rows="5" name="comments" class="form-control" id="comments"></textarea>
		<div class="help-block"></div>
	</div>

    <div class="form-group field-fish-created_date required">
		<input id="created_date" class="form-control" name="created_date" value="<?php echo date ("Y-m-d"); ?>" type="hidden">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
	</div>


    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return savediagno();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>

    function setseconddropdown(id){
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vetd/getsecond']); ?>",
         data: {id: id}
        })
       .done(function( msg ) {
		 $("#diagnostic_second").html(msg);
       });
		}
	function backfood(){
        $("#vetd").trigger('click');
	}

	function savediagno(){
		if($("#diagnostic_main").val() ==''){
			alert("Please Select Main Diagnostic.");
			$("#diagnostic_main").focus();
			return false;
		}
		if($("#diagnostic_second").val() ==''){
			alert("Please Select Second Diagnostic.");
			$("#diagnostic_second").focus();
			return false;
		}

		if($("#diagnostic_for").val() ==''){
			alert("Please Select For.");
			$("#diagnostic_for").focus();
			return false;
		}

		comments  = $("#comments").val();
		diagnostic_name   = $("#diagnostic_main").val();
		diagnostic_type   = $("#diagnostic_second").val();
		diagnostic_for       = $("#diagnostic_for").val();
		fullPath       = $("#image").val();
		if (fullPath) {
    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
    var image = fullPath.substring(startIndex);
    if (image.indexOf('\\') === 0 || image.indexOf('/') === 0) {
        image = image.substring(1);
    }
    //alert(image);
		}
		created_date     = $("#created_date").val();
		updated_date     = $("#updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vetd/savecomment']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", diagnostic_name: diagnostic_name, diagnostic_type:diagnostic_type, comments: comments,image:image, diagnostic_for:diagnostic_for, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("נשמר בהצלחה!");
         setTimeout(function() {
        $("#vetd").trigger('click');
    },1);
       });
	}
</script>
