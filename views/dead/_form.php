<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Dead;
$Lssmodel = new Dead();
$data              = $Lssmodel->dropParent();
/* @var $this yii\web\View */
/* @var $model app\models\Diagnostic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diagnostic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dead_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'parent')->dropdownList($data,
					['prompt'=>'Select Parent']
				); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', ' שמור '), ['class' => 'btn btn-success fa fa-floppy-o']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
