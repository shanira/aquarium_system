<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diagnostic */

$this->title = Yii::t('app', 'עדכון סיבת מוות מספר: ' . $model->dead_id, [
    'nameAttribute' => '' . $model->dead_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dead'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dead_id, 'url' => ['view', 'id' => $model->dead_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dead-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
