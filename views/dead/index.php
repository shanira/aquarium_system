<?php

use yii\web\Request;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Dead;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DiagnosticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'סיבת מוות');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dead-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוספת סיבת מוות'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'dead_id',
			[
			'attribute' => 'dead_name',
			'label' => 'סיבת מוות',
			'format' => 'raw',
			//'filter' => Dead::diagnoseconddropdown()
           ],
            //'dead_name',
			/*[
			'attribute' => 'd_name',
			'label' => 'D Name',
			'format' => 'raw',
			'filter' => Diagnostic::relationdropdown(),
			'value' => function($model, $index, $dataColumn) {
				 $webteamsdropdowns = Diagnostic::relationdropdown();
				if($model->d_name){
				return "";
				}else{
                return @$webteamsdropdowns[$model->d_name];
				}
            },
           ],*/
			[
			'attribute' => 'parent',
			'label' => 'סיבת מוות ראשית',
			'format' => 'raw',
			'filter' => Dead::deadmaindropdown(),
			'value' => function($model, $index, $dataColumn) {
				return $webteamsdropdowns = Dead::getSecondName($model->parent);
            },
           ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
