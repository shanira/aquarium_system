<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Diagnostic */

$this->title = Yii::t('app', 'הוספת סיבת מוות');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'מוות'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dead-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
