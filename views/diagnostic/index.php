<?php

use yii\web\Request;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Diagnostic;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DiagnosticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'אבחון');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diagnostic-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוסף אבחון'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'d_id',
			[
			'attribute' => 'd_name',
			'label' => 'אבחנה משנית',
			'format' => 'raw',
			//'filter' => Diagnostic::diagnoseconddropdown()
			
           ],
            //'d_name',
			/*[
			'attribute' => 'd_name',
			'label' => 'D Name',
			'format' => 'raw',
			'filter' => Diagnostic::relationdropdown(),
			'value' => function($model, $index, $dataColumn) {
				 $webteamsdropdowns = Diagnostic::relationdropdown();
				if($model->d_name){
				return "";
				}else{
                return @$webteamsdropdowns[$model->d_name];
				}
            },
           ],*/
			[
			'attribute' => 'parent',
			'label' => 'אבחנה ראשית',
			'format' => 'raw',
			'filter' => Diagnostic::diagnomaindropdown(),
			'value' => function($model, $index, $dataColumn) {
				return $webteamsdropdowns = Diagnostic::getSecondName($model->parent);
            },
           ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
