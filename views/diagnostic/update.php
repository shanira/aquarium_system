<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diagnostic */

$this->title = Yii::t('app', 'עדכון אבחון מספר: ' . $model->d_id, [
    'nameAttribute' => '' . $model->d_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'אבחון'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->d_id, 'url' => ['view', 'id' => $model->d_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="diagnostic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
