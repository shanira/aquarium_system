<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lss;
$Lssmodel = new Lss();


$data              = $Lssmodel->dropContainer();
$dropLssMachine    = $Lssmodel->dropLssMachine();
/* @var $this yii\web\View */
/* @var $model app\models\LssMachineAssociate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_id')->dropdownList($data,
					['prompt'=>'בחר מיכל']
				); ?>

	<?= $form->field($model, 'lss_attached_id')->dropdownList($dropLssMachine,
					['prompt'=>'בחר מכונת LSS']
				); ?>

    <?= $form->field($model, 'input_type')->dropDownList([ 'level' => 'לחצני גובה מים','fish' => 'לחצני מצב דגים','enter' => 'שדה מספרי ולחצני מכונה','on' => 'לחצני מכונה']) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', ' שמור '), ['class' => 'btn topbtn fa fa-floppy-o']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="col-sm-4"></div>
</div>
</div>
