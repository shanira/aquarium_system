<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LssMachineAssociate */

$this->title = Yii::t('app', 'יצירת קישור בין LSS למיכל');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lss Machine Associates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lss-machine-associate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
