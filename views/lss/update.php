<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LssMachineAssociate */

$this->title = Yii::t('app', 'Update Lss Machine Associate: ' . $model->aid, [
    'nameAttribute' => '' . $model->aid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lss Machine Associates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->aid, 'url' => ['view', 'id' => $model->aid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lss-machine-associate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
