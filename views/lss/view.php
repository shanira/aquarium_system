<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LssMachineAssociate */

$this->title = $model->aid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'קישור LSS למיכל'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lss-machine-associate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->aid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->aid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'aid',
            'container_id',
            'lss_attached_id',
        ],
    ]) ?>

</div>
