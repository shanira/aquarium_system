<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Lss;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LssMachineAssociateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'קישור LSS למיכל');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lss-machine-associate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוסף קישור בין LSS למיכל'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'aid',
			[
			'attribute' => 'container_id',
			'label' => 'מיכל',
			'format' => 'raw',
			'filter' => LSS::containerdropdown(),
		   'value' => function($model){
                return Lss::getContainerName($model->container_id);
            },
           ],
			[
			'attribute' => 'lss_attached_id',
			'label' => 'מכונת LSS',
			'format' => 'raw',
			'filter' => LSS::lssmachingdropdown(),
		   'value' => function($model){
                return Lss::getLssName($model->lss_attached_id);
            },
           ],
		   [
			'label' => 'Input Type',
			'format' => 'raw',
		   'value' => function($model){
                if($model->input_type=='enter'){
				return 'שדה מספרי ולחצני מכונה';
      }elseif($model->input_type=='fish'){
				return "לחצני מצב דגים";
      }elseif($model->input_type=='on'){
  				return "לחצני מכונה";
  		}elseif($model->input_type=='level'){
        				return "לחצני גובה מים";
        				}
            },
           ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
