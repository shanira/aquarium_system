<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Water */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="water-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_id')->textInput() ?>

    <?= $form->field($model, 'co2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'po4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hardness')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bromines')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'copper')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'salinity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ph')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alkalinity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'do_mg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'do')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
