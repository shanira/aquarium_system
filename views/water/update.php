<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Water */

$this->title = Yii::t('app', 'Update Water: ' . $model->wid, [
    'nameAttribute' => '' . $model->wid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Waters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wid, 'url' => ['view', 'id' => $model->wid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">

    <h1><?= Html::encode($this->title) ?></h1>

    <form id="w0" action="" method="post">

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name"><?php echo $watertype; ?></label>
		<input id="wid" class="form-control" name="" maxlength="55" aria-required="true" type="hidden" value="<?php echo $model->wid; ?>">
        <input id="watertype" class="form-control" name="" maxlength="55" aria-required="true" type="hidden" value="<?php echo $watertype; ?>">
		<input id="watervalue" class="form-control" name="watervalue" maxlength="55" aria-required="true" type="text" value="<?php echo $model->$watertype; ?>">
	</div>

   <div class="form-group"><button type="button" class="btn topbtn" style="width:50%;" onclick="return updatewaters();">שמור</button></div>
   </form>
</div>
<div class="col-sm-4"></div>
</div>
<script>
function updatewaters(){
	    if($("#watervalue").val() ==''){
			alert("Please Enter value.");
			$("#watervalue").focus();
			return false;
		}

		wid       = $("#wid").val();
		watertype      = $("#watertype").val();
		watervalue     = $("#watervalue").val();

		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['water/updatewater']); ?>",
         data: { wid:wid, watertype:watertype, watervalue:watervalue}
        })
       .done(function( msg ) {
		   alert("Data updated.");
         setTimeout(function() {
        $("#water").trigger('click');
    },1);
       });
}

</script>
