<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Containers */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'History');
?>
<div class="row">
	<div class="col-sm-12">

		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn" onclick="return backtowater();">חזור</a>
</div>

</div>
</div>
<div class="containers-update">
<?php
$post = Yii::$app->request->post();
 $fid = $post['field_id'];
?>
<div class="table-responsive ">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>תאריך</th>
        <th><?php echo $fid; ?></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
	<?php foreach($model as $models){?>
      <tr data-key="<?php echo $models['wid']; ?>">
        <td><?php echo $models['updated_date']?></td>
        <td><?php echo $models[$fid]?></td>
        <td>
		<a href="JavaScript:void(0);" onclick="updatewater(<?php echo $models['wid']; ?>,'<?php echo $fid; ?>');" ><span class="glyphicon glyphicon-pencil"></span></a>
        <a href="JavaScript:void(0);" onclick="deletewater(<?php echo $models['wid']; ?>);" ><span class="glyphicon glyphicon-trash"></span></a>
		</td>
      </tr>
	<?php } ?>
    </tbody>
  </table>
  </div>
</div>
<script>
function backtowater(){
        $("#water").trigger('click');
	}
</script>
