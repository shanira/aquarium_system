<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;
use app\models\Water;
$model = new Water();


$data              = $model->getWaterLastData($session->get('cid'));

?>



<div class="row">
  <div class="col-sm-12">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">



<form class="form-horizontal">
 <div class="col-sm-6">


   <div class="form-group">
   <label class="control-label col-sm-4" for="Water Level">Water Level</label>
   <div class="col-sm-4">
     <select class="form-control" id="water_level">
     <option value="" selected disabled>בחר</option>
     <option value="תקין">תקין</option>
     <option value="נמוך">נמוך</option>
     <option value="גבוה">גבוה</option>
   </select>
   </div>
   <div class="col-sm-2">
     <button type="button" class="histoy" onclick="return waterhistory('water_level');">היסטוריה</button>
     </div>
   </div>


     <div class="form-group">
         <label class="control-label col-sm-4" for="Temperature">Temperature</label>
         <div class="col-sm-4">
           <input type="text" class="form-control" id="temperature" value="<?php echo $data['temperature']; ?>" name="Temperature">
         </div>
   	  <div class="col-sm-4">
   	  <button type="button" class="histoy" onclick="return waterhistory('temperature');">היסטוריה</button>
   	  </div>
       </div>


    <div class="form-group">
      <label class="control-label col-sm-4" for="salinity">Salinity</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="salinity" value="<?php echo $data['salinity']; ?>" name="salinity">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('salinity');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="pH">pH</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="ph" value="<?php echo $data['ph']; ?>"  name="ph">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('ph');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="alkalinity">Alkalinity</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="alkalinity" value="<?php echo $data['alkalinity']; ?>" name="alkalinity">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('alkalinity');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="tan">TAN</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="tan" value="<?php echo $data['tan']; ?>" name="sf2">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('tan');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="uia">UIA</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="uia" value="<?php echo $data['uia']; ?>" name="uia">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('uia');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="do_mg">DO mg/l</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="do_mg" value="<?php echo $data['do_mg']; ?>" name="do_mg">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('do_mg');">היסטוריה</button>
	  </div>
    </div>



	<div class="form-group">
      <label class="control-label col-sm-4" for="do">DO %</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="do" value="<?php echo $data['do']; ?>" name="sf2">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('do');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="mg">Mg</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="mg" value="<?php echo $data['mg']; ?>" name="mg">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('mg');">היסטוריה</button>
	  </div>
    </div>
</div>
<div class="col-sm-6">
	<div class="form-group">
      <label class="control-label col-sm-4" for="co2">CO2</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="co2" value="<?php echo $data['co2']; ?>" name="co2">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('co2');">היסטוריה</button>
	  </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4" for="no2">NO2</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="no2" value="<?php echo $data['no2']; ?>" name="no2">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('no2');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="no3">NO3</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="no3" value="<?php echo $data['no3']; ?>" name="no3">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('no3');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="po4">PO4</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="po4" value="<?php echo $data['po4']; ?>" name="po4">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('po4');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="hardness">Hardness</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="hardness" value="<?php echo $data['hardness']; ?>" name="hardness">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('hardness');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="bromines">Bromines</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="bromines" value="<?php echo $data['bromines']; ?>" name="bromines">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('bromines');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="copper">Copper</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="copper" value="<?php echo $data['copper']; ?>" name="copper">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('copper');">היסטוריה</button>
	  </div>
    </div>

	<div class="form-group">
      <label class="control-label col-sm-4" for="Ca">Ca</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="ca" value="<?php echo $data['ca']; ?>" name="ca">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('ca');">היסטוריה</button>
	  </div>
    </div>


  <div class="form-group">
      <label class="control-label col-sm-4" for="Strontium">Strontium</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="strontium" value="<?php echo $data['strontium']; ?>" name="Strontium">
      </div>
	  <div class="col-sm-4">
	  <button type="button" class="histoy" onclick="return waterhistory('strontium');">היסטוריה</button>
	  </div>
    </div>

	</div>
    <button type="button" class="btn topbtn" style="width:70%" onclick="return savewater();">שמור</button>
  </form>

</div>
<div class="col-sm-2"></div>
</div>



<script>
function savewater(){
  water_level = $("#water_level").val();
  temperature = $("#temperature").val();
	co2  = $("#co2").val();
	no2  = $("#no2").val();
	no3  = $("#no3").val();
	po4  = $("#po4").val();
	hardness  = $("#hardness").val();
	bromines  = $("#bromines").val();
	copper  = $("#copper").val();
	ca  = $("#ca").val();

	salinity  = $("#salinity").val();
	ph  = $("#ph").val();
	alkalinity  = $("#alkalinity").val();
	tan  = $("#tan").val();
	uia  = $("#uia").val();
	do_mg  = $("#do_mg").val();
	dos  = $("#do").val();
	mg  = $("#mg").val();
  strontium = $("#strontium").val();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['water/savewater']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>",water_level:water_level, temperature:temperature, co2: co2, no2:no2, no3:no3, po4:po4,  hardness:hardness, bromines:bromines, copper:copper, ca:ca, salinity:salinity, ph:ph, alkalinity:alkalinity, tan:tan, uia:uia,  do_mg:do_mg, dos:dos, mg:mg, strontium:strontium }
        })
       .done(function( msg ) {
		   alert("Water saved.");
         setTimeout(function() {
        $("#water").trigger('click');
    },1);
       });
}

function waterhistory(field_id){
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['water/waterhistory']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", field_id: field_id }
        })
       .done(function( msg ) {
		   $("#w0-tab3").empty().html(msg);

       });
}
</script>
