<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WaterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="water-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'wid') ?>

    <?= $form->field($model, 'container_id') ?>

    <?= $form->field($model, 'co2') ?>

    <?= $form->field($model, 'no2') ?>

    <?= $form->field($model, 'no3') ?>

    <?php // echo $form->field($model, 'po4') ?>

    <?php // echo $form->field($model, 'hardness') ?>

    <?php // echo $form->field($model, 'bromines') ?>

    <?php // echo $form->field($model, 'copper') ?>

    <?php // echo $form->field($model, 'ca') ?>

    <?php // echo $form->field($model, 'salinity') ?>

    <?php // echo $form->field($model, 'ph') ?>

    <?php // echo $form->field($model, 'alkalinity') ?>

    <?php // echo $form->field($model, 'tan') ?>

    <?php // echo $form->field($model, 'uia') ?>

    <?php // echo $form->field($model, 'do_mg') ?>

    <?php // echo $form->field($model, 'do') ?>

    <?php // echo $form->field($model, 'mg') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
