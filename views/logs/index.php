<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Logs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Logs');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
<?php // echo "<pre>"; print_r($dataProvider); ?>
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>תאריך פעולה</th>
                    <th>מי ביצע</th>
                    <th>מה בוצע</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($dataProvider)){
			foreach($dataProvider->models as $report){ ?>
                <tr>
                    <td><?php echo $report->action_date; ?></td>
                    <td><?php echo $report->who_did; ?></td>
                    <td><?php echo $report->what_did." ".Logs::getContainerName($report->container_id); ?></td>

                </tr>
              <?php } } ?>
            </tbody>
            <tfoot align="right">
            <tr> <th></th> <th></th> <th></th></tr>
            </tfoot>
        </table>

        <?php

		?>
    </div>
    </div>
    </div>
<script type="text/javascript">
            $(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 20,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
					"order": [[ 1, "desc" ]]
                    //buttons: ['csv', 'excel', 'pdf'],


                });

               table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});
</script>
