<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logs */

$this->title = Yii::t('app', 'Update Logs: ' . $model->logs_id, [
    'nameAttribute' => '' . $model->logs_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->logs_id, 'url' => ['view', 'id' => $model->logs_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="logs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
