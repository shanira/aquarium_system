<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
use app\models\Fish;
use app\models\DeathCause;
use app\models\Dead;
$fishModel = new Fish();
$deathModel = new Fish();
$deathsModel = new Fish();
$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $model app\models\Fish */
$deadModel  = new Dead();
$parentDrop = $deadModel->getParentData();


$this->title = Yii::t('app', 'Update Fish: ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fish'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->fid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
	<div class="col-sm-12">

		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn fa fa-chevron-right" onclick="return createfishback();"> חזור </a>
</div>

</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-3"></div>
<div class="col-sm-6">





<form id="w0" action="" method="post">

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">שם הדג</label>
		<input id="f_id" class="form-control" name="" maxlength="55" aria-required="true" type="hidden" value="<?php echo $model->fid; ?>">
		<input id="fish_name" class="form-control" name="fish_name" maxlength="55" aria-required="true" type="text" style="display:none;">
        <div class="help-block"><?php echo $model->name; ?></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">סיבת עדכון</label>

		<div class="help-block">

		</div>
    </div>



  <ul class="nav nav-tabs" id="nav2">
    <li class="active"><a data-toggle="tab" href="#home">הוספה</a></li>
    <li><a data-toggle="tab" href="#menu1">העברה</a></li>
    <li><a data-toggle="tab" href="#menu2">מוות</a></li>
  </ul>

  <div class="tab-content" id="innertab">
    <div id="home" class="tab-pane fade in active">

	        <div class="center">
				<div class="form-group field-fish-amount required">
					<label class="control-label" for="fish-amount">כמות</label>


					 <div class="input-group" style="width:35%; margin-right:90px;">
					  <span class="input-group-btn">
						  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]" style="margin-right:-35px; background: #F00 !important;">
							<span class="glyphicon glyphicon-minus"></span>
						  </button>
					  </span>
					  <input name="quant[2]" id="add_amount" class="form-control input-number" value="1" min="1" max="100" type="text">
					  <span class="input-group-btn">
						  <button type="button" class="btn btn-success btn-number" style="background:#008000 !important;" data-type="plus" data-field="quant[2]">
							  <span class="glyphicon glyphicon-plus"></span>
						  </button>
					  </span>
					 </div>

					<div class="help-block"></div>
				</div>


				</div>

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea class="form-control" name="" id="a_note" style="width:50%;"></textarea>
	</div>
	<div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" style="width:50%;" onclick="return saveAddfish();"> שמור </button></div>

    </div>


    <div id="menu1" class="tab-pane fade">



	  <div class="center">
				<div class="form-group field-fish-amount required">
					<label class="control-label" for="fish-amount">כמות</label>


          <div class="input-group" style="width:35%; margin-right:90px;">
          <span class="input-group-btn">
            <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]" style="margin-right:-35px; background: #F00 !important;">
            <span class="glyphicon glyphicon-minus"></span>
            </button>
          </span>
          <input name="quant[2]" id="m_amount" class="form-control input-number" value="1" min="1" max="100" type="text">
          <span class="input-group-btn">
            <button type="button" class="btn btn-success btn-number" style="background:#008000 !important;" data-type="plus" data-field="quant[2]">
              <span class="glyphicon glyphicon-plus"></span>
            </button>
          </span>
         </div>

					<div class="help-block"></div>
				</div>
				</div>
	<div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">מיכל</label>
		<select name="" id="move_cid" class="form-control" style="width:50%;">
		<?php foreach($fishModel->getContainerData() as $cdata){ ?>
		  <option value="<?php echo $cdata['cid']; ?>"><?php echo $cdata['container_name']; ?></option>
		<?php } ?>
		</select>
	</div>

	    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">סיבת העברה</label>
		<select name="" id="move_reason" class="form-control" style="width:50%;">
			<option value="" disabled selected>בחר סיבת העברה</option>
			<option value="לתצוגה, בריא">לתצוגה, בריא</option>
			<option value="לקרנטינה, מחלה">לקרנטינה, מחלה</option>
			<option value="לקרנטינה, העברה לתצוגה חדשה">לקרנטינה, העברה לתצוגה חדשה</option>
			<option value="אחר">אחר</option>
		</select>
		</div>

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea class="form-control" name="" id="m_note" style="width:50%;"></textarea>
	</div>

	<div class="form-group"><button type="button" class="btn topbtn" style="width:50%;" onclick="return movefish();">שמור</button></div>


    </div>



    <div id="menu2" class="tab-pane fade">



	   <div class="center">
				<div class="form-group field-fish-amount required">
					<label class="control-label" for="fish-amount">כמות</label>


          <div class="input-group" style="width:35%; margin-right:90px;">
          <span class="input-group-btn">
            <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]" style="margin-right:-35px; background: #F00 !important;">
            <span class="glyphicon glyphicon-minus"></span>
            </button>
          </span>
          <input name="quant[2]" id="d_amount" class="form-control input-number" value="1" min="1" max="100" type="text">
          <span class="input-group-btn">
            <button type="button" class="btn btn-success btn-number" style="background:#008000 !important;" data-type="plus" data-field="quant[2]">
              <span class="glyphicon glyphicon-plus"></span>
            </button>
          </span>
         </div>
					<div class="help-block"></div>
				</div>
				</div>

        <div class="form-group fish-s_name required">
          <label class="control-label" for="fish-name">סיבת מוות</label>
      		<select class="form-control" id="death_main_name" name="death_main_name" style="height: 36px; width: 50%" onchange="setseconddropdown(this.value);">
            <option disabled selected value="0">בחר סיבת מוות</option>
      		  <?php foreach($parentDrop as $parentDrops){ ?>
      			<option value="<?php echo $parentDrops['dead_id']; ?>"><?php echo $parentDrops['dead_name']; ?></option>
      		  <?php } ?>
      		  </select>

        </div>

        <div class="form-group field-fish-name required">
          <label class="control-label" for="fish-name">גורם</label>
          <select name="" id="death_second_name" class="form-control" style="width:50%;">
            <option  value="0" disabled selected>בחר גורם</option>
          </select>
        </div>

        <div class="form-group field-fish-name required">
    		<label class="control-label" for="fish-name">הערה</label>
    		<textarea class="form-control" name="" id="d_note" style="width:50%;"></textarea>
    	</div>

	<div class="form-group"><button type="button" class="btn topbtn" style="width:50%;" onclick="return deadfish();">שמור</button></div>


    </div>

  </div>






    </form>
</div>
<div class="col-sm-3"></div>

</div>
</div>
</div>

<style>
.center{width: 30%; }
#nav2 > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{background-color:#003366; color:#FFF; }
#nav2 > li > a {line-height: .72857143; color:#003366; background-color:#fff;}
#innertab{ border:none !important; }
.nav-tabs {
    border-bottom: none;
}

</style>

<script type="text/javascript">
function setseconddropdown(id){
$.ajax({
   method: "POST",
      url: "<?php echo Url::to(['fish/getsecond']); ?>",
     data: {id: id}
    })
   .done(function( msg ) {
 $("#death_second_name").html(msg);
   });
}

	function createfishback(){
        $("#preview").trigger('click');
	}

$(document).ready(function(){
    $(".nav-tabs a").click(function(){
		x = $(this).attr('href');
		x = x.replace("#","");
		if(x=='home'){
			$("#menu1").removeClass("in active");
			$("#menu2").removeClass("in active");
			$("#menu1").removeClass("show");
			$("#menu2").removeClass("show");
			$("#home").addClass("in active");
		}
		if(x=='menu1'){
			$("#menu1").addClass("in active");
			$("#home").removeClass("in active");
			$("#menu2").removeClass("in active");
			$("#home").removeClass("show");
			$("#menu2").removeClass("show");
		}
		if(x=='menu2'){
			$("#menu2").addClass("in active");
			$("#home").removeClass("in active");
			$("#menu1").removeClass("in active");
			$("#home").removeClass("show");
			$("#menu1").removeClass("show");
		}
   });
});

function deadfish(){
	    if($("#d_amount").val() ==''){
			alert("Please Enter Amount.");
			$("#d_amount").focus();
			return false;
		}

		d_amount   = $("#d_amount").val();
		d_note     = $("#d_note").val();
    death_main_name     = $("#death_main_name").val();
    death_second_name     = $("#death_second_name").val();
		f_id     = $("#f_id").val();

		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/deadfish']); ?>",
         data: { cid: "<?php echo $session->get('cid'); ?>", f_id:f_id, d_amount:d_amount, d_note:d_note,death_main_name,death_second_name, created_date:"<?php echo date ("Y-m-d H:i:s"); ?>"}
        })
       .done(function( msg ) {
		   alert("כמות עודכנה בהצלחה!");
         setTimeout(function() {
        $("#preview").trigger('click');
    },1);
       });
}

function saveAddfish(){
	    if($("#add_amount").val() ==''){
			alert("Please Enter Amount.");
			$("#add_amount").focus();
			return false;
		}

		d_amount   = $("#add_amount").val();
		d_note     = $("#a_note").val();
		f_id     = $("#f_id").val();

		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/saveaddfish']); ?>",
         data: { cid: "<?php echo $session->get('cid'); ?>", f_id:f_id, d_amount:d_amount, d_note:d_note, created_date:"<?php echo date ("Y-m-d H:i:s"); ?>"}
        })
       .done(function( msg ) {
		   alert("כמות עודכנה בהצלחה!");
         setTimeout(function() {
        $("#preview").trigger('click');
    },1);
       });
}

function movefish(){
	    if($("#m_amount").val() ==''){
			alert("Please Enter Amount.");
			$("#m_amount").focus();
			return false;
		}

		d_amount     = $("#m_amount").val();
		d_note       = $("#m_note").val();
		f_id         = $("#f_id").val();
		move_cid     = $("#move_cid").val();
		move_reason     = $("#move_reason").val();

		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/movefish']); ?>",
         data: { cid: "<?php echo $session->get('cid'); ?>", f_id:f_id, move_cid:move_cid, move_reason:move_reason, d_amount:d_amount, d_note:d_note, created_date:"<?php echo date ("Y-m-d H:i:s"); ?>"}
        })
       .done(function( msg ) {
		   alert("כמות עודכנה בהצלחה!");
         setTimeout(function() {
        $("#preview").trigger('click');
    },1);
       });
}


$('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }


});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
