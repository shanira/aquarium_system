<?php
use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;
$this->title = Yii::t('app', 'Create Fish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fish'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
	  <div class="col-sm-1">
	  <div class="btn-group pull-right"><button class="btn topbtn fa fa-chevron-right" onclick="return createfishback();"> חזור </button>
		</div>
	  </div>
	  <div class="col-sm-1"><button class="btn topbtn" id="upt" onclick="return toggle();">עדכן</button> </div>
	  <div class="col-sm-2"></div>
	  <div class="col-sm-4"><h1>פרטי הדג</h1></div>
	  <div class="col-sm-4">

       </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-2"></div>
<div class="col-sm-2">
<form id="w0" action="" method="post">
<input id="fid" class="form-control" name="fid" value="<?php echo $model->fid;?>" type="hidden">

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">שם הדג</label>
		<input id="fish_name" class="form-control" style="display:none;"  name="fish_name" maxlength="55" aria-required="true" type="text" value="<?php echo $model->name;?>">

		<div class="help-block" id="f_name"><?php echo $model->name;?></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">שם משני</label>
		<input id="s_name" style="display:none;"  class="form-control" name="s_name" maxlength="255" aria-required="true" type="text" value="<?php echo $model->s_name;?>">

		<div class="help-block" id="sc_name"><?php echo $model->s_name;?></div>
    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">משקל ממוצע</label>
		<input id="avg_weight" style="display:none;" class="form-control" name="avg_weight" maxlength="255" aria-required="true" type="text" value="<?php echo $model->avg_weight;?>">

		<div class="help-block" id="av"><?php echo $model->avg_weight;?></div>
    </div>
    </form>
</div>
<div class="col-sm-4"></div>
<div class="col-sm-4"></div>
</div>
</div>
</div>
<style>
#upt{background-color:#009933;}
</style>
<script>
       var flag = 1;
		function toggle(){
			if(flag==0){
				document.getElementById("upt").style.background="#009933";
				flag=1;
				document.getElementById("fish_name").style.display='none';
				document.getElementById("s_name").style.display='none';
				document.getElementById("avg_weight").style.display='none';
				$('.help-block').show();
				fish_name  = $("#fish_name").val();
		            s_name     = $("#s_name").val();
		            avg_weight = $("#avg_weight").val();
		$("#upt").text('עדכן');
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/updatefish']); ?>",
         data: { fid: "<?php echo $model->fid;?>", fish_name: fish_name, s_name:s_name, avg_weight:avg_weight}
        })
       .done(function( msg ) {
		   $("#f_name").text(fish_name);
           $("#sc_name").text(s_name);
		   $("#av").text(avg_weight);
       });


			}
			else if(flag==1){
				document.getElementById("upt").style.background="#ff0000";
				flag=0;
				$('.help-block').hide();
				$("#upt").text('שמור');
				document.getElementById("fish_name").style.display='block';
				document.getElementById("s_name").style.display='block';
				document.getElementById("avg_weight").style.display='block';



			}
		}

		function createfishback(){
          $("#preview").trigger('click');
	    }
        </script>
