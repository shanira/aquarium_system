<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  yii\web\Session;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fish');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12" style="background-color: #003366;padding: 30px;margin-top: 20px;">

		<div class="btn-group pull-center">
		<input type="text" class="form-control" id="myInput"  onkeyup="myFunction()" placeholder="חיפוש" title="Type in a name">
        </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">

		<div class="btn-group pull-right m-t-15">
		<a class="btn topbtn" href="#tab=w2-tab0" onclick="return createfish();">הוספת דג</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">


<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'tableOptions' => ['id' => 'myTable','class' => 'table table-striped table-bordered'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
			'attribute' => 'name',
			'format' => 'raw',
			'label' => 'שם הדג',
			'value' => function ($model) {
					return '<a href="JavaScript:void(0);" onclick="viewfishdetails('.$model->fid.');" >'.$model->name.'</a>';
			},
           ],

            'amount',
            //'created_date',
            //'updated_date',

            //['class' => 'yii\grid\ActionColumn'],
			['class' => 'yii\grid\ActionColumn',
	'buttons' => [

			'update' => function ($url, $model, $key) {
				   return '<a href="JavaScript:void(0);" onclick="updatefish('.$model->fid.');" ><span class="glyphicon glyphicon-pencil"></span></a>';
                },

			'delete' => function ($url, $model, $key)
				{
					return '<a href="JavaScript:void(0);" onclick="deletefish('.$model->fid.');" ><span class="glyphicon glyphicon-trash"></span></a>';

				},

            ],
            'template' => '{update} {delete}'


        ],
        ],
    ]); ?>

<?php Pjax::end(); ?>

</div>
</div>
</div>
<script>
function myFunction(){
	var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];

    if (td) {
		//alert(td.textContent.toUpperCase().toSource());
      if (td.textContent.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
		$(".summary").hide();
      }
    }
  }
}
</script>
