<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;


/* @var $this yii\web\View */
/* @var $model app\models\Fish */

$this->title = Yii::t('app', 'Create Fish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fish'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12">

		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn fa fa-chevron-right" onclick="return createfishback();"> חזור </a>
</div>

</div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">


    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">שם הדג</label>
		<input id="fish_name" class="form-control" name="fish_name" maxlength="55" aria-required="true" type="text" onblur="chkname(this.value);">

		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">שם מדעי</label>
		<input id="s_name" class="form-control" name="s_name" maxlength="255" aria-required="true" type="text">

		<div class="help-block"></div>
    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">משקל ממוצע</label>
		<input id="avg_weight" class="form-control" name="avg_weight" maxlength="255" aria-required="true" type="text">

		<div class="help-block"></div>
    </div>

	  <div class="center">
    <div class="form-group field-fish-amount required">
		<label class="control-label" for="fish-amount">כמות</label>


         <div class="input-group" style="width:35%; margin-right:90px;">
					<span class="input-group-btn">
						<button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]" style="margin-right:-35px; background: #F00 !important;">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
					</span>
					<input name="quant[2]" id="amount" class="form-control input-number" value="1" min="1" max="100" type="text">
					<span class="input-group-btn">
						<button type="button" class="btn btn-success btn-number" style="background:#008000 !important;" data-type="plus" data-field="quant[2]">
							<span class="glyphicon glyphicon-plus"></span>
						</button>
					</span>
				 </div>

		<div class="help-block"></div>
    </div>
	</div>
    <div class="form-group field-fish-created_date required">
		<input id="fish_created_date" class="form-control" name="fish_created_date" value="<?php echo date ("Y-m-d"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-updated_date required">
		<input id="fish_updated_date" class="form-control" name="fish_updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>

    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return savefish();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>

<style>
.center{width: 150px;
  margin: 8px auto;

}
</style>
<script>
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }


});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	function createfishback(){
        $("#preview").trigger('click');
	}

	function savefish(){
		if($("#fish_name").val() ==''){
			alert("Please Enter Fish Name.");
			$("#fish_name").focus();
			return false;
		}
		if($("#s_name").val() ==''){
			alert("Please Enter Fish Scientific Name.");
			$("#s_name").focus();
			return false;
		}
		if($("#avg_weight").val() ==''){
			alert("Please Enter Avg Weight.");
			$("#avg_weight").focus();
			return false;
		}
		if($("#amount").val() ==''){
			alert("Please Enter Amount.");
			$("#amount").focus();
			return false;
		}
		fish_name  = $("#fish_name").val();
		s_name     = $("#s_name").val();
		avg_weight = $("#avg_weight").val();
		amount     = $("#amount").val();
		fish_created_date     = $("#fish_created_date").val();
		fish_updated_date     = $("#fish_updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/savefish']); ?>",
         data: { cid: "<?php echo $session->get('cid'); ?>", fish_name: fish_name, s_name:s_name, avg_weight:avg_weight, fish_created_date:fish_created_date,  fish_updated_date:fish_updated_date, amount:amount}
        })
       .done(function( msg ) {
		   alert("Fish has been saved.");
         setTimeout(function() {
        $("#preview").trigger('click');
    },1);
       });
	}

	function chkname(names){
			$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/chkname']); ?>",
         data: { cid: "<?php echo $session->get('cid'); ?>", names: names}
        })
       .done(function( msg ) {
		   if(msg==1){
		   alert("דג מסוג זה כבר קיים במיכל. באפשרותך לערוך את הדג הקיים או להוסיף דג עם שם אחר.");
		  document.getElementById("fish_name").value = "";
		   return false;
		   }else{
			  return true;
			 }
          });
		}
</script>
