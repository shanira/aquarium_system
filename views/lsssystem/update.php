<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LssSystem */

$this->title = Yii::t('app', 'Update Lss System: ' . $model->lss_machine_id, [
    'nameAttribute' => '' . $model->lss_machine_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lss Systems'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lss_machine_id, 'url' => ['view', 'id' => $model->lss_machine_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lss-system-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
