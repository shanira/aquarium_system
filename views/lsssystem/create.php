<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LssSystem */

$this->title = Yii::t('app', 'הוספת LSS חדש');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lss Systems'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lss-system-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
