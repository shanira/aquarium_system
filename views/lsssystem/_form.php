<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LssSystem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lss-system-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lss_machine_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', ' שמור '), ['class' => 'btn topbtn fa fa-floppy-o']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
