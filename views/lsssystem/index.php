<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LssSystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'מערכות LSS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lss-system-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'LSS חדש'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'lss_machine_id',
            'lss_machine_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
