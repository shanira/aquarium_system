<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;

$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח משקל דגים</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-5" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getContainerFish(this.value);">
         <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-5" style="background-color:lavender;">שם הדג:
    <div class="input-group">
        <select id="fish_to" name="fish_to" class="form-control">
         <option value="">בחר דג</option>
          <?php if(isset(Yii::$app->request->post()['container']) && Yii::$app->request->post()['container'] !='all_container'){
			  $cnt  = Yii::$app->request->post()['container'];
              $cntData = $model->getFishDatas($cnt);
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }else{
			   $cntData = $model->getFishDataz();
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }

		 ?>
        </select>
        </div>
    </div>

	 <div class="col-sm-2" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>


    <!--div class="col-sm-2" style="background-color:lavender;">Amount:
    <div class="input-group">
        <select id="amount" name="amount" class="form-control">
         <option value="">Select</option>
         <option value="ASC">Smaller</option>
         <option value="DESC">Greater</option>
        </select>
        </div>
    </div-->

       <!--div class="col-sm-2" style="background-color:lavender;">Avg weight:
    <div class="input-group">
        <select id="awg_weight" name="awg_weight" class="form-control">
         <option value="">Select</option>
         <option value="ASC">Smaller</option>
         <option value="DESC">Greater</option>
        </select>
        </div>
    </div-->




  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>מיכל</th>
                    <th>שם הדג</th>
                    <th>כמות דגים</th>
                    <th>משקל ממוצע (בקילוגרם)</th>
                    <th>משקל דגים כולל (משקל ממוצע*כמות דגים)</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['name']; ?></td>
                    <td><?php echo $report['amount']; ?></td>
                    <td><?php echo $report['avg_weight']; ?></td>
                    <td><?php echo $report['avg_weight']*$report['amount']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
            <tfoot align="right">
		<tr><th></th><th></th><th></th><th></th><th></th></tr>
	</tfoot>
        </table>

        <?php

		?>
    </div>
    </div>
    </div>

</div>




         <script type="text/javascript">
            $(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf'],
					"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
// alert(total);
            // Total over this page
            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

			 avgTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 //alert(pageTotal);
            // Update footer
			$( api.column( 4 ).footer() ).html(avgTotal);
            $( api.column( 2 ).footer() ).html(pageTotal);
			$( api.column( 0 ).footer() ).html('סה"כ');
        }
                });

               table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});


			function getContainerFish(fid){
	 ///alert(fid);
				 $.ajax({
				   method: "POST",
					  url: "<?php echo Url::to(['report/getcontainerfish']); ?>",
					 data: { fid: fid }
				  })
				   .done(function( msg ) {
				   $("#fish_to").empty().html(msg);
				});
			 }


        </script>
