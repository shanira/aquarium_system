<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;

$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח אבחון</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-1" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getContainerFish(this.value);">
         <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-1" style="background-color:lavender;">שם הדג:
    <div class="input-group">
        <select id="fish_to" name="fish_to" class="form-control">
         <option value="">All</option>
          <?php if(isset(Yii::$app->request->post()['container']) && Yii::$app->request->post()['container'] !='all_container'){
			  $cnt  = Yii::$app->request->post()['container'];
              $cntData = $model->getFishDatas($cnt);
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }else{
			   $cntData = $model->getFishDataz();
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }

		 ?>
        </select>
        </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">שם האבחון:
    <div class="input-group">
    <?php
	$diagonosticeData = $model->getDiagostic();
	?>
        <select id="diagonsticname" name="diagonsticname" class="form-control" onchange="getDiagonsticType(this.value);">
        <option value="">All</option>
         <?php foreach($diagonosticeData as $dname){ ?>
         <option value="<?php echo $dname['d_id']; ?>"  <?php if(isset(Yii::$app->request->post()['diagonsticname'])){ if(Yii::$app->request->post()['diagonsticname']==$dname['d_id']){ echo "selected"; } } ?> ><?php echo $dname['d_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">סוג אבחון:
    <div class="input-group">
        <select id="dtype" name="dtype" class="form-control">
        <option value="">All</option>
          <?php if(!empty(Yii::$app->request->post()['diagonsticname'])){
			  $name  = Yii::$app->request->post()['diagonsticname'];
              $cntData = $model->getdiagonstictypes($name);
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['d_id']; ?>"  <?php if(isset(Yii::$app->request->post()['dtype'])){ if(Yii::$app->request->post()['dtype']==$rows['d_id']){ echo "selected"; } } ?> ><?php echo $rows['d_name']; ?></option>
                  <?php
				}
		  }else{
			   $cntData = $model->getdiagonstictypez();
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['d_id']; ?>"  <?php if(isset(Yii::$app->request->post()['dtype'])){ if(Yii::$app->request->post()['dtype']==$rows['d_name']){ echo "selected"; } } ?> ><?php echo $rows['d_name']; ?></option>
                  <?php
				}
		  }

		 ?>

        </select>
        </div>
    </div>

     <div class="col-sm-1" style="background-color:lavender;">נוצר ע"י:
    <div class="input-group">
    <?php
	$empData = $model->getEmployee();
	?>
        <select id="emp" name="emp" class="form-control">
        <option value="">All</option>
         <?php foreach($empData as $edata){ ?>
         <option value="<?php echo $edata['name']; ?>"  <?php if(isset(Yii::$app->request->post()['emp'])){ if(Yii::$app->request->post()['emp']==$edata['name']){ echo "selected"; } } ?> ><?php echo $edata['name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>


    <div class="col-sm-2" style="background-color:lavender;">תאריך:
      <div class="input-group">
                <input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />

      </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">הערה:
        <div class="input-group">
        <input type="text" name="comments" class="form-control" value="" />
        </div>
    </div>

    <div class="col-sm-1" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>שם הדג</th>
                    <th>מיכל</th>
                    <th>שם האבחון</th>
                    <th>סוג האבחון</th>
                    <th>הערה</th>
                    <th>תאריך</th>
                    <th>נוצר ע"י</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['name']; ?></td>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['d_name']; ?></td>
                    <td><?php echo $model->getDiagnosticType($report['diagnostic_type']); ?></td>
                    <td><?php echo $report['comments']; ?></td>
                    <td><?php echo $report['date_created']; ?></td>
                    <td><?php echo $report['created_by']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
        </table>

        <?php

		?>
    </div>
    </div>
    </div>

</div>




         <script type="text/javascript">
            $(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf'],

                });

               table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});


			function getContainerFish(fid){
	 ///alert(fid);
				 $.ajax({
				   method: "POST",
					  url: "<?php echo Url::to(['report/getcontainerfish']); ?>",
					 data: { fid: fid }
				  })
				   .done(function( msg ) {
				   $("#fish_to").empty().html(msg);
				});
			 }

			 function getDiagonsticType(fid){
	        // alert(fid);
				 $.ajax({
				   method: "POST",
					  url: "<?php echo Url::to(['report/diagonstictype']); ?>",
					 data: { fid: fid }
				  })
				   .done(function( msg ) {
				   $("#dtype").empty().html(msg);
				});
			 }


        </script>
