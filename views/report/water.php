<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;
$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח מים</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-3" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control">
         <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

     <div class="col-sm-3" style="background-color:lavender;">מדד מים:
    <div class="input-group">
        <select id="sys" name="sys" class="form-control">
         <option value="">All</option>
         <option value="water_level">Water level</option>
         <option value="temperature">Temperature</option>
         <option value="co2">Co2</option>
         <option value="no2">No2</option>
         <option value="no3">No3</option>
         <option value="po4">Po4</option>
         <option value="hardness">Hardness</option>
         <option value="bromines">Bromines</option>
         <option value="copper">Copper</option>
         <option value="ca">CA</option>
         <option value="salinity">Salinity</option>
         <option value="ph">Ph</option>
         <option value="alkalinity">Alkalinity</option>
         <option value="tan">Tan</option>
         <option value="uia">Uia</option>
         <option value="do_mg">Do Mg</option>
         <option value="do">DO</option>
         <option value="mg">MG</option>
         <option value="strontium">Strontium</option>
        </select>
        </div>
    </div>

    <div class="col-sm-4" style="background-color:lavender;">תאריך
      <div class="input-group">
                מ<input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['end']){ echo Yii::$app->request->post()['end']; } } ?>" />
      </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>

  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>מיכל</th>
                    <th>co2</th>
                    <th>no2</th>
                    <th>no3</th>
                    <th>po4</th>
                    <th>hardness</th>
                    <th>bromines</th>
                    <th>copper</th>
                    <th>ca</th>
                    <th>salinity</th>
                    <th>ph</th>
                    <th>alkalinity</th>
                    <th>tan</th>
                    <th>uia</th>
                    <th>do_mg</th>
                    <th>do</th>
                    <th>mg</th>
                    <th>תאריך</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['co2']; ?></td>
                    <td><?php echo $report['no2']; ?></td>
                    <td><?php echo $report['no3']; ?></td>
                    <td><?php echo $report['po4']; ?></td>
                    <td><?php echo $report['hardness']; ?></td>
                    <td><?php echo $report['bromines']; ?></td>
                    <td><?php echo $report['copper']; ?></td>
                    <td><?php echo $report['ca']; ?></td>
                    <td><?php echo $report['salinity']; ?></td>
                    <td><?php echo $report['ph']; ?></td>
                    <td><?php echo $report['alkalinity']; ?></td>
                    <td><?php echo $report['tan']; ?></td>
                    <td><?php echo $report['uia']; ?></td>
                    <td><?php echo $report['do_mg']; ?></td>
                    <td><?php echo $report['do']; ?></td>
                    <td><?php echo $report['mg']; ?></td>
                    <td><?php echo $report['updated_date']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>




</div>
<script type="text/javascript">
$(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});

   </script>
