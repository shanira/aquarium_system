<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;
$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דוח דגימות מים</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-3" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control">
         <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

     <div class="col-sm-3" style="background-color:lavender;">מדד מים:
    <div class="input-group">
        <select id="sys" name="sys" class="form-control">
         <option value="">All</option>
         <option value="CO2">CO2</option>
         <option value="NO2">NO2</option>
         <option value="NO3">NO3</option>
         <option value="PO4">PO4</option>
         <option value="Hardness">Hardness</option>
         <option value="Bromines">Bromines</option>
         <option value="Copper">Copper</option>
         <option value="Ca">Ca</option>
         <option value="Salinity">Salinity</option>
         <option value="pH">pH</option>
         <option value="Alkalinity">Alkalinity</option>
         <option value="TAN">Tan</option>
         <option value="UIA">UIA</option>
         <option value="DO mg/l">DO mg/l</option>
         <option value="DO %">DO %</option>
         <option value="Mg">Mg</option>
         <option value="Strontium">Strontium</option>
        </select>
        </div>
    </div>

    <div class="col-sm-4" style="background-color:lavender;">תאריך
      <div class="input-group">
                מ<input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['end']){ echo Yii::$app->request->post()['end']; } } ?>" />
      </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>

  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>מיכל</th>
                    <th>sample_name</th>
                    <th>sample_value</th>
                    <th>comment</th>
                    <th>תאריך</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['sample_name']; ?></td>
                    <td><?php echo $report['sample_value']; ?></td>
					<td><?php echo $report['comment']; ?></td>
                    <td><?php echo $report['updated_date']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>




</div>
<script type="text/javascript">
$(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});

   </script>
