<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;
use app\models\Lss;
$model = new Fish();
$lmodel = new Lss();
$cntData              = $model->getContainerData();
$lssData              = $model->getLssMeachineData();
$lssData              = $lmodel->getLSSData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח LSS</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-3" style="background-color:lavender;">מיכל
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getLssmeachine(this.value);">
          <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>
    <div class="col-sm-3" style="background-color:lavender;">Lss מכונת
    <div class="input-group">
        <select id="lssmachine" name="lssmachine" class="form-control">
          <option value="">Select Machine</option>
         <?php foreach($lssData as $lssDatas){ ?>
         <option value="<?php echo $lssDatas['lss_machine_id']; ?>"  <?php if(isset(Yii::$app->request->post()['lssmachine'])){ if(Yii::$app->request->post()['lssmachine']==$lssDatas['lss_machine_id']){ echo "selected"; } } ?> ><?php echo $lssDatas['lss_machine_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">ערך
        <div class="input-group">
          <select name="comments" class="form-control">
           <option value="">All</option>
           <option value="X">X</option>
           <option value="Low">Low</option>
           <option value="High">High</option>
          </select>
        </div>
    </div>


    <div class="col-sm-4" style="background-color:lavender;">תאריך
      <div class="input-group">
                <input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['end']){ echo Yii::$app->request->post()['end']; } } ?>" />
      </div>
    </div>

    <div class="col-sm-12" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn col-sm-2" value="חפש" style="margin-top:25px;" />
        </div>
    </div>

  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>מיכל</th>
                    <th>מערכת LSS</th>
                    <th>ערך</th>
                    <th>תאריך</th>
                    <th>Last Check/Pre Show</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['lss_machine_name']; ?></td>
                    <td><?php echo $report['machine_value']; ?></td>
                    <td><?php echo $report['date_created']; ?></td>
                    <td><?php echo $report['status']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>




</div>
<script type="text/javascript">
$(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});

	function getLssmeachine(id){
		alert(id);
		 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['report/lssmachinedrop']); ?>",
         data: { cid:id }
      })
       .done(function( msg ) {
       $("#lssmachine").empty().html(msg);
    });
		}


        </script>
