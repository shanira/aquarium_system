<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;
$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח מזון</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-3" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getContainerFish(this.value);">
          <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">שם הדג:
    <div class="input-group">
        <select id="fish_to" name="fish_to" class="form-control">
         <option value="">All</option>
          <?php if(isset(Yii::$app->request->post()['container']) && Yii::$app->request->post()['container'] !='all_container'){
			  $cnt  = Yii::$app->request->post()['container'];
              $cntData = $model->getFishDatas($cnt);
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['fid']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['fid']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }else{
			   $cntData = $model->getFishDataz();
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['fid']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['fid']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }

		 ?>
        </select>
        </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">יום בשבוע:
    <div class="input-group">
        <select id="week_day" name="week_day" class="form-control">
          <option value="">All</option>
          <option value="Sunday">Sunday</option>
          <option value="Monday">Monday</option>
          <option value="Tuesday">Tuesday</option>
          <option value="Wednesday">Wednesday</option>
          <option value="Thursday">Thursday</option>
          <option value="Friday">Friday</option>
          <option value="Saturday">Saturday</option>
        </select>
        </div>
    </div>

     <div class="col-sm-2" style="background-color:lavender;">סוג מזון:
    <div class="input-group">
    <?php
	$empData = $model->getFoodType();
	?>
        <select id="emp" name="emp" class="form-control">
        <option value="">All</option>
         <?php foreach($empData as $edata){ ?>
         <option value="<?php echo $edata['foodt_id']; ?>"  <?php if(isset(Yii::$app->request->post()['emp'])){ if(Yii::$app->request->post()['emp']==$edata['foodt_id']){ echo "selected"; } } ?> ><?php echo $edata['food_type']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>



    <div class="col-sm-2" style="background-color:lavender;">יחידות
      <div class="input-group">
        <select name="unit" class="form-control">
          <option value="">All</option>
          <option value="קילוגרם">קילוגרם</option>
          <option value="גרם">גרם</option>
          <option value="יחידות">יחידות</option>
        </select>

      </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">סטטוס האכלה:
      <div class="input-group">
<select name="actual_amount" class="form-control">
        <option value="">All</option>
        <option value="אכל" >אכל</option>
        <option value="לא אכל" >לא אכל</option>
        <option value="אכל חלקית" >אכל חלקית</option>
        <option value="אכל פחות">אכל פחות</option>
        <option value="אכל יותר">אכל יותר</option>
</select>
      </div>
    </div>

    <div class="col-sm-6" style="background-color:lavender;">תאריך:
      <div class="input-group">
                מ<input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['start'])){ echo Yii::$app->request->post()['start'];  } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['end'])){ echo Yii::$app->request->post()['end']; } ?>" />
      </div>
    </div>

    <div class="col-sm-4" style="background-color:lavender;">
        <div class="input-group col-sm-6">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>

  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>מיכל</th>
                    <th>עבור</th>
                    <th>יום בשבוע</th>
                    <th>סוג מזון</th>
                    <th>יחידות</th>
                    <th>כמות</th>
                    <th>הערה</th>
                    <th>סטטוס האכלה</th>
                    <th>הערת מאכיל</th>
                    <th>תאריך</th>

                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php if($report['fish_to']=='ALL'){echo $report['fish_to']; } else{ echo $report['name'];} ?></td>
                    <td><?php echo $report['day_of_week']; ?></td>
                    <td><?php echo $report['fttype']; ?></td>
                    <td><?php echo $report['unit']; ?></td>
                    <td><?php echo $report['amount']; ?></td>
                    <td><?php echo $report['comment']; ?></td>
                    <td><?php echo $report['actual_amount']; ?></td>
                    <td><?php echo $report['comment_w']; ?></td>
                    <td><?php echo $report['created_date']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
             <tfoot align="right">
		<tr><th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr>
	</tfoot>
        </table>
    </div>
    </div>
    </div>




</div>
<script type="text/javascript">
          $(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf'],
					"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
// alert(total);
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 //alert(pageTotal);
            // Update footer
            $( api.column( 5 ).footer() ).html(pageTotal);
			$( api.column( 0 ).footer() ).html('סה"כ');
        }

                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});

			function getContainerFish(fid){
	 ///alert(fid);
				 $.ajax({
				   method: "POST",
					  url: "<?php echo Url::to(['report/getcontainerfish']); ?>",
					 data: { fid: fid }
				  })
				   .done(function( msg ) {
				   $("#fish_to").empty().html(msg);
				});
			 }
	 </script>
