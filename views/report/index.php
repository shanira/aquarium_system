<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
?>
<h1>דוחות</h1>

          <ul class="submenu" style="list-style-type: none !important">
						  <li class="space"><a href="<?= Url::to(['report/fishcount' ])?>"><?php echo Yii::t('app', 'דוח כמות דגים' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['report/fish' ])?>"><?php echo Yii::t('app', 'דוח היסטוריית דגים' ); ?></a></li>
              <li class="space"><a href="<?= Url::to(['report/weightreport' ])?>"><?php echo Yii::t('app', 'דוח משקל דגים' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['report/sample' ])?>"><?php echo Yii::t('app', 'דוח דגימות מים' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['report/food' ])?>"><?php echo Yii::t('app', 'דוח מזון' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['report/lss' ])?>"><?php echo Yii::t('app', 'דוח LSS' ); ?></a></li>
              <li class="space"><a href="<?= Url::to(['report/vetdiagno' ])?>"><?php echo Yii::t('app', 'דוח אבחון' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['report/vet' ])?>"><?php echo Yii::t('app', 'דוח טיפול' ); ?></a></li>
          </ul>

<style>
.space{
  padding: 10px !important;
      }
  <style>
