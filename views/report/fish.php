<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;

$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח היסטורית דגים</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">
    <div class="col-sm-5" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getContainerFish(this.value);">
         <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-5" style="background-color:lavender;">שם הדג:
    <div class="input-group">
        <select id="fish_to" name="fish_to" class="form-control">
         <option value="">All</option>
          <?php if(isset(Yii::$app->request->post()['container']) && Yii::$app->request->post()['container'] !='all_container'){ 
        $cnt  = Yii::$app->request->post()['container'];
              $cntData = $model->getFishDatas($cnt);
        foreach($cntData as $rows){
          ?>
          <option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
        }
      }else{
         $cntData = $model->getFishDataz();
        foreach($cntData as $rows){ 
          ?>
          <option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
        }
      }

     ?>
        </select>
        </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">סיבה
      <div class="input-group">
       <select id="reason" name="reason" class="form-control">
         <option value="all_reason">All</option>
         <option value="addition" <?php if(isset(Yii::$app->request->post()['reason'])){if(Yii::$app->request->post()['reason']=='addition'){ echo "selected"; } }?>>Added</option>
         <option value="death" <?php if(isset(Yii::$app->request->post()['reason'])){ if(Yii::$app->request->post()['reason']=='death'){ echo "selected"; } } ?>>Death</option>
         <option value="move" <?php if(isset(Yii::$app->request->post()['reason'])){ if(Yii::$app->request->post()['reason']=='move'){ echo "selected"; }} ?>>Moved</option>
        </select>
        </div>
    </div>
    <div class="col-sm-4" style="background-color:lavender;">תאריך:
      <div class="input-group">
                מ<input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['end']){ echo Yii::$app->request->post()['end']; } } ?>" />
      </div>
    </div>

    <div class="col-sm-2" style="background-color:lavender;">הערה
        <div class="input-group">
        <input type="text" name="comments" class="form-control" value="" />
        </div>
    </div>


      <div class="col-sm-1" style="background-color:lavender;">
          <div class="input-group">
          <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
          </div>
      </div>

  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>שם הדג</th>
                    <th>מיכל</th>
                    <th>כמות</th>
                    <th>הוספה</th>
                    <th>העברה</th>
                    <th>מוות</th>
                    <th>הערה הוספה</th>
                    <th>הערה העברה</th>
                    <th>סיבת מוות</th>
                    <th>סוג המוות</th>
                    <th>הערה מוות</th>
                    <th>תאריך</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['name']; ?></td>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['current_num']; ?></td>
                    <td><?php echo $report['addition']; ?></td>
                    <td><?php echo $report['move']; ?></td>
                    <td><?php echo $report['death']; ?></td>
                    <td><?php echo $report['a_note']; ?></td>
                    <td><?php echo $report['m_note']; ?></td>
                    <td><?php echo $model->getDeadNamebyId($report['death_main_name']); ?></td>
                    <td><?php echo $model->getDeadNamebyId($report['death_second_name']); ?></td>
                    <td><?php echo $report['d_note']; ?></td>
                    <td><?php echo $report['created_date']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
            <tfoot align="right">
            <tr> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th><th></th> <th></th> <th></th> <th></th> </tr>
            </tfoot>
        </table>

        <?php

		?>
    </div>
    </div>
    </div>

</div>


         <script type="text/javascript">
            $(document).ready(function() {
               $.noConflict();
                //Buttons examples
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf'],
            "footerCallback": function ( row, data, start, end, display ) {
                      var api = this.api(), data;

                      // Remove the formatting to get integer data for summation
                      var intVal = function ( i ) {
                          return typeof i === 'string' ?
                              i.replace(/[\$,]/g, '')*1 :
                              typeof i === 'number' ?
                                  i : 0;
                      };

                      // Total over all pages
                      total = api
                          .column( 2 )
                          .data()
                          .reduce( function (a, b) {
                              return intVal(a) + intVal(b);
                          }, 0 );
          // alert(total);
                      // Total over this page
                      pageTotal = api
                          .column( 2, { page: 'current'} )
                          .data()
                          .reduce( function (a, b) {
                              return intVal(a) + intVal(b);
                          }, 0 );

          			 amount = api
                          .column( 4 )
                          .data()
                          .reduce( function (a, b) {
                              return intVal(a) + intVal(b);
                          }, 0 );

                death = api
                          .column( 5 )
                          .data()
                          .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                             }, 0 );

                 move = api
                          .column( 4 )
                          .data()
                          .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                            }, 0 );

                addition = api
                          .column( 3 )
                          .data()
                          .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                            }, 0 );
           //alert(pageTotal);
                      // Update footer
                $( api.column( 4 ).footer() ).html(move);
                $( api.column( 3 ).footer() ).html(addition);
                $( api.column( 5 ).footer() ).html(death);
          			$( api.column( 0 ).footer() ).html('סה"כ');
                  }
                      });
                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});


      function getContainerFish(fid){
   ///alert(fid);
         $.ajax({
           method: "POST",
            url: "<?php echo Url::to(['report/getcontainerfish']); ?>",
           data: { fid: fid }
          })
           .done(function( msg ) {
           $("#fish_to").empty().html(msg);
        });
       }


        </script>
