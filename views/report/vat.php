<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Fish;
$model = new Fish();
$cntData              = $model->getContainerData();
/* @var $this yii\web\View */
?>

<div class="container">
  <h1>דו"ח טיפול</h1>
  <?php
  $form = ActiveForm::begin([
    'id' => 'fish-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
  <div class="row">

    <div class="col-sm-3" style="background-color:lavender;">מיכל:
    <div class="input-group">
        <select id="container" name="container" class="form-control" onchange="getContainerFish(this.value);">
          <option value="all_container">All</option>
         <?php foreach($cntData as $cData){ ?>
         <option value="<?php echo $cData['cid']; ?>"  <?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['container']==$cData['cid']){ echo "selected"; } } ?> ><?php echo $cData['container_name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">שם הדג:
    <div class="input-group">
        <select id="fish_to" name="fish_to" class="form-control">
         <option value="">Select Fish</option>
          <?php if(isset(Yii::$app->request->post()['container']) && Yii::$app->request->post()['container'] !='all_container'){
			  $cnt  = Yii::$app->request->post()['container'];
              $cntData = $model->getFishDatas($cnt);
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }else{
			   $cntData = $model->getFishDataz();
			  foreach($cntData as $rows){
				  ?>
					<option value="<?php echo $rows['name']; ?>"  <?php if(isset(Yii::$app->request->post()['fish_to'])){ if(Yii::$app->request->post()['fish_to']==$rows['name']){ echo "selected"; } } ?> ><?php echo $rows['name']; ?></option>
                  <?php
				}
		  }

		 ?>
        </select>
        </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">תאריך:
      <div class="input-group">
                <input type="date" class="form-control" name="start" value="<?php if(isset(Yii::$app->request->post()['container'])){if(Yii::$app->request->post()['start']){ echo Yii::$app->request->post()['start']; } } ?>" />
                עד
                <input type="date" class="form-control" name="end" value="<?php if(isset(Yii::$app->request->post()['container'])){ if(Yii::$app->request->post()['end']){ echo Yii::$app->request->post()['end']; } } ?>" />
      </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">נוצר ע"י:
    <div class="input-group">
    <?php
  $empData = $model->getEmployee();
  ?>
        <select id="emp" name="emp" class="form-control">
        <option value="">Select</option>
         <?php foreach($empData as $edata){ ?>
         <option value="<?php echo $edata['name']; ?>"  <?php if(isset(Yii::$app->request->post()['emp'])){ if(Yii::$app->request->post()['emp']==$edata['name']){ echo "selected"; } } ?> ><?php echo $edata['name']; ?></option>
         <?php } ?>
        </select>
        </div>
    </div>


    <div class="col-sm-3" style="background-color:lavender;">טיפול:
      <div class="input-group">
                <input type="text" class="form-control" name="treatment" value="<?php if(isset(Yii::$app->request->post()['treatment'])){if(Yii::$app->request->post()['treatment']){ echo Yii::$app->request->post()['treatment']; } } ?>" />

      </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">מינון:
      <div class="input-group">
                <input type="text" class="form-control" name="specific_treatment" value="<?php if(isset(Yii::$app->request->post()['specific_treatment'])){if(Yii::$app->request->post()['specific_treatment']){ echo Yii::$app->request->post()['specific_treatment']; } } ?>" />

      </div>
    </div>






    <div class="col-sm-3" style="background-color:lavender;">הערה:
        <div class="input-group">
        <input type="text" name="comments" class="form-control" value="" />
        </div>
    </div>

    <div class="col-sm-3" style="background-color:lavender;">
        <div class="input-group">
        <input type="submit" name="submit" class="btn topbtn" value="חפש" style="margin-top:25px;" />
        </div>
    </div>


  </div>
  <?php ActiveForm::end() ?>



  <div class="row">
 <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-bordered" id="datatable-buttons" width="100%" style="width:100%; table-layout: fixed;">
            <thead>
                <tr>
                    <th>שם הדג</th>
                    <th>מיכל</th>
                    <th>טיפול</th>
                    <th>מינון</th>
                    <th>תדירות</th>
                    <th>דילול</th>
                    <th>צורת נתינה</th>
                    <th>הערה</th>
                    <th>נוצר ע"י</th>
                    <th>תאריך</th>
                </tr>
            </thead>
            <tbody>
            <?php
			if(!empty($reportData)){
			foreach($reportData as $report){ ?>
                <tr>
                    <td><?php echo $report['name']; ?></td>
                    <td><?php echo $report['container_name']; ?></td>
                    <td><?php echo $report['treatment']; ?></td>
                    <td><?php echo $report['specific_treatment']; ?></td>
                    <td><?php echo $report['frequency']; ?></td>
                    <td><?php echo $report['dilution']; ?></td>
                    <td><?php echo $report['how_given']; ?></td>
                    <td><?php echo $report['comments']; ?></td>
                    <td><?php echo $report['created_by']; ?></td>
                    <td><?php echo $report['date_created']; ?></td>
                </tr>
              <?php } } ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>




</div>
<script type="text/javascript">
$(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});


			function getContainerFish(fid){
	 ///alert(fid);
				 $.ajax({
				   method: "POST",
					  url: "<?php echo Url::to(['report/getcontainerfish']); ?>",
					 data: { fid: fid }
				  })
				   .done(function( msg ) {
				   $("#fish_to").empty().html(msg);
				});
			 }
        </script>
