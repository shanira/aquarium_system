<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Food */

$this->title = Yii::t('app', 'Create Food');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Foods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12">
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn" href="#tab=w0-tab2" onclick="return backfood();">Back</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">


    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">Food Type</label>
		<input id="food_type" class="form-control" name="food_type" maxlength="55" aria-required="true" type="text" value="<?php echo $model->food_type; ?>">

		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Unit</label>
		<select class="form-control" id="unit" name="unit" style="height: 36px;">
			<option value="Gram" <?php if($model->unit=='Gram'){ echo "selected"; }?>>Gram</option>
			<option value="Ponds" <?php if($model->unit=='Ponds'){ echo "selected"; }?>>Ponds</option>
			<option value="Kg" <?php if($model->unit=='Kg'){ echo "selected"; }?>>Kg</option>
		  </select>

    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Amount</label>
		<input id="amount" class="form-control" name="amount" maxlength="255" aria-required="true" type="text" value="<?php echo $model->amount; ?>">

		<div class="help-block"></div>
    </div>

    <div class="form-group field-fish-amount required">
		<label class="control-label" for="fish-amount">Actual Unit</label>
		<select class="form-control" id="actual_unit" name='actual_unit' style="height: 36px;">
			<option value="Gram" <?php if($model->actual_unit=='Gram'){ echo "selected"; }?>>Gram</option>
			<option value="Ponds" <?php if($model->actual_unit=='Ponds'){ echo "selected"; }?>>Ponds</option>
			<option value="Kg" <?php if($model->actual_unit=='Kg'){ echo "selected"; }?>>Kg</option>
		  </select>

    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Actual Amount</label>
		<input id="actual_amount" class="form-control" name="actual_amount" maxlength="255" aria-required="true" type="text" value="<?php echo $model->actual_amount; ?>">

		<div class="help-block"></div>
    </div>

    <div class="form-group field-fish-created_date required">
	    <input id="food_id" class="form-control" name="food_id" value="<?php echo $model->food_id; ?>" type="hidden">
		<input id="created_date" class="form-control" name="created_date" value="<?php echo date ("Y-m-d"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-updated_date required">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>

    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return updatefood();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>
	function backfood(){
        $("#food").trigger('click');
	}

</script>
