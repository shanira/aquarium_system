<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Food */

$this->title = Yii::t('app', 'Create Food');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Foods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12">
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn" href="#tab=w0-tab2" onclick="return backfood();">Back</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">


    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">Food Type</label>
		<input id="food_type" class="form-control" name="food_type" maxlength="55" aria-required="true" type="text">

		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Unit</label>
		<select class="form-control" id="unit" name="unit" style="height: 36px;">
			<option value="Gram">Gram</option>
			<option value="Ponds">Ponds</option>
			<option value="Kg">Kg</option>
		  </select>

    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Amount</label>
		<input id="amount" class="form-control" name="amount" maxlength="255" aria-required="true" type="text">

		<div class="help-block"></div>
    </div>

    <div class="form-group field-fish-amount required">
		<label class="control-label" for="fish-amount">Actual Unit</label>
		<select class="form-control" id="actual_unit" name='actual_unit' style="height: 36px;">
			<option value="Gram">Gram</option>
			<option value="Ponds">Ponds</option>
			<option value="Kg">Kg</option>
		  </select>

    </div>

	<div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">Actual Amount</label>
		<input id="actual_amount" class="form-control" name="actual_amount" maxlength="255" aria-required="true" type="text">

		<div class="help-block"></div>
    </div>

    <div class="form-group field-fish-created_date required">

		<input id="created_date" class="form-control" name="created_date" value="<?php echo date ("Y-m-d"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-updated_date required">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
		<div class="help-block"></div>
	</div>

    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return savefood();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>
	function backfood(){
        $("#food").trigger('click');
	}
	function savefood(){
		if($("#food_type").val() ==''){
			alert("Please Enter Food Type.");
			$("#food_type").focus();
			return false;
		}
		if($("#amount").val() ==''){
			alert("Please Enter Amount.");
			$("#amount").focus();
			return false;
		}
		if($("#actual_amount").val() ==''){
			alert("Please Enter Actual Amount.");
			$("#actual_amount").focus();
			return false;
		}

		food_type  = $("#food_type").val();
		unit       = $("#unit").val();
		amount     = $("#amount").val();
		actual_unit    = $("#actual_unit").val();
		actual_amount  = $("#actual_amount").val();

		created_date     = $("#created_date").val();
		updated_date     = $("#updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/savefood']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", food_type: food_type, unit:unit,  amount:amount, actual_unit:actual_unit,  actual_amount:actual_amount, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("Food has been saved.");
         setTimeout(function() {
        $("#food").trigger('click');
    },1);
       });
	}
</script>
