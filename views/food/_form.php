<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Food */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="food-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_id')->textInput() ?>

    <?= $form->field($model, 'food_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit')->dropDownList([ 'Gram' => 'Gram', 'Ponds' => 'Ponds', 'Kg' => 'Kg', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'actual_unit')->dropDownList([ 'Gram' => 'Gram', 'Ponds' => 'Ponds', 'Kg' => 'Kg', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'actual_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
