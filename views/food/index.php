<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use  yii\web\Session;
use app\models\Food;
use app\models\Lss;
use yii\helpers\Url;
$session = Yii::$app->session;
$foodTable = new Food;
$foodTable = new Lss;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Foods');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

	<div class="col-sm-1 col-md-1 col-lg-12">
		<div class="btn-group pull-right">
		 תאריך:  <?php echo $current_update; ?>
		</div>
    </div>
</div>

<div class="row">



    <form>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="text-align:center;">סוג מזון</th>
        <th style="text-align:center;">יום בשבוע</th>
        <th style="text-align:center;">עבור</th>
        <th style="text-align:center;">כמות</th>
        <th style="text-align:center;">יחידות</th>
        <th style="text-align:center;">סטטוס האכלה</th>
		<!--th style="text-align:center;">Actual Unit:</th-->
        <th style="text-align:center;">הערה</th>
				<th style="text-align:center;">הערת מאכיל</th>
      </tr>
    </thead>
    <tbody>
	<?php
	$commaString = array();

	foreach($data as $rows){
	if( date("l")==$rows['day_of_week']){
		$commaString[] = $rows['food_id'];
		$oDate = strtotime($rows['updated_date']);
       $sDate = date("Y-m-d",$oDate);
	   $curDate  = date("Y-m-d");
	?>
      <tr data-key="<?php echo $rows['food_id']; ?>">
        <td><?php echo $foodTable->getFoodType($rows['food_type']); ?></td>
        <td><?php echo $rows['day_of_week']; ?></td>
        <td><?php echo $foodTable->GetFishName($rows['fish_to']); ?></td>
        <td><?php echo $rows['amount']; ?></td>
				<td><?php echo $rows['unit']; ?></td>

		<td>
		  <select class="form-control" id="actualamount_<?php echo $rows['food_id']; ?>">
			<option value="" <?php if($sDate !=$curDate){ echo "disabled selected"; }?>>בחר סטטוס</option>
			<option value="אכל" <?php if($sDate ==$curDate){ if($rows['actual_amount']=='אכל'){ echo "selected"; }}?>>אכל</option>
			<option value="לא אכל" <?php if($sDate ==$curDate){if($rows['actual_amount']=='לא אכל'){ echo "selected"; }}?>>לא אכל</option>
			<option value="אכל חלקית" <?php if($sDate ==$curDate){if($rows['actual_amount']=='אכל חלקית'){ echo "selected"; }}?>>אכל חלקית</option>
            <option value="אכל פחות" <?php if($sDate ==$curDate){ if($rows['actual_amount']=='אכל פחות'){ echo "selected"; }}?>>אכל פחות</option>
			<option value="אכל יותר" <?php if($sDate ==$curDate){ if($rows['actual_amount']=='אכל יותר'){ echo "selected"; }}?>>אכל יותר</option>

		  </select>
		</td>
      	<td><textarea  disabled class="form-control" rows="1" id="comment_<?php echo $rows['food_id']; ?>"><?php if(!empty($rows['comment'])){ echo $rows['comment']; }?></textarea></td>
			<td><textarea class="form-control" rows="1" id="comment_w_<?php echo $rows['food_id']; ?>"></textarea></td>
        </tr>

	<?php }  } ?>
    <?php
	$cnt = sizeof($commaString);
	if($cnt >0){ ?>
	<tr><td colspan="12" ><button class="btn savelss fa fa-floppy-o" onclick="return backfoodss();" style="width:30%"> שמור </button></td></tr>
<?php }else{echo '<tr><td colspan="8" st>לא נמצא מזון להאכלה</td></tr>';} ?>
    </tbody>
  </table>
  <?php $strArr = implode(",", $commaString);?>
  <input type="hidden" id="hid" value="<?php echo $strArr; ?>" />
  </form>
 <?php if($cnt >0){ ?>
  <?php echo date('D M j G:i:s T Y', strtotime($last_update)); ?>         Last Update
  <?php } ?>



</div>
<script>
function backfoodss(){
    str = $("#hid").val();
	var myarray = str.split(',');

for(var i = 0; i < myarray.length; i++)
{
	food_id = myarray[i];
    if($("#actualamount_"+food_id).val()==''){
		alert("Please enter actual amount.");
		$("#actualamount_"+food_id).focus();
		return false;
		}
	comment = $("#comment_"+food_id).val();
   actual_amount = $("#actualamount_"+food_id).val();
	 comment_w = $("#comment_w_"+food_id).val();
   //actual_unit   = $("#actualunit_"+food_id).val();
   $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/saveactualamount']); ?>",
         data: { food_id:food_id,  actual_amount:actual_amount, comment:comment, comment_w:comment_w}
        })
       .done(function( msg ) {

       });
}
 alert("ההאכלה נשמרה בהצלחה!");
		 setTimeout(function() {
        $("#food").trigger('click');
    },1);
	return false;
}


</script>
