<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
?>
<h1>מנהל מערכת</h1>

            <ul class="submenu" style="list-style-type: none !important">
              <li class="space"><a href="<?= Url::to(['foods/index' ])?>"><?php echo Yii::t('app', 'ניהול מזון' ); ?></a></li>
              <li class="space"><a href="<?= Url::to(['foodtype/index' ])?>"><?php echo Yii::t('app', 'הוספת סוגי מזון' ); ?></a></li>
              <li class="space"><a href="<?= Url::to(['diagnostic/index' ])?>"><?php echo Yii::t('app', 'הוספת סוגי אבחון' ); ?></a></li>
              <li class="space"><a href="<?= Url::to(['dead/index' ])?>"><?php echo Yii::t('app', 'הוספת סוגי מוות' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['lsssystem/index' ])?>"><?php echo Yii::t('app', 'מכונות LSS' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['lss/index' ])?>"><?php echo Yii::t('app', 'קישור LSS למיכל' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['user/index' ])?>"><?php echo Yii::t('app', 'הרשאות משתמשים' ); ?></a></li>
							<li class="space"><a href="<?= Url::to(['employee/index' ])?>"><?php echo Yii::t('app', 'הרשאות עובדים' ); ?></a></li>
            </ul>


<style>
.space{
padding: 10px !important;
list-style-type: none !important;
}

<style>
