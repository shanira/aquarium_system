<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\web\Session;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$this->title = 'Varify your identity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="row">
   <div class="col-md-6 col-md-offset-3 centered">
 <div class="site-login">
 <div class="col-md-2 bluecolor">

			<?php if(!empty(Yii::$app->user->identity->username)){ ?>
			<a href="<?= Url::to(['site/logout'])?>" class="dropdown-item notify-item fa fa-sign-out" data-method="get"><span><?php echo Yii::t('app', ' התנתק '); ?></span></a>
			<?php } ?>
			</div>
    <p style="text-align:center;padding-top:8%;"><img src="<?php echo Yii::$app->request->baseUrl;?>/web/uploads/u5.png"></p>
	<h2 class="login">יש להזין שם עובד</h2>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-error alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-check"></i>Error!</h4>
    <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-md-12\">{input}</div>\n<div class=\"col-md-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-md-1 control-label'],
        ],
    ]); ?>
	<div class="col-md-12">
        <div class="col-md-2"></div>
		<div class="col-md-8">
        <?= $form->field($model, 'code')->textInput(['autofocus' => true]) ?>
		</div>
        <div class="col-md-2"></div>
		</div>

		<div class="col-md-12">
           <div class="col-md-2"></div>
		<div class="col-md-8">
                <?= Html::submitButton('התחבר', ['class' => 'btn bbtn', 'name' => 'login-button']) ?>
            </div>
<div class="col-md-2"></div>
</div>

    <?php ActiveForm::end(); ?>


</div>
    </div>



</div>

</div>
