<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="row">
   <div class="col-md-6 col-md-offset-3 centered">
 <div class="site-login">
    <p style="text-align:center;padding-top:8%;"><img src="<?php echo Yii::$app->request->baseUrl;?>/web/uploads/u5.png"></p>
    <h1 class="wel">ברוך הבא לאקווריום ישראל</h1>
	<h2 class="login">לקבלת גישה יש להתחבר</h2>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-md-12\">{input}</div>\n<div class=\"col-md-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-md-1 control-label'],
        ],
    ]); ?>
	<div class="col-md-12">
        <div class="col-md-2"></div>
		<div class="col-md-8">
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password')->input('password') ?>
		</div>
        <div class="col-md-2"></div>
		</div>

		<div class="col-md-12">
           <div class="col-md-2"></div>
		<div class="col-md-8">
                <?= Html::submitButton('התחבר', ['class' => 'btn bbtn', 'name' => 'login-button']) ?>
            </div>
<div class="col-md-2"></div>
</div>

    <?php ActiveForm::end(); ?>


</div>
    </div>



</div>

</div>
