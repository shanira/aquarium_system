<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Containers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="containers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->hiddenInput(['value' => date('Y-m-d')])->label(false); ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'שמור'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
