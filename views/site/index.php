<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContainersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'מיכלים');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="containers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
 <?php
	if(Yii::$app->user->identity->status=='Admin'){ ?>
    <p>
        <?= Html::a(Yii::t('app', 'הוספת מיכל'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
  <?php } ?>
  <?php
 if(Yii::$app->user->identity->status=='Admin'){ ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
			'attribute' => 'container_name',
			'format' => 'raw',
			'label' => 'מיכלים',
			'value' => function ($model) {
                    $url = Url::to(['containers/show', 'id' => trim($model->cid)]);
					return '<a href="'.$url.'" target="_blank">'.$model->container_name.'</a>';
			},
           ],


            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
    <?php }else{ ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
			'attribute' => 'container_name',
			'format' => 'raw',
			'label' => 'מיכלים',
			'value' => function ($model) {
                    $url = Url::to(['containers/show', 'id' => trim($model->cid)]);
					return '<a href="'.$url.'" target="_blank">'.$model->container_name.'</a>';
			},
           ],

        ],
    ]); ?>
    <?php } ?>
    <?php Pjax::end(); ?>
</div>
<style>
.grid-view th{ text-align:center; }
</style>
