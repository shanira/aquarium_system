<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Food */

$this->title = Yii::t('app', 'Create Vet');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vet'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();
$data = $model->getFishDataDrop($post['cid']);
?>
<div class="row">
	<div class="col-sm-12">
	     <h1>טיפול חדש</h1>
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn fa fa-chevron-right" href="#tab=w0-tab2" onclick="return backfood();">חזור</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">

     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">טיפול</label>
			<select class="form-control" id="treatment" name="treatment" style="height: 36px;">
			<option value="" selected disabled>בחר טיפול</option>
			<option value="Dentisept">Dentisept</option>
			<option value="Diazavet">Diazavet</option>
			<option value="Enfrofloxacin">Enfrofloxacin</option>
			<option value="Erythromycin">Erythromycin</option>
			<option value="Fenbendazole">Fenbendazole</option>
			<option value="Formalin">Formalin</option>
			<option value="Fortum">Fortum</option>
			<option value="Metronidazole">Metronidazole</option>
			<option value="Methylene Blue">Methylene Blue</option>
			<option value="Praziquantel">Praziquantel</option>
			<option value="Other">Other</option>
		  </select>

		<div class="help-block"></div>
	</div>

     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">מינון</label>
			<select class="form-control" id="specific_treatment" name="specific_treatment" style="height: 36px;">
			<option value="" selected disabled>בחר מינון</option>
			<option value="5Mg/Kg">5Mg/Kg</option>
			<option value="10Mg/Kg">10Mg/Kg</option>
			<option value="20Mg/Kg">20Mg/Kg</option>
			<option value="30Mg/Kg">30Mg/Kg</option>
			<option value="50Mg/Kg">50mg/Kg</option>
			<option value="250mg/100gr food">250Mg/100gr food</option>
			<option value="Other">Other</option>
		  </select>

		<div class="help-block"></div>
	</div>

	     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">תדירות</label>
		<select class="form-control" id="frequency" name="frequency" style="height: 36px;">
			<option value="" selected disabled>בחר תדירות</option>
			<option value="SID">SID</option>
			<option value="BID">BID</option>
			<option value="q48hrs">q48hrs</option>
			<option value="q72hrs">q72hrs</option>
			<option value="Other">Other</option>
		  </select>
		<div class="help-block"></div>
	</div>

	     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">דילול</label>
		<select class="form-control" id="dilution" name="dilution" style="height: 36px;">
			<option value="" selected disabled>בחר דילול</option>
			<option value="1:05">1:05</option>
			<option value="1:10">1:10</option>
			<option value="Other">Other</option>
		  </select>
		<div class="help-block"></div>
	</div>

		     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">צורת נתינה</label>
		<select class="form-control" id="how_given" name="how_given" style="height: 36px;">
			<option value="" selected disabled>בחר צורת נתינה</option>
			<option value="PO in food">PO in food</option>
			<option value="IM">IM</option>
			<option value="SQ">SQ</option>
			<option value="Bath">Bath</option>
			<option value="Other">Other</option>
		  </select>
		<div class="help-block"></div>
	</div>

    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea cols="5" rows="3" name="comments" class="form-control" id="comments"></textarea>
		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">עבור</label>
		<select class="form-control" id="treatment_for" name="treatment_for" style="height: 36px;">
		  <?php foreach($data as $fishDrop){ ?>
			<option value="<?php echo $fishDrop['fid']; ?>"><?php echo $fishDrop['name']; ?></option>
		  <?php } ?>
			<option value="0">All Container</option>
		  </select>

    </div>

		     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">האם להדגיש?</label>
		<select class="form-control" id="high" name="how_given" style="height: 36px;">
			<option value="" selected disabled>בחר האם להדגיש</option>
			<option value="1">כן</option>
			<option value="0">לא</option>
		  </select>
		<div class="help-block"></div>
	</div>


    <div class="form-group field-fish-created_date required">
		<input id="created_date" class="form-control" name="created_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
	</div>

    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return savefood();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>
	function backfood(){
        $("#vet").trigger('click');
	}
	function savefood(){
		if($("#treatment").val() ==''){
			alert("Please Enter Treatment.");
			$("#treatment").focus();
			return false;
		}

		if($("#specific_treatment").val() ==''){
			alert("Please Enter Dose.");
			$("#specific_treatment").focus();
			return false;
		}

		if($("#frequency").val() ==''){
			alert("Please Enter frequency.");
			$("#frequency").focus();
			return false;
		}
				if($("#dilution").val() ==''){
			alert("Please Enter dilution.");
			$("#dilution").focus();
			return false;
		}
				if($("#how_given").val() ==''){
			alert("Please Enter how given.");
			$("#how_given").focus();
			return false;
		}
		if($("#treatment_for").val() ==''){
			alert("Please Select For.");
			$("#treatment_for").focus();
			return false;
		}

		comments           = $("#comments").val();
		treatment_for      = $("#treatment_for").val();
		treatment          = $("#treatment").val();
		specific_treatment = $("#specific_treatment").val();
		frequency = $("#frequency").val();
		dilution = $("#dilution").val();
		how_given = $("#how_given").val();
		high = $("#high").val();
		created_date       = $("#created_date").val();
		updated_date       = $("#updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vet/savecomment']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", treatment:treatment, specific_treatment:specific_treatment, frequency:frequency,dilution:dilution,how_given:how_given, high:high, comments: comments, treatment_for:treatment_for, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("נשמר בהצלחה!");
         setTimeout(function() {
        $("#vet").trigger('click');
    },1);
       });
	}
</script>
