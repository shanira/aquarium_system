<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'container_id')->textInput() ?>

    <?= $form->field($model, 'treatment_for')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
