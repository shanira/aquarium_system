<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Vet;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Vets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-12">
		<div class="btn-group pull-right m-t-15">
		<a class="btn topbtn" href="#tab=w2-tab0" onclick="return addcomment();">הוספת טיפול</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
		'rowOptions' => function($model){
						if($model->high == '1')
						{
							return['class'=>'danger'];

						}

						},
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

						[
					 'label' => 'תאריך',
					 'format' => 'raw',
						'value' => function($model){
							$newdate = date('d-m-Y', strtotime($model->updated_date));
											return $newdate;
									},
								 ],
			[
			'label' => 'טיפול',
			'format' => 'raw',
		   'value' => function($model){
			    return $model->treatment;
            },
           ],
		   [
			'label' => 'מינון',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->specific_treatment;
            },
           ],

		   [
			'label' => 'תדירות',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->frequency;
            },
           ],
			[
			'label' => 'דילול',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->dilution;
            },
           ],
		    [
			'label' => 'צורת נתינה',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->how_given;
            },
           ],

		   [
			'label' => 'הערה',
			'format' => 'raw',
		   'value' => function($model){
			   return $model->comments;
            },
           ],

			[
			'label' => 'עבור',
			'format' => 'raw',
		   'value' => function($model){
                return Vet::getFish($model->treatment_for);
            },
           ],


           // ['class' => 'yii\grid\ActionColumn'],
		   ['class' => 'yii\grid\ActionColumn',
	'buttons' => [

			'update' => function ($url, $model, $key) {
				   return '<a href="JavaScript:void(0);" onclick="commentsupdate('.$model->vet_id.');" ><span class="glyphicon glyphicon-pencil"></span></a>';
                },

			'delete' => function ($url, $model, $key)
				{
					return '<a href="JavaScript:void(0);" onclick="deletecomment('.$model->vet_id.');" ><span class="glyphicon glyphicon-trash"></span></a>';

                },

            ],
            'template' => '{update} {delete}'


        ],
        ],

    ]); ?>
    </div>
</div>
