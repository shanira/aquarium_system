<?php

use yii\helpers\Html;
use  yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $model app\models\Vet */

$this->title = Yii::t('app', 'Update Vet: ' . $model->vet_id, [
    'nameAttribute' => '' . $model->vet_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vet_id, 'url' => ['view', 'id' => $model->vet_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$post = Yii::$app->request->post();
$data = $model->getFishDataDrop($post['cid']);
?>
<div class="row">
	<div class="col-sm-12">
	     <h1>עדכון טיפול</h1>
		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn" href="#tab=w0-tab2" onclick="return backfood();">חזור</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-4"></div>
<div class="col-sm-4">
<form id="w0" action="" method="post">
    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">טיפול</label>
    <select class="form-control" id="treatment" name="treatment" style="height: 36px;">
      <option value="" <?php if($model->treatment==''){ echo "selected disabled"; }?>>בחר טיפול</option>
      <option value="Dentisept" <?php if($model->treatment=='Dentisept'){ echo "selected"; }?>>Dentisept</option>
      <option value="Diazavet" <?php if($model->treatment=='Diazavet'){ echo "selected"; }?>>Diazavet</option>
      <option value="Enfrofloxacin" <?php if($model->treatment=='Enfrofloxacin'){ echo "selected"; }?>>Enfrofloxacin</option>
      <option value="Erythromycin" <?php if($model->treatment=='Erythromycin'){ echo "selected"; }?>>Erythromycin</option>
      <option value="Fenbendazole" <?php if($model->treatment=='Fenbendazole'){ echo "selected"; }?>>Fenbendazole</option>
      <option value="Formalin" <?php if($model->treatment=='Formalin'){ echo "selected"; }?>>Formalin</option>
      <option value="Fortum" <?php if($model->treatment=='Fortum'){ echo "selected"; }?>>Fortum</option>
      <option value="Metronidazole" <?php if($model->treatment=='Metronidazole'){ echo "selected"; }?>>Metronidazole</option>
      <option value="Methylene Blue" <?php if($model->treatment=='Methylene Blue'){ echo "selected"; }?>>Methylene Blue</option>
      <option value="Praziquantel" <?php if($model->treatment=='Praziquantel'){ echo "selected"; }?>>Praziquantel</option>
      <option value="Other" <?php if($model->treatment=='Other'){ echo "selected"; }?>>Other</option>
      </select>
		<div class="help-block"></div>
	</div>


     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">מינון</label>
    <select class="form-control" id="specific_treatment" name="specific_treatment" style="height: 36px;">
      <option value="" <?php if($model->specific_treatment==''){ echo "selected disabled"; }?>>בחר מינון</option>
      <option value="5Mg/Kg" <?php if($model->specific_treatment=='5Mg/Kg'){ echo "selected"; }?>>5Mg/Kg</option>
      <option value="10Mg/Kg" <?php if($model->specific_treatment=='10Mg/Kg'){ echo "selected"; }?>>10Mg/Kg</option>
      <option value="20Mg/Kg" <?php if($model->specific_treatment=='20Mg/Kg'){ echo "selected"; }?>>20Mg/Kg</option>
      <option value="30Mg/Kg" <?php if($model->specific_treatment=='30Mg/Kg'){ echo "selected"; }?>>30Mg/Kg</option>
      <option value="50Mg/Kg" <?php if($model->specific_treatment=='50Mg/Kg'){ echo "selected"; }?>>50Mg/Kg</option>
      <option value="250mg/100gr food" <?php if($model->specific_treatment=='250mg/100gr food'){ echo "selected"; }?>>250mg/100gr food</option>
      <option value="Other" <?php if($model->specific_treatment=='Other'){ echo "selected"; }?>>Other</option>
    </select>
		<div class="help-block"></div>
	</div>


	     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">תדירות</label>
    <select class="form-control" id="frequency" name="frequency" style="height: 36px;">
      <option value="" <?php if($model->frequency==''){ echo "selected disabled"; }?>>בחר תדירות</option>
      <option value="SID" <?php if($model->frequency=='SID'){ echo "selected"; }?>>SID</option>
      <option value="BID" <?php if($model->frequency=='BID'){ echo "selected"; }?>>BID</option>
      <option value="q48hrs" <?php if($model->frequency=='q48hrs'){ echo "selected"; }?>>q48hrs</option>
      <option value="q72hrs" <?php if($model->frequency=='q72hrs'){ echo "selected"; }?>>q72hrs</option>
      <option value="Other" <?php if($model->frequency=='Other'){ echo "selected"; }?>>Other</option>
   </select>
		<div class="help-block"></div>
	</div>

		     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">דילול</label>
    <select class="form-control" id="dilution" name="dilution" style="height: 36px;">
      <option value="" <?php if($model->dilution==''){ echo "selected disabled"; }?>>בחר דילול</option>
      <option value="1:05" <?php if($model->dilution=='1:05'){ echo "selected"; }?>>1:05</option>
      <option value="1:10" <?php if($model->dilution=='1:10'){ echo "selected"; }?>>1:10</option>
      <option value="Other" <?php if($model->dilution=='Other'){ echo "selected"; }?>>Other</option>
    </select>
		<div class="help-block"></div>
	</div>

		     <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">צורת נתינה</label>
    <select class="form-control" id="how_given" name="how_given" style="height: 36px;">
      <option value="" <?php if($model->how_given==''){ echo "selected disabled"; }?>>בחר צורת נתינה</option>
      <option value="PO in food" <?php if($model->how_given=='PO in food'){ echo "selected"; }?>>PO in food</option>
      <option value="IM" <?php if($model->how_given=='IM'){ echo "selected"; }?>>IM</option>
      <option value="SQ" <?php if($model->how_given=='SQ'){ echo "selected"; }?>>SQ</option>
      <option value="Bath" <?php if($model->how_given=='Bath'){ echo "selected"; }?>>Bath</option>
      <option value="Other" <?php if($model->how_given=='Other'){ echo "selected"; }?>>Other</option>
    </select>
		<div class="help-block"></div>





    <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">הערה</label>
		<textarea cols="5" rows="3" name="comments" class="form-control" id="comments"><?php echo $model->comments; ?></textarea>
		<div class="help-block"></div>
	</div>
    <div class="form-group field-fish-s_name required">
		<label class="control-label" for="fish-s_name">עבור</label>
		<select class="form-control" id="treatment_for" name="treatment_for" style="height: 36px;">
		  <?php foreach($data as $fishDrop){ ?>
			<option value="<?php echo $fishDrop['fid']; ?>"  <?=$model->treatment_for == $fishDrop['fid'] ? ' selected="selected"' : '';?>><?php echo $fishDrop['name']; ?></option>
		  <?php } ?>
			<option value="0" <?=$model->treatment_for == 0 ? ' selected="selected"' : '';?>>All Container</option>
		  </select>

    </div>

	      <div class="form-group field-fish-name required">
		<label class="control-label" for="fish-name">האם להדגיש?</label>
    <select class="form-control" id="high" name="high" style="height: 36px;">
      <option value="" <?php if($model->high==''){ echo "selected disabled"; }?>>בחר האם להדגיש</option>
      <option value="1" <?php if($model->high=='1'){ echo "selected"; }?>>כן</option>
      <option value="0" <?php if($model->high=='0'){ echo "selected"; }?>>לא</option>

    </select>
		<div class="help-block"></div>



    <div class="form-group field-fish-created_date required">
	<input id="vet_id" class="form-control" name="vet_id" value="<?php echo $model->vet_id; ?>" type="hidden">
		<input id="created_date" class="form-control" name="created_date" value="<?php echo $model->date_created; ?>" type="hidden">
		<input id="updated_date" class="form-control" name="updated_date" value="<?php echo date ("Y-m-d H:i:s"); ?>" type="hidden">
	</div>

    <div class="form-group"><button type="button" class="btn topbtn fa fa-floppy-o" onclick="return updatevet();"> שמור </button></div>

    </form>
</div>
<div class="col-sm-4"></div>

</div>
</div>
</div>
<script>
	function backfood(){
        $("#vet").trigger('click');
	}

</script>
