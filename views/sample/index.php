<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;

use app\models\Lss;
use app\models\Food;
use app\models\Sample;

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use  yii\web\Session;
$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SampleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'דגימות מים');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sample-index" >

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוסף דגימה חדשה'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'sample_id',
            [
      'label' => 'מיכל',
      'attribute' => 'container_id',
      'format' => 'raw',
      'filter' => Food::containerdropdown(),
      'value' => function($model, $index, $dataColumn) {
                // more optimized than $model->role->name;
                $containerDropdowns = Food::containerdropdown();
         if($model->container_id){
                return $containerDropdowns[$model->container_id];
         }else{
            return '';
          }
            },

           ],
		   [
			'attribute' => 'sample_name',
			'label' => 'שם דגימה',
			'format' => 'raw',
			'filter' => Sample::sampledropdown(),

           ],
            //'sample_name',
            'sample_value',
            'comment',
            'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<script type="text/javascript">
$(document).ready(function() {
               $.noConflict();
                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
					"pageLength": 50,
					"bInfo": false,
					"bPaginate": true,
					"bFilter": false,
                    buttons: ['csv', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-12:eq(0)');
			});

   </script>
