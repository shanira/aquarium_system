<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Lss;
use  yii\web\Session;
use app\models\FoodType;

$session = Yii::$app->session;
$Lssmodel = new Lss();
$data              = $Lssmodel->dropContainer();
/* @var $this yii\web\View */
/* @var $model app\models\Sample */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sample-form">

    <?php $form = ActiveForm::begin(); ?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-check"></i>Saved!</h4>
    <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

    <?= $form->field($model, 'container_id')->dropdownList($data,
           ['prompt'=>'בחר מיכל'
      ]);  ?>


    <?= $form->field($model, 'sample_name')->dropdownList([
      'Alkalinity'=>'Alkalinity',
      'Bromines'=>'Bromines',
      'Ca'=>'Ca',
      'Copper'=>'Copper',
      'CO2'=>'CO2',
      'DO %'=>'DO %',
      'DO mg/l'=>'DO mg/l',
      'Hardness'=>'Hardness',
      'Mg'=>'Mg',
      'NO2'=>'NO2',
      'NO3'=>'NO3',
      'PO4'=>'PO4',
      'pH'=>'pH',
      'Salinity'=>'Salinity',
      'Strontium'=>'Strontium',
      'TAN'=>'TAN',
      'UIA'=>'UIA'],
 					['prompt'=>'בחר דגימה']
 				); ?>

    <?= $form->field($model, 'sample_value')->textInput([

                            ]) ?>


    <?= $form->field($model, 'comment')->textInput(['rows' => 5]) ?>

    <?= $form->field($model, 'updated_date')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', ' שמור '), ['class' => 'btn topbtn fa fa-floppy-o']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
