<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sample */

$this->title = Yii::t('app', 'דגימה חדשה');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Samples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
 <div class="col-sm-12">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
    <div class="col-sm-3"></div>
 </div>
</div>
