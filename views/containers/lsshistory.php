<?php

use yii\helpers\Html;
use app\models\Lss;
$model = new Lss();
/* @var $this yii\web\View */
/* @var $model app\models\Containers */


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'History');
?>
<div class="row">
	<div class="col-sm-12">

		<div class="btn-group pull-right m-t-15">
         <a class="btn topbtn fa fa-chevron-right" onclick="return backtoless();">חזור</a>
</div>

</div>
</div>
<div class="containers-update">
<?php
$post = Yii::$app->request->post();
 $fid = $post['field_id'];
?>
<div class="table-responsive ">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>תאריך</th>
        <th><?php echo $model->getLssName($fid); ?></th>
				<th>מצב</th>
		<th></th>
      </tr>
    </thead>
    <tbody>
	<?php foreach($result as $models){?>
      <tr>
				  <!--td><==?php echo $models['lss_id']; ?></td-->
        <td><?php echo $models['date_created']; ?></td>
        <td><?php echo $models['machine_value']; ?></td>
				<td><?php echo $models['status']; ?></td>
		<td>
			<?php
		 if(Yii::$app->user->identity->status=='Admin'){ ?>
				<!--a href="JavaScript:void(0);" onclick="updatelss(<--?php echo $models['lss_id']; ?>,'<--?php echo $fid; ?>');" ><span class="glyphicon glyphicon-pencil"></span></a-->
        <a href="JavaScript:void(0);" onclick="deletelss(<?php echo $models['lss_id']; ?>);" ><span class="glyphicon glyphicon-trash"></span></a>
	<?php } ?>
		</td>
	  </tr>
	<?php } ?>
    </tbody>
  </table>
  </div>
</div>
<script>
function backtoless(){
        $("#lss").trigger('click');
	}

</script>
