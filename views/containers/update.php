<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Containers */

$this->title = Yii::t('app', 'עדכון מיכל' . $model->cid, [
    'nameAttribute' => '' . $model->cid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cid, 'url' => ['view', 'id' => $model->cid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="containers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
