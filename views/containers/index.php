<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContainersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'מיכלים');
$this->params['breadcrumbs'][] = $this->title;
use  yii\web\Session;
$session = Yii::$app->session;

?>
<div class="containers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'הוספת מיכל'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cid',
			[
			'attribute' => 'container_name',
			'label' => 'Containers',
			'format' => 'raw',
			'value' => function ($model) {
					     $url = Url::to(['containers/show', 'id' => $model->cid]);
					return '<a href="'.$url.'">'.$model->container_name.'</a>';

			},
           ],
            'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
