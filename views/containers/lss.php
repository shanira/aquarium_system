<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\helpers\Url;
$session = Yii::$app->session;
use app\models\Lss;
$model = new Lss();
$currentdata              = $model->getCurrentDate($session->get('cid'));
$last_updated_date = $model->getLastUpdatedData($session->get('cid'));
$lssData           = $model->getContainerLssMachine($session->get('cid'));
/* @var $this yii\web\View */
/* @var $model app\models\Lss */
/* @var $form ActiveForm */

?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2 pull-left">
		 <button type="button" class="btn topbtn" id="lastcheckmode" onclick="return lastcheckmode();">מצב <br> Last Check </button>
		</div>
		<div class="btn-group pull-right">
         תאריך: <?php echo date("d-m-Y") ?>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<div class="col-sm-2"></div>
<div class="col-sm-8">
<form class="form-horizontal">
   <?php
   foreach($lssData as $machine){
	   $stat = $model->getstatus($machine['lss_attached_id']);
   ?>
	<div class="form-group">
      <label class="control-label col-sm-3" for="<?php echo $machine['lss_machine_id']; ?>"><?php echo $machine['lss_machine_name']; ?>:</label>
      <div class="col-sm-5">
        <?php if($machine['input_type']=='on'){ ?>
        <script>
		$(document).ready(function(){
$("#gender_radio_<?php echo $machine['lss_attached_id']; ?> a").on('click', function(){
var selected = $(this).data('title');
var toggle = $(this).data('toggle');
$('#'+toggle).prop('value', selected);
$('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
$('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
})
});
		</script>
        <div id="gender_radio_<?php echo $machine['lss_attached_id']; ?>" class="btn-group">
<a class="btn btn-primary right-corner3 btn-sm  <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='on' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="on">ON</a>
<a class="btn btn-primary middle3 btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='off' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="off">OFF</a>
<a class="btn btn-primary left-corner3 fa fa-flag btnred btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='X' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="X"></a>

</div>
<input type="hidden" name="<?php echo $machine['lss_attached_id']; ?>"  id="<?php echo $machine['lss_attached_id']; ?>"  value="<?php echo $model->getLesstodayData($session->get('cid'), $machine['lss_attached_id']); ?>"  name="<?php echo $machine['lss_attached_id']; ?>">
<?php }elseif($machine['input_type']=='enter') { ?>

							<div class=""></div>
												<script>
																$(document).ready(function(){
																	$("#gender_radio_<?php echo $machine['lss_attached_id']; ?> a").on('click', function(){
																	var selected = $(this).data('title');
																	var toggle = $(this).data('toggle');
																	$('#'+toggle).prop('value', selected);
																	$('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
																	$('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
																	$( "#gender_radio_<?php echo $machine['lss_attached_id']; ?> input" ).removeClass('numcol')
																		})
																	$( "#gender_radio_<?php echo $machine['lss_attached_id']; ?> input" ).focus(function() {
																				console.log(1)
																					$(this).val('');
																					$(this).addClass('numcol')
																		$("#gender_radio_<?php echo $machine['lss_attached_id']; ?> a.btn").removeClass('active').addClass('noActive');
																							})

																	});
											</script>

					<div id="gender_radio_<?php echo $machine['lss_attached_id']; ?>" class="btn-group">
						<a class="btn btn-primary right-corner btn-sm  <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='on' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="on">ON</a>
						<a class="btn btn-primary middle btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='off' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="off">OFF</a>
						<a class="btn btn-primary middle fa fa-flag  btnred btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='X' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="X"></a>
						<input type="text" placeholder="num"  class="form-control input-left col-sm-3 " name="<?php echo $machine['lss_attached_id']; ?>"  id="<?php echo $machine['lss_attached_id']; ?>"  value="<?php echo $model->getLesstodayData($session->get('cid'), $machine['lss_attached_id']); ?>"  name="<?php echo $machine['lss_attached_id']; ?>">

					</div>


				<?php }elseif($machine['input_type']=='fish') { ?>
					<script>
									$(document).ready(function(){
										$("#gender_radio_<?php echo $machine['lss_attached_id']; ?> a").on('click', function(){
										var selected = $(this).data('title');
										var toggle = $(this).data('toggle');
										$('#'+toggle).prop('value', selected);
										$('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
										$('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
											})
										});
				</script>

					<div id="gender_radio_<?php echo $machine['lss_attached_id']; ?>" class="btn-group">
							<a class="btn btn-primary right-corner3 btn-sm  <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='Good' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="Good">Good</a>
							<a class="btn btn-primary middle3 btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='No-Fish' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="No-Fish">No-Fish</a>
							<a class="btn btnred left-corner3 btn-primary fa fa-flag btnred btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='X' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="X"></a>
					</div>
					<input type="hidden" name="<?php echo $machine['lss_attached_id']; ?>"  id="<?php echo $machine['lss_attached_id']; ?>"  value="<?php echo $model->getLesstodayData($session->get('cid'), $machine['lss_attached_id']); ?>"  name="<?php echo $machine['lss_attached_id']; ?>">

				<?php }elseif($machine['input_type']=='level') { ?>
					<script>
									$(document).ready(function(){
										$("#gender_radio_<?php echo $machine['lss_attached_id']; ?> a").on('click', function(){
										var selected = $(this).data('title');
										var toggle = $(this).data('toggle');
										$('#'+toggle).prop('value', selected);
										$('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
										$('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
											})
										});
				</script>
					<div id="gender_radio_<?php echo $machine['lss_attached_id']; ?>" class="btn-group">
						<a class="btn btn-primary right-corner3 btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='Normal' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="Normal">Normal</a>
							<a class="btn btn-primary middle3 btn-sm  <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='Low' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="Low">Low</a>
							<a class="btn btn-primary left-corner3  btn-sm <?php echo ($model->getLesstodayData($session->get('cid'), $machine['lss_attached_id'])=='High' ? 'active' : 'noActive');?>" data-toggle="<?php echo $machine['lss_attached_id']; ?>" data-title="High">High</a>
					</div>
					<input type="hidden" name="<?php echo $machine['lss_attached_id']; ?>"  id="<?php echo $machine['lss_attached_id']; ?>"  value="<?php echo $model->getLesstodayData($session->get('cid'), $machine['lss_attached_id']); ?>"  name="<?php echo $machine['lss_attached_id']; ?>">

				<?php }else{ ?>


			<?php } ?>
				</div>
	<div class="col-sm-2">
	  <button type="button" class="histoy" onclick="return lsshistory('<?php echo $machine['lss_attached_id']; ?>');">היסטוריה</button>
	  </div>
      <div class="col-sm-2 andy noshow">
       <!--input type="checkbox" class="form-control" <--?php if($stat=='ok'){ echo "checked"; } ?> id="ok_<--?php echo $machine['lss_attached_id']; ?>"  onclick="return saveok('<--?php echo $machine['lss_attached_id']; ?>');" -->
      </div>
    </div>

    <?php
       }
    ?>

<?php
   foreach($lssData as $machine){
	$lss_ids[] = '#'.$machine['lss_attached_id'];
	 }
	 $lss_id = implode(',', $lss_ids);
	 ?>

	<input type="hidden" id="cArr" value="<?php echo $lss_id; ?>" />
	<br>
<div class="col-sm-3"></div>
		 <div class="col-sm-6">
	<button id="saver" type="button" class="btn savelss fa fa-floppy-o" onclick="return savelss();"> שמור </button>
<button id="savel" type="button" class="btn savelss" style="display: none;" onclick="return savelsslast();">שמור</button>
</div>
	</form>




</div>
<div class="col-sm-4"></div>
</div>


<div class="row">
	<div class="col-sm-12">
			עודכן לאחרונה ב: <?php  echo date('d-m-Y H:i:s',strtotime($last_updated_date)); ?>

</div>
</div>
<script>
function lastcheckmode() {

	//alert("get")

	if($("#lastcheckmode").css('background-color')=='rgb(0, 51, 102)'){
      $("#lastcheckmode").css('background-color', '#ff3333');
		  $("#lastcheckmode").css('border-color', '#ff3333');
		  //$( "#preshow" ).css( "background-color", "#003366" );
        //  $( "#preshow" ).css( "border-color", "#003366" );
          //    $( "#logs" ).hide();
          //    $( "#vet" ).hide();
			 // $( "#vetd" ).hide();
            //  $( "#water").hide();
            //  $( "#food" ).hide();
              $( "#w0-tab1 div" ).removeClass( "noshow" );
							$( "#saver" ).hide();
							$( "#savel" ).show();
	}else {
         $("#lastcheckmode").css('background-color', '#003366');
				 $("#lastcheckmode").css('border-color', '#003366');
		      //$( "#logs" ).show();
          //    $( "#vet" ).show();
			  	//			$( "#vetd" ).show();
            //  $( "#water").show();
            //  $( "#food" ).show();
			  $( ".andy" ).addClass( "noshow" );
				$( "#saver" ).show();
				 $( "#savel" ).hide();
   }
}


function savelss(){
      var larr = $("#cArr").val();
	  //alert(larr);
	 var datalist = $(larr).serialize();
	// alert(datalist);
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['containers/savelss']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", datalist }
        })
       .done(function( msg ) {
		   alert("נשמר בהצלחה!");
         setTimeout(function() {
        $("#lss").trigger('click');
    },1);
       });
}

function savelsslast(){
      var larr = $("#cArr").val();
	  //alert(larr);
	 var datalist = $(larr).serialize();
	// alert(datalist);
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['containers/savelsslast']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", datalist }
        })
       .done(function( msg ) {
		   alert("נשמר בהצלחה!");
         setTimeout(function() {
        $("#lss").trigger('click');
    },1);
       });
}

function lsshistory(field_id){
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['containers/lsshistory']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", field_id: field_id }
        })
       .done(function( msg ) {
		   $("#w0-tab1").empty().html(msg);

       });
}

function saveok(id){
	if($("#ok_"+id).prop('checked')==true){
       $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['containers/savestatus']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", lss_machine: id, "flag" : "ok" }
        })
       .done(function( msg ) {
		   //$("#w0-tab1").empty().html(msg);

       });
    }else{
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['containers/savestatus']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", lss_machine: id, "flag" : "notok" }
        })
       .done(function( msg ) {
		   //$("#w0-tab1").empty().html(msg);

       });
		}

	}
</script>
<style>
.noActive{
color: #3276b1;
background-color: #fff;

}

.btnred{

color: #FE2E2E !important;

}
.input-left
{
  	-moz-border-radius: 15px 0px 0px 15px;
 		border-radius: 15px 0px 0px 15px !important;
    border:solid 1px #039cfd !important;
    padding:1.8rem .75rem !important ;
		font-size: 2rem !important ;
		width: 30% !important;
}

.right-corner{
	border-radius: 0px 15px 15px 0px !important;
	width: 70% !important;
}

.left-corner{
	border-radius: 15px 0px 0px 15px !important;
	width: 70% !important;

}
	.middle{

	width: 70% !important;
	}


	.right-corner3{
		border-radius: 0px 15px 15px 0px !important;
		width: 109px !important;
	}

	.left-corner3{
		border-radius: 15px 0px 0px 15px !important;
		width: 109px !important;
	}
		.middle3{

		width: 109px !important;
		}

.control-label{
    text-align: right !important;

}
.btn-primary.active{
	border:solid 1px #039cfd !important;
	color: #fff !important;
	background-color: #039cfd !important;
	}

	.btn-primary:hover{
		border:solid 1px #039cfd !important;
		color: #fff !important;
		background-color: #039cfd !important;
		}
.btn-group-sm > .btn, .btn-sm {
   font-size: 2rem;
line-height: 3rem;
}

.form-horizontal .form-group{
padding: 5px;

}
.numcol {
  background-color : #039cfd;
	color=#fff;
}



</style>
