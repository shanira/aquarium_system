<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use yii\helpers\Url;
use  yii\web\Session;
$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $model app\models\Containers */

$this->title = $model->cid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$session = Yii::$app->session;
?>
<div class="containers-view">
<br>
    <h1><?= Html::encode($model->container_name) ?></h1>
<br>
   <?php  $cid = Yii::$app->request->getQueryParam('id');
         $session->set('cid', $cid);
   ?>

<?php

    $items = [
        [
            'label'=>'דגים',
            'content'=> '',
			'encode'=>false,
			'linkOptions'=>['id'=>'preview','data-url'=>\yii\helpers\Url::to(['/fish/tabsdata?tab=w0-tab0'])],
            'active'=>true
        ],
        [
            'label'=>'LSS',
            'content'=> '',
			'encode'=>false,
			'linkOptions'=>['id'=>'lss','data-url'=>\yii\helpers\Url::to(['/containers/lss?tab=w0-tab1'])],
			'pluginOptions' => ['enableCache'=>FALSE],
        ],
		[
            'label'=>'מזון',
			'encode'=>false,
            'content'=> '',
			'linkOptions'=>['id'=>'food','data-url'=>\yii\helpers\Url::to(['/food/index?tab=w0-tab2'])],
			'pluginOptions' => ['enableCache'=>FALSE],

        ],
    		//[
          //      'label'=>'מים',
            //    'content'=>'',
    			//'linkOptions'=>['id'=>'water','data-url'=>\yii\helpers\Url::to(['/water/index?tab=w0-tab3'])],
        //    ],
        [
                'label'=>'טיפול',
                'content'=>'',
                'linkOptions'=>['id'=>'vet','data-url'=>\yii\helpers\Url::to(['/vet/index?tab=w0-tab3'])]
            ],
    		[
              'label'=>'אבחון',
            'content'=>'',
          'linkOptions'=>['id'=>'vetd','data-url'=>\yii\helpers\Url::to(['/vetd/index?tab=w0-tab4'])]
          ],
		[
            'label'=>'Logs',
            'content'=>'',
            'linkOptions'=>['id'=>'logs','data-url'=>\yii\helpers\Url::to(['/logs/index?tab=w0-tab5'])]
        ],

    ];
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false,
	'pluginOptions' => ['enableCache'=>FALSE],
]);
?>


</div>
<script>
$( "#preshow" ).click(function() {

	if($(this).css('background-color')=='rgb(0, 51, 102)'){
          $(this).css('background-color', '#ff3333');
		  $(this).css('border-color', '#ff3333');
		  $( "#lastcheck" ).css( "background-color", "#003366" );
          $( "#lastcheck" ).css( "border-color", "#003366" );
              $( "#logs" ).hide();
              $( "#vet" ).hide();
              $( "#vetd" ).hide();
            //  $( "#water").hide();
              $( "#food" ).hide();
              $( "div" ).removeClass( "noshow" );
	}else {
         $(this).css('background-color', '#003366');
		      $( "#logs" ).show();
              $( "#vet" ).show();
              $( "#vetd" ).show();
            //  $( "#water").show();
              $( "#food" ).show();
   }

});
$( "#lastcheck" ).click(function() {

	if($(this).css('background-color')=='rgb(0, 51, 102)'){
          $(this).css('background-color', '#ff3333');
		  $(this).css('border-color', '#ff3333');
		//  $( "#preshow" ).css( "background-color", "#003366" );
    //      $( "#preshow" ).css( "border-color", "#003366" );
    //          $( "#logs" ).hide();
    //          $( "#vet" ).hide();
		//	        $( "#vetd" ).hide();
    //          $( "#water").hide();
    //          $( "#food" ).hide();
              $( "#w0-tab1 div" ).removeClass( "noshow" );
	}else {
         $(this).css('background-color', '#003366');
         $(this).css('border-color', '#003366');
		     // $( "#logs" ).show();
          //    $( "#vet" ).show();
			 // $( "#vetd" ).show();
        //      $( "#water").show();
        //      $( "#food" ).show();
			  $( ".andy" ).addClass( "noshow" );
   }



});
function addcomment(){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vet/addcomment']); ?>",
         data: { cid: "<?php echo $model->cid; ?>" }
      })
       .done(function( msg ) {
       $("#w0-tab3").empty().html(msg);
    });
 }
 function addcommentd(){
 	 $.ajax({
        method: "POST",
           url: "<?php echo Url::to(['vetd/addcomment']); ?>",
          data: { cid: "<?php echo $model->cid; ?>" }
       })
        .done(function( msg ) {
        $("#w0-tab4").empty().html(msg);
     });
  }
function addfood(){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/tabscreate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>" }
      })
       .done(function( msg ) {
       $("#w0-tab2").empty().html(msg);
    });
 }
 function createfish(){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/tabscreate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>" }
      })
       .done(function( msg ) {
       $("#w0-tab0").empty().html(msg);
    });
 }
 function updatefish(fid){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/tabsupdate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid }
      })
       .done(function( msg ) {
       $("#w0-tab0").empty().html(msg);
    });
 }
 function deletefish(fid){
	 var x = confirm("האם אתה בטוח שברצונך למחוק?");
  if (x){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/deletefish']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
       //$("#w1-tab0").empty().html(msg);
    });
  }else{
	 return false;
	 }
 }


 function viewfishdetails(fid){
	 //alert(fid);
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['fish/viewfishdetails']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
       $("#w0-tab0").empty().html(msg);
    });
 }

$("document").ready(function() {
	if ($("#lastcheck").hasClass("orangeclass")) {
   $("#lss").trigger('click');
   $( "div" ).removeClass( "noshow" );
}
    setTimeout(function() {
        $("#preview").trigger('click');
    },2);
});
$("#lss").click(function(){

    $("#w0-tab0").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
	//$("#w0-tab6").removeClass("show in");
});
$("#food").click(function(){
    $("#w0-tab0").removeClass("show in");
	$("#w0-tab1").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
//	$("#w0-tab6").removeClass("show in");
});
$("#preview").click(function(){
    $("#w0-tab1").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
//	$("#w0-tab6").removeClass("show in");
});
$("#logs").click(function(){
    $("#w0-tab0").removeClass("show in");
	$("#w0-tab1").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
});
$("#vet").click(function(){
    $("#w0-tab0").removeClass("show in");
	$("#w0-tab1").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
//	$("#w0-tab6").removeClass("show in");
});
$("#water").click(function(){
    $("#w0-tab0").removeClass("show in");
	$("#w0-tab1").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab5").removeClass("show in");
	$("#w0-tab6").removeClass("show in");
});
$("#vetd").click(function(){
    $("#w0-tab0").removeClass("show in");
	$("#w0-tab1").removeClass("show in");
	$("#w0-tab2").removeClass("show in");
	$("#w0-tab4").removeClass("show in");
	$("#w0-tab3").removeClass("show in");
//	$("#w0-tab6").removeClass("show in");
});

function vetdupdate(fid){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vetd/tabsupdate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid }
      })
       .done(function( msg ) {
       $("#w0-tab4").empty().html(msg);
    });
 }
 function vetddelete(fid){
	 var x = confirm("האם אתה בטוח שברצונך למחוק?");
  if (x){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vetd/deletecomment']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
       //$("#w1-tab0").empty().html(msg);
    });
  }else{
	  return false;
	  }
 }
 function updatevetd(){
if($("#diagnostic_main").val() ==''){
			alert("יש לבחור אבחון ראשי.");
			$("#diagnostic_main").focus();
			return false;
		}
		if($("#diagnostic_second").val() ==''){
			alert("יש לבחון אבחון משני");
			$("#diagnostic_second").focus();
			return false;
		}

		if($("#diagnostic_for").val() ==''){
			alert("יש לבחור עבור מי");
			$("#diagnostic_for").focus();
			return false;
		}

		comments  = $("#comments").val();
		diagnostic_name   = $("#diagnostic_main").val();
		diagnostic_type   = $("#diagnostic_second").val();
		diagnostic_for       = $("#diagnostic_for").val();

		created_date     = $("#created_date").val();
		updated_date     = $("#updated_date").val();
		vet_id           = $("#vet_id").val();

		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vetd/updatecomment']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", vet_id:vet_id, diagnostic_name: diagnostic_name, diagnostic_type:diagnostic_type, comments: comments, diagnostic_for:diagnostic_for, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("אבחון עודכן בהצלחה!");
         setTimeout(function() {
        $("#vetd").trigger('click');
    },1);
       });
	}

function commentsupdate(fid){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vet/tabsupdate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid }
      })
       .done(function( msg ) {
       $("#w0-tab3").empty().html(msg);
    });
 }
 function deletecomment(fid){
	 var x = confirm("האם אתה בטוח שברצונך למחוק?");
  if (x){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vet/deletecomment']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
       //$("#w1-tab0").empty().html(msg);
    });
  }else{
	  return false;
	  }
 }
 function updatevet(){
	 	if($("#treatment").val() ==''){
			alert("יש לבחור טיפול");
			$("#treatment").focus();
			return false;
		}

		if($("#specific_treatment").val() ==''){
			alert("יש לבחון מינון");
			$("#specific_treatment").focus();
			return false;
		}
		if($("#frequency").val() ==''){
			alert("יש לבחור תדירות");
			$("#frequency").focus();
			return false;
		}
		if($("#dilution").val() ==''){
			alert("יש לבחור דילול");
			$("#dilution").focus();
			return false;
		}

    if($("#how_given").val() ==''){
      alert("יש לבחור צורת נתינה");
      $("#how_given").focus();
      return false;
    }

		if($("#treatment_for").val() ==''){
			alert("יש לבחור עבור מי");
			$("#treatment_for").focus();
			return false;
		}
        treatment         = $("#treatment").val();
		specific_treatment= $("#specific_treatment").val();
		frequency= $("#frequency").val();
		comments         = $("#comments").val();
		treatment_for    = $("#treatment_for").val();
		dilution    = $("#dilution").val();
		how_given    = $("#how_given").val();
		high    		= $("#high").val();
		vet_id           = $("#vet_id").val();
		created_date     = $("#created_date").val();
		updated_date     = $("#updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['vet/updatecomment']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>",treatment:treatment, specific_treatment:specific_treatment, frequency:frequency, dilution:dilution, how_given:how_given, high:high, vet_id:vet_id, comments: comments, treatment_for:treatment_for, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("טיפול עודכן בהצלחה!");
         setTimeout(function() {
        $("#vet").trigger('click');
    },1);
       });
	}

	function deletefood(fid){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/deletefood']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
       //$("#w1-tab0").empty().html(msg);
    });
 }
 function updatefood(){
		if($("#food_type").val() ==''){
			alert("יש לבחור סוג מזון");
			$("#food_type").focus();
			return false;
		}
		if($("#amount").val() ==''){
			alert("יש להזין כמות");
			$("#amount").focus();
			return false;
		}
		if($("#actual_amount").val() ==''){
			alert("יש לבחור סטטוס הזנה");
			$("#actual_amount").focus();
			return false;
		}

		food_type      = $("#food_type").val();
		unit           = $("#unit").val();
		amount         = $("#amount").val();
		actual_unit    = $("#actual_unit").val();
		actual_amount  = $("#actual_amount").val();
		food_id        = $("#food_id").val();
		created_date   = $("#created_date").val();
		updated_date   = $("#updated_date").val();
		$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/updatedfood']); ?>",
         data: { container_id: "<?php echo $session->get('cid'); ?>", food_type: food_type, food_id:food_id, unit:unit,  amount:amount, actual_unit:actual_unit,  actual_amount:actual_amount, created_date:created_date, updated_date:updated_date, created_by:"<?php echo $session->get('employee_name'); ?>"}
        })
       .done(function( msg ) {
		   alert("אוכל נשמר בהצלחה!");
         setTimeout(function() {
        $("#food").trigger('click');
    },1);
       });
	}

	function foodupdate(fid){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['food/updatefood']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid }
      })
       .done(function( msg ) {
       $("#w0-tab2").empty().html(msg);
    });
    }

	function updatewater(fid,watertype){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['water/tabsupdate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid, watertype:watertype }
      })
       .done(function( msg ) {
       $("#w0-tab3").empty().html(msg);
    });
 }
 function deletewater(fid){
	 var x = confirm("האם אתה בטוח שברצונך למחוק?");
  if (x){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['water/deletewater']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
       //$("#w1-tab0").empty().html(msg);
    });
  }else{
	 return false;
	 }
 }


 function updatelss(fid,watertype){
	 $.ajax({
       method: "POST",
          url: "<?php echo Url::to(['lss/tabsupdate']); ?>",
         data: { cid: "<?php echo $model->cid; ?>", fid:fid, watertype:watertype }
      })
       .done(function( msg ) {
       $("#w0-tab1").empty().html(msg);
    });
 }
 function deletelss(fid){
   alert(fid)
	 var x = confirm("האם אתה בטוח שברצונך למחוק?");
  if (x){
    $("tr[data-key='" + fid + "']").remove();
	$.ajax({
       method: "POST",
          url: "<?php echo Url::to(['lss/deletelss']); ?>",
         data: { fid: fid }
      })
       .done(function( msg ) {
		   alert("רשומה נמחקה בהצלחה!");
		   $("table tr:eq(fid)").remove();
      // $("#w1-tab1").empty().html(msg);
    });
  }else{
	 return false;
	 }
 }
</script>
