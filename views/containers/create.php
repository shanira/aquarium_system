<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Containers */

$this->title = Yii::t('app', 'הוספת מיכל');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="containers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
